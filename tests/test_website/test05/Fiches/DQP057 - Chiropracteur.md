﻿DQP057 - CHIROPRACTEUR
======================


1°. Définition de l'activité
----------------------------

Le chiropracteur est un professionnel de la santé pratiquant des actes de
manipulation et de mobilisation manuelles, instrumentales ou assistées, en se
consacrant sur les systèmes nerveux et musculo-squelettique dont la colonne
vertébrale.

Il effectue une analyse en repérant les points de blocage, détecte les
déplacements de structures osseuses et procède aux manipulations.

*Pour aller plus loin* : article 1 du décret n° 2011-32 du 7 janvier 2011 relatif
aux actes et aux conditions d'exercice de la chiropraxie.

2°. Qualifications professionnelles
-----------------------------------

### a. Exigences nationales

#### Législation nationale

L'exercice de la profession de chiropracteur est réservé :

 - aux titulaires du diplôme de chiropraxie délivré par un établissement de  formation agréé par le ministre chargée de la santé, après avis de la
  Commission nationale d'agrément ;
 - aux ressortissants d'un État membre de l'Union européenne (UE) ou partie à  l'Espace économique européen (EEE), titulaire d'une autorisation d'exercice
  de la chiropraxie ou d'user du titre de chiropracteur délivrée par
  l'autorité administrative compétente de cet État ;
 - aux médecins, sages-femmes, masseurs-kinésithérapeutes et infirmiers  autorisés à exercer, titulaires d'un diplôme universitaire ou
  interuniversitaire sanctionnant une formation suivie dans ce domaine au sein
  d'une unité de formation et de recherche de médecine délivré par une
  université de médecine et reconnu par le Conseil national de l'ordre des
  médecins.

*Pour aller plus loin* : article 75 de la loi n° 2002-303 du 4 mars 2002 relative
aux droits des malades et à la qualité du système de santé ; article 4 du décret
n° 2011-32 du 7 janvier 2011 relatif aux actes et aux conditions d'exercice de
la chiropraxie.

#### Formation

L'entrée dans un établissement de formation en chiropraxie est ouverte aux
titulaires du baccalauréat français ou d'un titre délivré par un autre État et
reconnu comme équivalent.

Une formation théorique d'au moins 2 120 heures et une formation pratique en
clinique d'au moins 1 400 heures, sont nécessaires pour se présenter aux examens
menant à la délivrance du diplôme de chiropracteur.

En France, l'[Institut franco-européen de chiropratique](https://www.ifec.net/)
(IFEC) dispense la formation et décerne le diplôme de chiropraxie.

*Pour aller plus loin* : articles 1 à 7 de l'arrêté du 24 mars 2014 relatif à la
formation des chiropracteurs et à l'agrément des établissements de formation en
chiropraxie

#### Coûts associés à la qualification

Le coût de cette formation est payant. Pour plus d'informations, il est
conseillé de se rapprocher de l'IFEC.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l'UE ou de l’EEE, exerçant légalement l’activité
de chiropracteur dans l’un de ces États, peut faire usage de son titre
professionnel en France, à titre temporaire ou occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par
déclaration adressée au directeur général de l'agence régionale de santé
d'Île-de-France (cf. infra « 5°. a. Effectuer une déclaration préalable
d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité
temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont
réglementées dans l'État dans lequel il est légalement établi, le professionnel
devra justifier l’avoir exercée dans un ou plusieurs États membres pendant au
moins deux ans, au cours des dix années qui précèdent la prestation.

*Pour aller plus loin* : articles 11 et 12 du décret n° 2011-32 du 7 janvier 2011
relatif aux actes et aux conditions d'exercice de la chiropraxie.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France et y
exercer l'activité de chiropracteur de manière permanente s'il est titulaire :

 - d'un titre de formation délivré par un État de l'UE ou de l'EEE qui  réglemente l'accès à l'activité ou son exercice ;
 - d'un titre de formation délivré par un État tiers mais reconnu par  l'autorité compétente d'un État de l'UE ou de l'EEE ;
 - de toute attestation permettant de justifier que le ressortissant a exercé  l'activité, pendant deux ans au cours des dix dernières années, dans un État
  de l’UE ou de l’EEE qui ne réglemente ni l’accès, ni l’exercice de cette
  activité sur son territoire.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant peut demander
une autorisation individuelle d'exercice auprès du directeur général de l'agence
régionale de santé d'Île-de-France (cf. infra « 5°. b. Demander une autorisation
individuelle d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'un
exercice permanent (LE) »).

Si l’examen des qualifications professionnelles attestées par les titres de
formation et l’expérience professionnelle fait apparaître des différences
substantielles avec les qualifications requises pour l’accès à la profession et
son exercice en France, l’intéressé doit se soumettre à une mesure de
compensation (cf. infra « 5°. b. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : articles 6 et 7 du décret n° 2011-32 du 7 janvier 2011
relatif aux actes et aux conditions d'exercice de la chiropraxie.

3°. Conditions d'honorabilité, règles déontologiques, éthique
-------------------------------------------------------------

### a. Règles déontologiques et incompatibilités

Bien que non codifiées, des [règles
déontologiques](http://chiropraxie.com/wp-content/uploads/2013/07/20130406-AFC-RI.pdf)
s'appliquent au chiropracteur et notamment :

 - veiller au respect du secret professionnel de ses patients ;
 - ne pas user de son mandat électif ou d'une fonction administrative pour  accroître sa clientèle ;
 - examiner, conseiller ou soigner chaque patient de la même manière, sans  considération de leur origine, mœurs ou situation familiale ;
 - pratiquer des actes avec le consentement libre et éclairé du patient.

Le chiropracteur ne peut pas effectuer certains actes dont :

 - la manipulation gynéco-obstétricale ;
 - les touchers pelviens.

*Pour aller plus loin* : article 3 du décret n° 2011-32 du 7 janvier 2011 relatif
aux actes et aux conditions d'exercice de la chiropraxie.

### b. Obligation de suivre une formation professionnelle continue

Les chiropracteurs doivent participer à un programme pluriannuel de
développement professionnel continu. Ce programme vise notamment à évaluer les
pratiques professionnelles, à perfectionner les compétences, à améliorer la
qualité et la sécurité des soins, à maintenir et à actualiser les connaissances
et les compétences. L’ensemble des actions réalisées par les chiropracteurs au
titre de leur obligation de développement professionnel continu est retracé dans
un document spécifique attestant du respect de cette obligation.

*Pour aller plus loin* : article 75 de la loi n° 2002-303 du 4 mars 2002 relative
aux droits des malades et à la qualité du système de santé ; articles L. 4021-1
et suivants et R. 4021-4 et suivants du code de la santé publique.

4°. Assurance
-------------

En qualité de professionnel de santé, le chiropracteur exerçant à titre libéral
doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que
facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire
pour ses salariés une telle assurance pour les actes effectués à l’occasion de
leur activité professionnelle.

**À savoir** : le fait de pratiquer cette activité sans être couvert par une
assurance de responsabilité civile professionnelle est puni de 45 000 euros
d'amende.

*Pour aller plus loin* : articles 1 et 2 de la loi n° 2014-201 du 24 février 2014
portant diverses dispositions d'adaptation au droit de l'Union européenne dans
le domaine de la santé.

5°. Démarche et formalités de reconnaissance de qualification
-------------------------------------------------------------

### a. Effectuer une déclaration préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

**Autorité compétente** : le directeur général de l'agence régionale de santé
d'Île-de-France est compétent pour délivrer la déclaration préalable d'activité
au ressortissant qui souhaite exercer sa profession en France à titre temporaire
et occasionnel.

**Pièces justificatives** : la demande de déclaration préalable d'activité est
accompagnée d'un dossier complet comprenant les pièces justificatives suivantes
:

 - le  [formulaire](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=75C65DA9206264782345D43D208F57AC.tplgfr39s_3?idArticle=LEGIARTI000023449766&cidTexte=LEGITEXT000023449667&dateTexte=20180208)
  de déclaration rempli, daté et signé ;
 - une photocopie de la pièce d'identité en cours de validité ;
 - une attestation d'assurance de responsabilité civile professionnelle ;
 - une photocopie des titres de formation ;
 - une attestation de l'autorité compétente de l’État de l'UE ou de l'EEE  certifiant que le ressortissant ne fait l'objet d'aucune interdiction
  d'exercer.

**À savoir** : le cas échéant, les pièces doivent être traduites en français par un
traducteur agréé.

**Procédure** : le directeur général accuse réception du dossier et dispose d'un
délai d'un mois pour se prononcer et informer le ressortissant :

 - qu'il peut débuter la prestation ;
 - qu'il ne peut pas la débuter ;
 - qu'il sera soumis à une épreuve d'aptitude en cas de différence  substantielle entre la formation exigée en France et ses qualifications
  professionnelles.
 - que l'examen du dossier est retardé et qu'un complément d'informations est  nécessaire.

**À noter** : la déclaration est renouvelable tous les ans et en cas de changement
de situation du ressortissant.

*Pour aller plus loin* : annexe 3 de l'arrêté du 7 janvier 2011 relatif à la
composition du dossier et aux modalités de l'organisation de l'épreuve
d'aptitude et du stage d'adaptation prévues pour les chiropracteurs par le
décret n° 2011-32 du 7 janvier 2011 relatif aux actes et aux conditions
d'exercice de la chiropraxie

### b. Demander une autorisation individuelle d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

**Autorité compétente** : le directeur général de l'agence régionale de santé
d'Île-de-France est compétent pour délivrer l'autorisation individuelle
d'exercice au ressortissant qui souhaite s'établir en France pour y exercer
l'activité de chiropracteur.

**Pièces justificatives** : pour obtenir cette autorisation, le ressortissant
doit envoyer à l'autorité compétente, un dossier en deux exemplaires par lettre
recommandée avec accusé de réception, comportant les pièces justificatives
suivantes :

 - le  [formulaire](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=8E0C3323BA88B61EACBF39F205F5CEAF.tplgfr39s_3?idArticle=LEGIARTI000023449673&cidTexte=LEGITEXT000023449667&dateTexte=20180208)
  de demande d'autorisation individuelle, rempli, daté et signé ;
 - une photocopie de la pièce d'identité en cours de validité ;
 - une copie du titre de formation permettant l'exercice de la profession ;
 - le cas échéant, tout diplôme complémentaire ;
 - tout document justifiant le suivi d'une formation continue, une expérience  et des compétences acquises dans cet État ;
 - une attestation de l'autorité compétente de l’État justifiant que le  ressortissant n'a pas de sanction à son encontre ;
 - une copie des attestations justifiant le niveau de formation et, année par  année, le détail et le volume horaire des enseignements suivis ainsi que le
  contenu et la durée des stages validés ;
 - le cas échéant, tout document justifiant l'exercice de la profession dans un  État de l'UE ou de l'EEE pendant deux ans au cours des dix dernières années,
  lorsque cet État ne réglemente ni l'accès, ni l'exercice de la profession de
  chiropracteur ;
 - le cas échéant, la reconnaissance d'un titre de formation délivrée par une  État de l'UE ou de l'EEE lorsque le titre a été acquis dans un État tiers.

**À savoir** : le cas échéant, les pièces doivent être traduites en français par un
traducteur agréé.

**Procédure** : le directeur général accusera réception du dossier dans un délai
d'un mois. Lorsque des mesures de compensation seront jugées nécessaires, le
directeur laissera au ressortissant un délai de deux mois pour choisir entre une
épreuve d'aptitude ou un stage d'adaptation.

Le silence gardé de l'autorité compétente dans un délai de quatre mois vaut
rejet de la demande d'autorisation.

**Bon à savoir** : mesures de compensation

L'épreuve d'aptitude prend la forme d'un examen écrit ou oral noté sur 20. Sa
validation est prononcée lorsque le ressortissant a obtenu une note moyenne
égale ou supérieure à 10 sur 20, et ce, sans note inférieure à 8 sur 20. En cas
de réussite à l'épreuve, le ressortissant sera autorisé à utiliser le titre de
chiropracteur.

Le stage d'adaptation se fait dans un établissement de santé public ou privé, ou
chez un professionnel, et ne doit pas durer plus de trois ans. Il comprend
également une formation théorique qui sera validée par le responsable du stage.
La décision d'autoriser le ressortissant à user du titre de chiropracteur sera
soumis à l'avis d'une commission composée du directeur générale de l'agence
régionale de santé d'Île-de-France et de quatre chiropracteurs reconnus pour
leurs compétences et leur expérience.

*Pour aller plus loin* : articles 6 à 10 du décret n° 2011-32 du 7 janvier 2011
relatif aux actes et aux conditions d'exercice de la chiropraxie ; arrêté du 7
janvier 2011 relatif à la composition du dossier et aux modalités de
l'organisation de l'épreuve d'aptitude et du stage d'adaptation prévues pour les
chiropracteurs par le décret n° 2011-32 du 7 janvier 2011 relatif aux actes et
aux conditions d'exercice de la chiropraxie.

### c. Enregistrement auprès du répertoire ADELI

Le ressortissant souhaitant exercer la profession de chiropracteur en France est
tenu de faire enregistrer son autorisation d'exercer sur le répertoire ADELI («
Automatisation Des Listes »).

**Autorité compétente** : l’enregistrement au répertoire ADELI se fait auprès de
l’agence régionale de santé (ARS) du lieu d’exercice.

**Délai** : la demande d’enregistrement est présentée dans le mois suivant la
prise de fonction du ressortissant, quel que soit le mode d’exercice (libéral,
salarié, mixte).

**Pièces justificatives** : à l'appui de sa demande d'enregistrement, le
chiropracteur doit fournir un dossier comportant :

 - le diplôme original ou titre attestant de la formation d'épithésiste délivréepar l'État de l'UE ou de l'EEE (traduit en français par un traducteur agréé, le
cas échéant) ;
 - une pièce d’identité ;
 - le formulaire Cerfa 13777*03 complété, daté et signé.

**Issue de la procédure** : le numéro ADELI du ressortissant sera directement
mentionné sur le récépissé du dossier, délivré par l'ARS.

Coût : gratuit.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’informations sur la reconnaissance
académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État
membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de
trouver une solution à un différend opposant un ressortissant de l’UE à
l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière
de reconnaissance des qualifications professionnelles.

**Conditions** : l’intéressé ne peut recourir à SOLVIT que s’il établit :

 - que l’Administration publique d’un État de l’UE n’a pas respecté les droitsque la législation européenne lui confère en tant que citoyen ou entreprise d’un
autre État de l’UE ;
 - et qu’il n’a pas déjà initié d’action judiciaire (le recours administratifn’est pas considéré comme tel).

**Procédure** : le ressortissant doit remplir un [formulaire de plainte en
ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine
pour demander, si besoin, des informations supplémentaires et pour vérifier que
le problème relève bien de sa compétence.

**Pièces justificatives** : pour saisir SOLVIT, le ressortissant doit
communiquer :

 - ses coordonnées complètes ;
 - la description détaillée de son problème ;
 - l’ensemble des éléments de preuve du dossier (par exemple, la correspondanceet les décisions reçues de l’autorité administrative concernée).

**Délai** : SOLVIT s’engage à trouver une solution dans un délai de dix semaines
à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays
dans lequel est survenu le problème.

**Coût** : gratuit.

**Issue de la procédure** : à l’issue du délai de dix semaines, le SOLVIT
présente une solution :

 - si cette solution règle le différend portant sur l’application du droiteuropéen, la solution est acceptée et le dossier est clos ;
 - s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyévers la Commission européenne.

Informations supplémentaires : SOLVIT en France : Secrétariat général des
affaires européennes, 68 rue de Bellechasse, 75700 Paris, [site
officiel](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html).

