﻿DQP015 - ANATOMIE ET CYTOLOGIE
==============================


1°. Définition de l’activité
----------------------------

L’anatomie et cytologie pathologique est une spécialité de la médecine qui
étudie la composition microscopique des cellules et des organes afin d’aider au
diagnostic.

Le médecin va prélever et étudier au microscope des cellules ou des tissus
présentant des lésions pour déterminer par quel type de maladies ils sont
touchés.

À partir de leur observation et de leur analyse, il pourra fournir les éléments
nécessaires au traitement des pathologies et évaluer la réponse à leurs
traitements.

2°. Qualifications professionnelles
-----------------------------------

### a. Exigences nationales

#### Législation nationale

En application de l’article L. 4111-1 du Code de la santé publique, pour exercer
légalement la profession de médecin en France, les intéressés doivent remplir
cumulativement les trois conditions suivantes :

 - être titulaire du diplôme français d’État de docteur en médecine ou d'un  titre ou diplôme conférant le titre de médecin ;
 - être de nationalité française, de citoyenneté andorrane ou ressortissant  d’un État membre de l’Union européenne (UE) ou partie à l’accord sur
  l’Espace économique européen (EEE) ou du Maroc, sous réserve de
  l’application des règles issues du Code de la santé publique ou
  d’engagements internationaux. Toutefois, cette condition ne s’applique pas
  au médecin titulaire du diplôme français d’État de docteur en médecine ;
 - être inscrit au tableau de l’Ordre des médecins (cf. infra «5°a.Demander son  inscription au tableau de l’Ordre des médecins »).

Toutefois, les personnes qui ne remplissent pas les conditions de diplôme ou de
nationalité peuvent être autorisées à exercer la profession de médecin par
arrêté individuel du ministre de la santé (cf. infra « 5°. c. Le cas échéant,
demander une autorisation individuelle d’exercice »).

*Pour aller plus loin* : articles L. 4111-1, L. 4112-6, L. 4112-7 et L. 4131-1 du
Code de la santé publique.

**À noter** : à défaut de remplir l’ensemble de ces conditions, l’exercice de la
profession de médecin est illégal et puni de deux ans d’emprisonnement et de 30
000 euros d’amende.

*Pour aller plus loin* : articles L. 4161-1 et L. 4161-5 du Code de la santé
publique.

**Bon à savoir** : la reconnaissance automatique de diplôme

En application de l’article L. 4131-1 du Code de la santé publique, les
ressortissants de l’UE ou de l’EEE peuvent exercer la profession de médecin
s’ils sont titulaires d’un des titres suivants :

 - les titres de formation de médecin délivrés par un État de l’UE ou de l’EEE  conformément aux obligations communautaires et figurant sur la liste établie
  en annexe de l’arrêté du 13 juillet 2009 fixant les listes et les conditions
  de reconnaissance des titres de formation de médecin et de médecin
  spécialiste délivrés par les États membres de l’UE ou parties à l’accord sur
  l’EEE visées au 2° de l’article L. 4131-1 du Code de la santé publique ;
 - les titres de formation de médecin délivrés par un État de l’UE ou de l’EEE  conformément aux obligations communautaires, ne figurant pas sur la liste
  précitée, s’ils sont accompagnés d’une attestation de cet État certifiant
  qu’ils sanctionnent une formation conforme à ces obligations et qu’ils sont
  assimilés, par lui, aux titres de formation figurant sur cette liste ;
 - les titres de formation de médecin délivrés par un État de l’UE ou de l’EEE  sanctionnant une formation de médecin commencée dans cet État antérieurement
  aux dates figurant dans l’arrêté précité et non conforme aux obligations
  communautaires, s’ils sont accompagnés d’une attestation de l’un de ces
  États certifiant que le titulaire des titres de formation s’est consacré,
  dans cet État, de façon effective et licite, à l’exercice de la profession
  de médecin dans la spécialité concernée pendant au moins trois années
  consécutives au cours des cinq années précédant la délivrance de
  l’attestation ;
 - les titres de formation de médecin délivrés par l’ancienne Tchécoslovaquie,  l’ancienne Union soviétique ou l’ancienne Yougoslavie ou qui sanctionnent
  une formation commencée avant la date d’indépendance de la République
  tchèque, de la Slovaquie, de l’Estonie, de la Lettonie, de la Lituanie ou de
  la Slovénie, s’ils sont accompagnés d’une attestation des autorités
  compétentes de l’un de ces États certifiant qu’ils ont la même validité sur
  le plan juridique que les titres de formation délivrés par cet État. Cette
  attestation est accompagnée d’un certificat délivré par ces mêmes autorités
  indiquant que son titulaire a exercé dans cet État, de façon effective et
  licite, la profession de médecin dans la spécialité concernée pendant au
  moins trois années consécutives au cours des cinq années précédant la
  délivrance du certificat ;
 - les titres de formation de médecin délivrés par un État de l’UE ou de l’EEE  ne figurant pas sur la liste précitée s’ils sont accompagnés d’une
  attestation délivrée par les autorités compétentes de cet État certifiant
  que le titulaire du titre de formation était établi sur son territoire à la
  date fixée dans l’arrêté précité et qu’il a acquis le droit d’exercer les
  activités de médecin généraliste dans le cadre de son régime national de
  sécurité sociale ;
 - les titres de formation de médecin délivrés par un État de l’UE ou de l’EEE  sanctionnant une formation de médecin commencée dans cet État antérieurement
  aux dates figurant dans l’arrêté précité et non conforme aux obligations
  communautaires mais permettant d’exercer légalement la profession de médecin
  dans l’État qui les a délivrés, si le médecin justifie avoir effectué en
  France au cours des cinq années précédentes trois années consécutives à
  temps plein de fonctions hospitalières dans la spécialité correspondant aux
  titres de formation en qualité d’attaché associé, de praticien attaché
  associé, d’assistant associé ou de fonctions universitaires en qualité de
  chef de clinique associé des universités ou d’assistant associé des
  universités, à condition d’avoir été chargé de fonctions hospitalières dans
  le même temps ;
 - les titres de formation de médecin spécialiste délivrés par l’Italie  figurant sur la liste précitée sanctionnant une formation de médecin
  spécialiste commencée dans cet État après le 31 décembre 1983 et avant le
  1er janvier 1991, s’ils sont accompagnés d’un certificat délivré par les
  autorités de cet État indiquant que son titulaire a exercé dans cet État, de
  façon effective et licite, la profession de médecin dans la spécialité
  concernée pendant au moins sept années consécutives au cours des dix années
  précédant la délivrance du certificat.

*Pour aller plus loin* : article L. 4131-1 du Code de la santé publique ; arrêté
du 13 juillet 2009 fixant les listes et les conditions de reconnaissance des
titres de formation de médecin et de médecin spécialiste délivrés par les États
membres de l’Union européenne ou parties à l’accord sur l’Espace économique
européen visées au 2° de l’article L. 4131-1 du Code de la santé publique.

#### Formation

Les études de médecine sont composées de trois cycles d’une durée totale
comprise entre neuf et onze ans, selon la filière choisie.

La formation, qui s’effectue à l’université, inclut de nombreux stages et est
ponctuée de deux concours :

 - le 1er survient en fin de 1ère année. Cette année d’étude, appelée «  première année commune aux études de santé » (PACES) est commune aux
  étudiants en médecine, pharmacie, odontologie, kinésithérapie et
  sages-femmes. À l’issue de ce premier concours, les étudiants sont classés
  selon leurs résultats. Ceux figurant en rang utile au regard du numerus
  clausus sont admis à poursuivre leurs études et à choisir, le cas échéant,
  de continuer une formation menant à l’exercice de la médecine ;
 - le 2ème survient en fin de 2ème cycle (c’est-à-dire à l’issue de la 6ème  année d’étude) : ce concours est appelé épreuves classantes nationales (ECN)
  ou anciennement « internat ». À l’issue de ce concours, les étudiants
  choisissent, en fonction de leur classement, leur spécialité et/ou leur
  ville d’affectation. La durée des études qui s’en suivent varie selon la
  spécialité choisie.

Pour obtenir son diplôme d’État (DE) de docteur en médecine, l’étudiant doit
valider l’ensemble de ses stages, son diplôme d’études spécialisées (DES) et
soutenir sa thèse avec succès.

*Pour aller plus loin* : article L. 632-1 du Code de l’éducation.
**Bon à savoir** : les étudiants en médecine doivent procéder à des vaccinations
obligatoires. Pour plus de précisions, il est conseillé de se reporter à
l’article R. 3112-1 du Code de la santé publique.

**Diplôme de formation générale en sciences médicales

Le 1er cycle est sanctionné par le diplôme de formation générale en sciences
médicales. Il comprend six semestres et correspond au niveau licence. Les deux
premiers semestres correspondent à la PACES.

La formation a pour objectifs :

 - l’acquisition des connaissances scientifiques de base, indispensables à la  maîtrise ultérieure des savoirs et des savoir-faire nécessaires à l’exercice
  des métiers médicaux. Cette base scientifique est large et englobe la
  biologie, certains aspects des sciences exactes et plusieurs disciplines des
  sciences humaines et sociales ;
 - l’approche fondamentale de l’homme sain et de l’homme malade, incluant tous  les aspects de la sémiologie.

Elle comprend des enseignements théoriques, méthodologiques, appliqués et
pratiques ainsi que l’accomplissement de stages dont un stage d’initiation aux
soins d’une durée de quatre semaines dans un établissement hospitalier.

*Pour aller plus loin* : arrêté du 22 mars 2011 relatif au régime des études en
vue du diplôme de formation générale en sciences médicales.

**Diplôme de formation approfondie en sciences médicales

Le 2ème cycle des études médicales est sanctionné par le diplôme de formation
approfondie en sciences médicales. Il comprend six semestres de formation et
correspond au niveau master.

Il a pour objectif l’acquisition des compétences génériques permettant aux
étudiants d’exercer par la suite, en milieu hospitalier ou en milieu
ambulatoire, les fonctions du 3ème cycle et d’acquérir les compétences
professionnelles de la formation dans laquelle ils s’engageront au cours de leur
spécialisation.

Les compétences à acquérir sont celles de communicateur, de clinicien, de
coopérateur, membre d’une équipe soignante pluriprofessionnelle, d’acteur de
santé publique, de scientifique et de responsable au plan éthique et
déontologique. L’étudiant doit également apprendre à faire preuve de
réflexivité.

Les enseignements portent essentiellement sur ce qui est fréquent ou grave ou
constitue un problème de santé publique ainsi que sur ce qui est cliniquement
exemplaire.

Les objectifs de la formation sont :

 - l’acquisition de connaissances relatives aux processus physiopathologiques,  à la pathologie, aux bases thérapeutiques et à la prévention complétant et
  approfondissant celles acquises au cours du cycle précédent ;
 - une formation à la démarche scientifique ;
 - l’apprentissage du raisonnement clinique ;
 - l’acquisition des compétences génériques préparant au 3ème cycle des études  médicales.

Outre les enseignements théoriques et pratiques, la formation comprend
l’accomplissement de trente-six mois de stages et de vingt-cinq gardes.

*Pour aller plus loin* : arrêté du 8 avril 2013 relatif au régime des études en
vue du 1er et du 2ème cycle des études médicales.

**Diplôme d'études spécialisées (DES)

L’accès au 3ème cycle se fait par les ECN. Pour exercer dans cette spécialité,
le professionnel doit obtenir le DES d'anatomie et cytologie pathologiques.

Ce diplôme se compose de dix semestres dont :

 - au moins six semestres dans la spécialité ;
 - au moins quatre semestres dans un lieu de stage avec encadrement  universitaire ;
 - au moins un semestre dans un lieu de stage sans encadrement universitaire.

La formation du DES se décompose en trois phases :

 - **La phase socle** : d'une durée de deux semestres, elle permet au candidat  d'acquérir les connaissances de base de la spécialité choisie et d'effectuer
  :

 - un stage dans un lieu hospitalier agréé à titre principal en anatomie et  cytologie pathologiques,
 - un stage libre ;
 - **La phase d'approfondissement** : d'une durée de six semestres, elle permet  au candidat d'acquérir les connaissances et compétences nécessaires à
  l'exercice de la spécialité choisie et d'effectuer :

 - quatre stages dans un lieu hospitalier agréé à titre principal en  anatomie et cytologie pathologiques,
 - un stage dans un lieu agréé à titre complémentaire ou principal en  anatomie et cytologie pathologiques et ayant des activités de pathologie
  moléculaire ou de génétique constitutionnelle ou de génétique somatique
  des tumeurs,
 - un stage libre ;
 - **La phase de consolidation** : d'une durée d’une année, elle permet au  candidat de consolider ses connaissances et d'effectuer d'un stage de un an
  accompli soit :

 - dans un lieu hospitalier agréé à titre principal en anatomie et  cytologie pathologiques,
 - sous la forme d’un stage mixte dans des lieux hospitalier et extra  hospitalier agréés à titre principal en anatomie et cytologie
  pathologiques.

Le DES ouvre droit à la qualification de spécialiste en anatomie et cytologie
pathologiques.

*Pour aller plus loin* : [annexe
II](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000036237037) de
l'arrêté du 27 novembre 2017 modifiant l’arrêté du 12 avril 2017 relatif à
l’organisation du troisième cycle des études de médecine et l’arrêté du 21 avril
2017 relatif aux connaissances, aux compétences et aux maquettes de formation
des diplômes d’études spécialisées et fixant la liste de ces diplômes et des
options et formations spécialisées transversales du troisième cycle des études
de médecine.

#### Coûts associés à la qualification

La formation menant à l'obtention du diplôme de médecin est payante et son coût
varie selon l'établissement choisi. Pour plus de précisions, il est conseillé de
se renseigner auprès des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services (LPS))

Le médecin ressortissant d’un État de l’UE ou de l’EEE qui est établi et exerce
légalement son activité dans l’un de ces États peut exercer en France, de
manière temporaire et occasionnelle, des actes de sa profession à la condition
d’avoir préalablement adressé une déclaration préalable au Conseil national de
l’Ordre des médecins (cf. infra « 5°. b. Effectuer une déclaration préalable
pour les ressortissants de l’UE ou de l’EEE exerçant une activité temporaire et
occasionnelle »).

**À savoir** : l’inscription au tableau de l’Ordre des médecins n’est pas requise
pour le professionnel en situation de libre prestation de services (LPS). Il
n'est donc pas tenu de s’acquitter des cotisations ordinales. Le médecin est
simplement enregistré sur une liste spécifique tenue par le Conseil national de
l’Ordre des médecins.

La déclaration préalable doit être accompagnée d’une déclaration concernant les
connaissances linguistiques nécessaires à la réalisation de la prestation. Dans
cette hypothèse, le contrôle de la maîtrise de la langue doit être proportionné
à l’activité à exercer et réalisé une fois la qualification professionnelle
reconnue.

Lorsque les titres de formation ne bénéficient pas d’une reconnaissance
automatique (cf. supra « 2°. a. Législation nationale »), les qualifications
professionnelles du prestataire sont vérifiées avant la première prestation de
services. En cas de différences substantielles entre les qualifications de
l’intéressé et la formation exigée en France de nature à nuire à la santé
publique, le prestataire est soumis à une épreuve.

Le médecin en situation de LPS est tenu de respecter les règles professionnelles
applicables en France, notamment l’ensemble des règles déontologiques (cf. infra
« 3°. Conditions d’honorabilité et règles déontologiques »). Il est soumis à la
juridiction disciplinaire de l’Ordre des médecins.

**À noter** : la prestation est réalisée sous le titre professionnel français de
médecin. Toutefois, lorsque les titres de formation ne bénéficient pas d’une
reconnaissance et dans le cas où les qualifications n’ont pas été vérifiées, la
prestation est réalisée sous le titre professionnel de l’État d’établissement,
de manière à éviter toute confusion avec le titre professionnel français.

*Pour aller plus loin* : article L. 4112-7 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Exercice (LE))

**Le régime de la reconnaissance automatique du diplôme obtenu dans un État de
l’UE

L’article L. 4131-1 du Code de la santé publique crée un régime de
reconnaissance automatique en France de certains diplômes ou titres, le cas
échéant, accompagnés de certificats, obtenus dans un État de l’UE ou de l’EEE
(cf. supra « 2°. a. Législation nationale »).

Il appartient au conseil départemental de l’Ordre des médecins compétent de
vérifier la régularité des diplômes, titres, certificats et attestations, d’en
accorder la reconnaissance automatique puis de statuer sur la demande
d’inscription au tableau de l’Ordre.

*Pour aller plus loin* : article L. 4131-1 du Code de la santé publique ; arrêté
du 13 juillet 2009 fixant les listes et les conditions de reconnaissance des
titres de formation de médecin et de médecin spécialiste délivrés par les États
membres de l’Union européenne ou parties à l’accord sur l’Espace économique
européen visées au 2° de l’article L. 4131-1 du Code de la santé publique.

**Le régime dérogatoire : l’autorisation préalable

Si le ressortissant de l’UE ou de l’EEE ne remplit pas les conditions pour
bénéficier du régime de reconnaissance automatique de ses titres ou diplômes, il
relève d’un régime d’autorisation d’exercice (cf. infra « 5°. c. Le cas échéant,
demander une autorisation individuelle d’exercice).

Les personnes qui ne bénéficient pas de la reconnaissance automatique mais qui
sont titulaires d’un titre de formation permettant d’exercer légalement la
profession de médecin, peuvent être individuellement autorisées à exercer dans
la spécialité concernée par le ministre de la santé, après avis d’une commission
composée notamment de professionnels.

Si l’examen des qualifications professionnelles attestées par les titres de
formation et l’expérience professionnelle fait apparaître des différences
substantielles avec les qualifications requises pour l’accès à la profession
dans la spécialité concernée et son exercice en France, l’intéressé doit se
soumettre à une mesure de compensation.

Selon le niveau de qualification exigé en France et celui détenu par
l’intéressé, l’autorité compétente peut soit :

 - proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve  d’aptitude ;
 - imposer un stage d’adaptation et / ou une épreuve d’aptitude.

*Pour aller plus loin* : article L. 4131-1-1 du Code de la santé publique.

3°. Conditions d’honorabilité, règles déontologiques, éthique
-------------------------------------------------------------

### a. Respect du Code de déontologie des médecins

Les dispositions du Code de déontologie médicale s’imposent à tous les médecins
exerçant en France, qu’ils soient inscrits au tableau de l’Ordre ou qu’ils
soient dispensés de cette obligation (cf. infra « 5°. a. Demander son
inscription au tableau de l’Ordre des médecins »).

**À savoir** : l’ensemble des dispositions du Code de déontologie est codifié aux
articles R. 4127-1 à R. 4127-112 du Code de la santé publique.

À ce titre, le médecin doit notamment respecter les principes de moralité, de
probité et de dévouement indispensables à l’exercice de la médecine. Il est
également soumis au secret médical et doit exercer en toute indépendance.

*Pour aller plus loin* : articles R. 4127-1 à R. 4127-112 du Code de la santé
publique.

### b. Cumul d’activités

Le médecin ne peut exercer une autre activité que si un tel cumul est compatible
avec les principes d’indépendance et de dignité professionnelles qui s’imposent
à lui. Le cumul d’activités ne doit pas lui permettre de tirer profit de ses
prescriptions ou de ses conseils médicaux.

Ainsi, le médecin ne peut pas cumuler l’exercice médical avec une autre activité
voisine du domaine de la santé. Il lui est notamment interdit d’exercer comme
opticien, ambulancier ou dirigeant d’une société d’ambulances, fabricant ou
vendeur d’appareils médicaux, propriétaire ou gérant d’un hôtel pour curistes,
d’une salle de sport, d’un spa, d’un cabinet de massage.

De la même manière, il est interdit à un médecin qui remplit un mandat électif
ou une fonction administrative d’en user pour accroître sa clientèle.

*Pour aller plus loin* : articles R. 4127-26 et R. 4127-27 du Code de la santé
publique.

### c. Conditions d’honorabilité

Pour pouvoir exercer, le médecin doit certifier qu’aucune instance pouvant
donner lieu à une condamnation ou une sanction susceptible d’avoir des
conséquences sur son inscription au tableau n’est en cours à son encontre.

*Pour aller plus loin* : article R. 4112-1 du Code de la santé publique.

### d. Obligation de développement professionnel continu

Les médecins doivent participer à un programme pluriannuel de développement
professionnel continu. Ce programme vise notamment à évaluer les pratiques
professionnelles, à perfectionner les compétences, à améliorer la qualité et la
sécurité des soins, à maintenir et à actualiser les connaissances et les
compétences.

L’ensemble des actions réalisées par les médecins au titre de leur obligation de
développement professionnel continu est retracé dans un document spécifique
attestant du respect de cette obligation.

*Pour aller plus loin* : articles L. 4021-1 et suivants et R. 4021-4 et suivants
du Code de la santé publique.

### e. Aptitude physique

Les médecins ne doivent pas présenter d’infirmité ou de pathologie incompatible
avec l’exercice de la profession (cf. infra « 5°. a. Demander son inscription au
tableau de l’Ordre des médecins »).

*Pour aller plus loin* : article R. 4112-2 du Code de la santé publique.

4°. Assurances
--------------

### a. Obligation de souscrire à une assurance de responsabilité civile professionnelle

En qualité de professionnel de santé exerçant une activité de prévention, de
diagnostic ou de soins, le médecin doit, s'il exerce à titre libéral, souscrire
une assurance de responsabilité professionnelle.

S'il exerce en tant que salarié, c'est à l'employeur de souscrire pour ses
salariés une telle assurance pour les actes effectués à l'occasion de cette
activité.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

### b. Obligation d'affiliation à la caisse autonome de retraite des médecins de France (CARMF)

Tout médecin inscrit au tableau de l’Ordre et exerçant sous la forme libérale
(même à temps partiel et même s’il exerce par ailleurs une activité salariée) a
l’obligation d’adhérer à la CARMF.

**Délai** : l’intéressé doit se déclarer à la CARMF dans le mois suivant le
début de son activité libérale.

**Modalités** : l’intéressé doit renvoyer le formulaire de déclaration, rempli,
daté et contresigné par le conseil départemental de l’Ordre des médecins. Ce
formulaire est téléchargeable sur le site de la [CARMF](http://www.carmf.fr/).

**À savoir** : en cas d’exercice au sein d’une société d’exercice libéral (SEL),
l’affiliation à la CARMF est également obligatoire pour tous les associés
professionnels y exerçant.

### c. Obligation de déclaration après de l'Assurance Maladie

Une fois inscrit au tableau de l’Ordre, le médecin exerçant sous forme libérale
doit déclarer son activité auprès de la Caisse primaire d’assurance maladie
(CPAM).

**Modalités** : l’inscription auprès de la CPAM peut être réalisée en ligne sur
le site officiel de l’Assurance maladie.

**Pièces justificatives** : le déclarant doit communiquer un dossier complet
comprenant :

 - la copie d’une pièce d’identité en cours de validité ;
 - un relevé d’identité bancaire (RIB) professionnel ;
 - le cas échéant, le(s) titre(s) justificatif(s) permettant l’accès au secteur  2.

Pour plus d’informations, il est conseillé de se reporter à la rubrique
consacrée à l’installation en libéral des médecins du site de l’Assurance
maladie.

5°. Démarche et formalités de reconnaissance de qualification
-------------------------------------------------------------

### a. Demander d'inscription au tableau de l'Ordre des médecins

L’inscription au tableau de l’Ordre est obligatoire pour exercer légalement
l’activité de médecin en France.

L’inscription au tableau de l’Ordre ne s’applique pas :

 - aux ressortissants de l’UE ou de l’EEE qui sont établis et qui exercent  légalement l’activité de médecin dans un État de l’UE ou de l’EEE,
  lorsqu’ils exécutent en France, de manière temporaire et occasionnelle, des
  actes de leur profession (cf. supra « 2°. b. Ressortissants UE et EEE : en
  vue d’un exercice temporaire et occasionnel ») ;
 - aux médecins appartenant aux cadres actifs du service de santé des armées ;
 - aux médecins qui, ayant la qualité de fonctionnaire de l’État ou d’agent  titulaire d’une collectivité locale, ne sont pas appelés, dans l’exercice de
  leurs fonctions, à exercer la médecine.

*Pour aller plus loin* : articles L. 4112-5 à L. 4112-7 du Code de la santé
publique.

**À noter** : l’inscription au tableau de l’Ordre permet la délivrance automatique
et gratuite de la carte de professionnel de santé (CPS). La CPS est une carte
d’identité professionnelle électronique. Elle est protégée par un Code
confidentiel et contient notamment les données d’identification du médecin
(identité, profession, spécialité). Pour plus d’informations, il est recommandé
de se reporter au site gouvernemental de l’[Agence française de la santé
numérique](http://esante.gouv.fr/).

**Autorité compétente** : la demande d’inscription est adressée au président du
conseil de l’Ordre des médecins du département dans lequel l’intéressé souhaite
établir sa résidence professionnelle.

La demande peut être directement déposée au conseil départemental de l’Ordre
concerné ou lui être adressée par courrier recommandé avec demande d’avis de
réception.

*Pour aller plus loin* : article R. 4112-1 du Code de la santé publique.

**À savoir** : en cas de transfert de sa résidence professionnelle hors du
département, le praticien est tenu de demander sa radiation du tableau de
l’Ordre du département où il exerçait et son inscription au tableau de l’Ordre
de sa nouvelle résidence professionnelle.

*Pour aller plus loin* : article R. 4112-3 du Code de la santé publique.

**Procédure** : à la réception de la demande, le conseil départemental désigne
un rapporteur qui procède à l’instruction de la demande et fait un rapport
écrit.

Le conseil vérifie les titres du candidat et demande communication du bulletin
n° 2 du casier judiciaire de l’intéressé. Il vérifie notamment que le candidat :

 - remplit les conditions nécessaires de moralité et d’indépendance (cf. supra  « 3°. c. Conditions d’honorabilité ») ;
 - remplit les conditions nécessaires de compétence ;
 - ne présente pas une infirmité ou un état pathologique incompatible avec  l’exercice de la profession (cf. supra « 3°. e. Aptitude physique »).

En cas de doute sérieux sur la compétence professionnelle du demandeur ou sur
l’existence d’une infirmité ou d’un état pathologique incompatible avec
l’exercice de la profession, le conseil départemental saisit le conseil régional
ou interrégional qui diligente une expertise. S’il est constaté, au vu du
rapport d’expertise, une insuffisance professionnelle rendant dangereux
l’exercice de la profession, le conseil départemental refuse l’inscription et
précise les obligations de formation du praticien.

Aucune décision de refus d’inscription ne peut être prise sans que l’intéressé
ait été invité quinze jours au moins à l’avance par lettre recommandée avec
demande d’avis de réception à comparaître devant le conseil pour y présenter ses
explications.

La décision du conseil de l’Ordre est notifiée, dans la semaine qui suit, à
l’intéressé au Conseil national de l’Ordre des médecins et au directeur général
de l’agence régionale de santé (ARS). La notification se fait par lettre
recommandée avec demande d’avis de réception.

La notification mentionne les voies de recours contre la décision. La décision
de refus doit être motivée.

*Pour aller plus loin* : articles R. 4112-2 et R. 4112-4 du Code de la santé
publique.

**Délai** : le président accuse réception du dossier complet dans un délai d’un
mois à compter de son enregistrement.

Le conseil départemental de l’Ordre doit statuer sur la demande d’inscription
dans un délai maximum de trois mois à compter de la réception du dossier complet
de demande. À défaut de réponse dans ce délai, la demande d’inscription est
réputée rejetée.

Ce délai est porté à six mois pour les ressortissants des États tiers lorsqu’il
y a lieu de procéder à une enquête hors de la France métropolitaine. L’intéressé
en est alors avisé.

Il peut également être prorogé d’une durée qui ne peut excéder deux mois par le
conseil départemental lorsqu’une expertise a été ordonnée.

*Pour aller plus loin* : articles L. 4112-3 et R. 4112-1 du Code de la santé
publique.

**Pièces justificatives** : l’intéressé doit adresser un dossier complet de
demande d’inscription comprenant :

 - deux exemplaires du questionnaire normalisé avec une photo d’identité  rempli, daté et signé, disponible dans les conseils départementaux de
  l’Ordre ou directement téléchargeable sur le [site
  officiel](https://www.conseil-national.medecin.fr/) du Conseil national de
  l’Ordre des médecins ;
 - une photocopie d’une pièce d’identité en cours de validité ou, le cas  échéant, une attestation de nationalité délivrée par une autorité compétente
  ;
 - le cas échéant, une photocopie de la carte de séjour de membre de la famille  d’un citoyen de l’UE en cours de validité, de la carte de résident de longue
  durée-CE en cours de validité ou de la carte de résident portant mention du
  statut de réfugié en cours de validité ;
 - le cas échéant, une photocopie de la carte bleue européenne en cours de  validité ;
 - une copie, accompagnée le cas échéant d’une traduction, faite par un  traducteur agréé, des titres de formation à laquelle sont joints :

 - lorsque le demandeur est un ressortissant de l’UE ou de l’EEE, la ou les  attestations prévues (cf. supra « 2°a. Exigences nationales »),
 - lorsque le demandeur bénéficie d’une autorisation d’exercice  individuelle (cf. supra « 2°. c. Ressortissants UE et EEE : en vue d’un
  exercice permanent »), la copie de cette autorisation,
 - lorsque le demandeur présente un diplôme délivré dans un État étranger  dont la validité est reconnue sur le territoire français, la copie des
  titres à la possession desquels cette reconnaissance peut être
  subordonnée ;
 - pour les ressortissants d’un État étranger, un extrait de casier judiciaire  ou un document équivalent datant de moins de trois mois, délivré par une
  autorité compétente de l’État d’origine. Cette pièce peut être remplacée,
  pour les ressortissants des États de l’UE ou de l’EEE qui exigent une preuve
  de moralité ou d’honorabilité pour l’accès à l’activité de médecin, par une
  attestation, datant de moins de trois mois, de l’autorité compétente de
  l’État d’origine certifiant que ces conditions de moralité ou d’honorabilité
  sont remplies ;
 - une déclaration sur l’honneur du demandeur certifiant qu’aucune instance  pouvant donner lieu à condamnation ou sanction susceptible d’avoir des
  conséquences sur l’inscription au tableau n’est en cours à son encontre ;
 - un certificat de radiation d’inscription ou d’enregistrement délivré par  l’autorité auprès de laquelle le demandeur était antérieurement inscrit ou
  enregistré ou, à défaut, une déclaration sur l’honneur du demandeur
  certifiant qu’il n’a jamais été inscrit ou enregistré ou, à défaut, un
  certificat d’inscription ou d’enregistrement dans un État de l’UE ou de
  l’EEE ;
 - tous les éléments de nature à établir que le demandeur possède les  connaissances linguistiques nécessaires à l’exercice de la profession ;
 - un curriculum vitae ;
 - les contrats et avenants ayant pour objet l’exercice de la profession ainsi  que ceux relatifs à l’usage du matériel et du local dans lequel le demandeur
  exerce ;
 - si l’activité est exercée sous forme de SEL ou de société civile  professionnelle (SCP), les statuts de cette société et leurs avenants
  éventuels ;
 - si le demandeur est fonctionnaire ou agent public, l’arrêté de nomination ;
 - si le demandeur est professeur des universités – praticien hospitalier  (PU-PH), maître de conférences des universités – praticien hospitalier
  (MCU-PH) ou praticien hospitalier (PH), l’arrêté de nomination en qualité de
  praticien hospitalier et, le cas échéant, le décret ou l’arrêté de
  nomination en qualité de professeur des universités ou de maître de
  conférences des universités.

Pour plus d’informations, il est conseillé de se reporter au site officiel du
Conseil national de l’Ordre des médecins.

*Pour aller plus loin* : articles L. 4113-9 et R. 4112-1 du Code de la santé
publique.

**Voies de recours** : le demandeur ou le Conseil national de l’Ordre des
médecins peuvent contester la décision d’inscription ou de refus d’inscription
dans un délai de 30 jours à compter de la notification de la décision ou de la
décision implicite de rejet. L’appel est porté devant le conseil régional
territorialement compétent.

Le conseil régional doit statuer dans un délai de deux mois à compter de la
réception de la demande. À défaut de décision dans ce délai, le recours est
réputé rejeté.

La décision du conseil régional est également susceptible d’appel, dans les 30
jours, auprès du Conseil national de l’Ordre des médecins. La décision ainsi
rendue peut elle-même faire l’objet d’un recours devant le Conseil d’État.

*Pour aller plus loin* : articles L. 4112-4 et R. 4112-5 du Code de la santé
publique.

**Coût** : l’inscription au tableau de l’Ordre est gratuite mais elle engendre
l’obligation de s’acquitter de la cotisation ordinale obligatoire dont le
montant est fixé annuellement et qui doit être versée au cours du premier
trimestre de l’année civile en cours. Le paiement peut s’effectuer en ligne sur
le site officiel du Conseil national de l’Ordre des médecins. À titre indicatif,
en 2017, le montant de cette cotisation s’élève à 333 euros.

*Pour aller plus loin* : article L. 4122-2 du Code de la santé publique.

### b. Effectuer une déclaration préalable en vue d'un exercice temporaire et occasionnel (LPS)

Tout ressortissant de l’UE ou de l’EEE qui est établi et exerce légalement les
activités de médecin dans l’un de ces États peut exercer en France de manière
temporaire ou occasionnelle s’il en fait la déclaration préalable (cf. supra 2°
b. « Ressortissants UE et EEE : en vue d’un exercice temporaire et occasionnel
»). La déclaration préalable doit être renouvelée tous les ans.

**À noter** : tout changement de situation du demandeur doit être notifié dans les
mêmes conditions.

*Pour aller plus loin* : articles L. 4112-7 et R. 4112-9-2 du Code de la santé
publique.

**Autorité compétente** : la déclaration doit être adressée, avant la première
prestation de services, au Conseil national de l’Ordre des médecins.

**Modalités de la déclaration et récépissé** : la déclaration peut être envoyée
par courrier ou directement effectuée en ligne sur le site officiel de l’Ordre
des médecins.

Lorsque le Conseil national de l’Ordre des médecins reçoit la déclaration et
l’ensemble des pièces justificatives nécessaires, il adresse au prestataire un
récépissé précisant son numéro d’enregistrement ainsi que la discipline exercée.

**À noter** : le prestataire de services informe préalablement l’organisme national
d’assurance maladie compétent de sa prestation de services par l’envoi d’une
copie de ce récépissé ou par tout autre moyen.

*Pour aller plus loin* : articles R. 4112-9-2 et R. 4112-11 du Code de la santé
publique.

**Pièces justificatives** : la déclaration doit comporter les éléments suivants
:

 - le [formulaire de  déclaration](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=83B6B69A79BA3CA1D089890DFF32CC1E.tplgfr34s_3?idArticle=LEGIARTI000036145868&cidTexte=LEGITEXT000036145857&dateTexte=20180115)
  complété et signé ;
 - la copie d'une pièce d'identité en cours de validité ou tout document  attestant la nationalité du demandeur ;
 - la copie du titre de formation et du titre de formation de spécialiste  permettant au demandeur d'exercer son activité ;
 - une attestation de moins de trois mois, justifiant que le professionnel est  légalement établi et ne fait l'objet d'aucune interdiction d'exercer, même à
  titre temporaire, sa profession ;
 - lorsque le professionnel a acquis sa formation dans un État tiers et reconnu  dans un État de l'UE ou de l'EEE :

 - la reconnaissance de ces titres de formation de base et de spécialiste ;
 - toute document justifiant qu'il a exercé la profession pendant trois ans  à temps plein ou à temps partiel ;
 - le cas échéant, une copie de la déclaration précitée.

*Pour aller plus loin* : article R. 4112-9 et suivants du CSP ; arrêté du 4
décembre 2017 relatif à la déclaration préalable de services pour les
professions médicales et pharmaciens.

**Délai** : dans un délai d’un mois à compter de la réception de la déclaration,
le Conseil national de l’Ordre informe le demandeur :

 - qu’il peut ou non débuter la prestation de services ;
 - lorsque la vérification des qualifications professionnelles met en évidence  une différence substantielle avec la formation exigée en France, qu’il doit
  prouver avoir acquis les connaissances et les compétences manquantes en se
  soumettant à une épreuve d’aptitude. S’il satisfait à ce contrôle, il est
  informé dans un délai d’un mois qu’il peut débuter la prestation de services
  ;
 - lorsque l’examen du dossier met en évidence une difficulté nécessitant un  complément d’informations, des raisons du retard pris dans l’examen de son
  dossier. Il dispose alors d’un délai d’un mois pour obtenir les compléments
  d’informations demandés. Dans ce cas, avant la fin du 2ème mois à compter de
  la réception de ces informations, le Conseil national informe le
  prestataire, après réexamen de son dossier :

 - qu’il peut ou non débuter la prestation de services,
 - lorsque la vérification des qualifications professionnelles du  prestataire met en évidence une différence substantielle avec la
  formation exigée en France, qu’il doit démontrer qu’il a acquis les
  connaissances et compétences manquantes, notamment en se soumettant à
  une épreuve d’aptitude.

Dans cette dernière hypothèse, s’il satisfait à ce contrôle, il est informé dans
le délai d’un mois qu’il peut débuter la prestation de services. Dans le cas
contraire, il est informé qu’il ne peut pas débuter la prestation de services.

En l’absence de réponse du Conseil national de l’Ordre dans ces délais, la
prestation de services peut débuter.

*Pour aller plus loin* : article R. 4112-9-1 du Code de la santé publique.

### c. Le cas échéant, demander une autorisation individuelle d’exercice

**Pour les ressortissants de l’UE ou de l’EEE

Peuvent solliciter une autorisation individuelle les ressortissants de l’UE ou
de l’EEE titulaires d’un titre de formation :

 - délivré par l’un de ces États ne bénéficiant pas de la reconnaissance  automatique (cf. supra « 2°. c. Ressortissants UE et EEE : en vue d’un
  exercice permanent ») ;
 - délivré par un État tiers mais reconnu par un État membre de l’UE ou de  l’EEE, à condition qu’ils justifient avoir exercé la profession de médecin
  dans la spécialité pendant une durée équivalente à trois ans à temps plein
  dans cet État membre.

Une commission d’autorisation d’exercice (CAE) examine la formation du demandeur
et son expérience professionnelle.

Elle peut proposer une mesure de compensation :

 - lorsque la formation est inférieure d’au moins un an à celle du DE français,  lorsqu’elle porte sur des matières substantiellement différentes ou
  lorsqu’une ou plusieurs composantes de l’activité professionnelle dont
  l’exercice est subordonné au diplôme précité n’existent pas dans la
  profession correspondante dans l’État membre d’origine ou n’ont pas fait
  l’objet d’un enseignement dans cet État ;
 - et lorsque la formation et l’expérience du demandeur ne sont pas de nature à  couvrir ces différences.

Selon le niveau de qualification exigé en France et celui détenu par
l’intéressé, l’autorité compétente peut soit :

 - proposer au demandeur de choisir entre un stage d’adaptation ou une épreuve  d’aptitude ;
 - imposer un stage d’adaptation ou une épreuve d’aptitude.

L’**épreuve d’aptitude** a pour objet de vérifier, par des épreuves écrites ou
orales ou par des exercices pratiques, l’aptitude du demandeur à exercer la
profession de médecin dans la spécialité concernée. Elle porte sur les matières
qui ne sont pas couvertes par le ou les titres de formation du demandeur ou de
son expérience professionnelle.

Le **stage d’adaptation** a pour objet de permettre aux intéressés d’acquérir
les compétences nécessaires à l’exercice de la profession de médecin. Il est
accompli sous la responsabilité d’un médecin et peut être accompagné d’une
formation théorique complémentaire facultative. La durée du stage n’excède pas
trois ans. Il peut être effectué à temps partiel.

*Pour aller plus loin* : articles L. 4111-2 II, L. 4131-1-1, R. 4111-17 à R.
4111-20 et R. 4131-29 du Code de la santé publique.

**Pour les ressortissants d’un État tiers

Peuvent solliciter une autorisation individuelle d’exercice, à condition
qu’elles justifient d’un niveau suffisant de maîtrise de la langue française,
les personnes titulaires d’un titre de formation :

 - délivré par un État de l’UE ou de l’EEE dont l’expérience est attestée par  tout moyen ;
 - délivré par un État tiers permettant l’exercice de la profession de médecin  dans le pays d’obtention du diplôme :

 - si elles satisfont à des épreuves anonymes de vérification des  connaissances fondamentales et pratiques. Pour plus d’informations sur
  ces épreuves, il est conseillé de se reporter au site officiel du Centre
  national de gestion (CNG),
 - et si elles justifient de trois ans de fonctions accomplies dans un  service ou un organisme agréé pour la formation des internes.

**À noter** : sont réputés avoir satisfait aux épreuves de vérification des
connaissances les médecins titulaires d’un diplôme d’études spécialisées obtenu
dans le cadre de l’internat à titre étranger.

*Pour aller plus loin* : articles L. 4111-2 (I et I bis), D. 4111-1, D.4111-6 et
R. 4111-16-2 du Code de la santé publique.

**Autorité compétente** : la demande est adressée en deux exemplaires, par
lettre recommandée avec demande d’avis de réception à la cellule chargée des
commissions d’autorisation d’exercice (CAE) du CNG.

L’autorisation d’exercice est délivrée par le ministre de la santé après avis de
la CAE.

*Pour aller plus loin* : articles R. 4111-14 et R. 4131-29 du Code de la santé
publique ; arrêté du 25 février 2010 fixant la composition du dossier à fournir
aux CAE compétentes pour l’examen des demandes présentées en vue de l’exercice
en France des professions de médecin, chirurgien-dentiste, sage-femme et
pharmacien.

**Délai** : le CNG accuse réception de la demande dans le délai d’un mois à
compter de sa réception.

Le silence gardé pendant un certain délai à compter de la réception du dossier
complet vaut décision de rejet de la demande. Ce délai est de :

 - quatre mois pour les demandes présentées par les ressortissants de l’UE ou  de l’EEE titulaires d’un diplôme délivré dans l’un de ces États ;
 - six mois pour les demandes présentées par les ressortissants d’États tiers  titulaires d’un diplôme délivré par un État de l’UE ou de l’EEE ;
 - un an pour les autres demandes. Ce délai peut être prolongé de deux mois,  par décision de l’autorité ministérielle notifiée au plus tard un mois avant
  l’expiration de celui-ci, en cas de difficulté sérieuse portant sur
  l’appréciation de l’expérience professionnelle du candidat.

*Pour aller plus loin* : articles R. 4111-2, R. 4111-14 et R. 4131-29 du Code de
la santé publique.

**Pièces justificatives** : le dossier de demande doit contenir :

 - un formulaire de demande d’autorisation d’exercice de la profession dont le  modèle figure à l’annexe 1 de l’arrêté du 25 février 2010, complété, daté et
  signé et faisant apparaître, le cas échéant, la spécialité dans laquelle le
  candidat dépose sa demande ;
 - une photocopie d’une pièce d’identité en cours de validité ;
 - une copie du titre de formation permettant l’exercice de la profession dans  l’État d’obtention ainsi que, le cas échéant, une copie du titre de
  formation de spécialiste ;
 - le cas échéant, une copie des diplômes complémentaires ;
 - toutes pièces utiles justifiant des formations continues, de l’expérience et  des compétences acquises au cours de l’exercice professionnel dans un État
  de l’UE ou de l’EEE, ou dans un État tiers (attestations de fonctions, bilan
  d’activité, bilan opératoire, etc.) ;
 - dans le cadre de fonctions exercées dans un État autre que la France, une  déclaration de l’autorité compétente de cet État, datant de moins d’un an,
  attestant de l’absence de sanctions à l’égard du demandeur.

Selon la situation du demandeur, d’autres pièces justificatives sont exigées.
Pour plus d’informations, il est conseillé de se reporter au site officiel du
CNG.

**À savoir** : les pièces justificatives doivent être rédigées en langue française
ou traduites par un traducteur agréé.

*Pour aller plus loin* : arrêté du 25 février 2010 fixant la composition du
dossier à fournir aux commissions d’autorisation d’exercice compétentes pour
l’examen des demandes présentées en vue de l’exercice en France des professions
de médecin, chirurgien-dentiste, sage-femme et pharmacien ; instruction du
gouvernement du 17 novembre 2014 n°DGOS/RH1/RH2/RH4/2014/318.

### d. Voies de recours

**Centre d’assistance Français**

Le Centre ENIC-NARIC est le centre français d’informations sur la reconnaissance
académique et professionnelle des diplômes.

**SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État
membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de
trouver une solution à un différend opposant un ressortissant de l’UE à
l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière
de reconnaissance des qualifications professionnelles.

**Conditions** : L’intéressé ne peut recourir à SOLVIT que s’il établit :

 - que l’Administration publique d’un État de l’UE n’a pas respecté les droits  que la législation européenne lui confère en tant que citoyen ou entreprise
  d’un autre État de l’UE ;
 - et qu’il n’a pas déjà initié d’action judiciaire (le recours administratif  n’est pas considéré comme tel).

**Procédure** : Le ressortissant doit remplir un formulaire de plainte en ligne.
Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine
pour demander, si besoin, des informations supplémentaires et pour vérifier que
le problème relève bien de sa compétence.

**Pièces justificatives** : Pour saisir SOLVIT, le ressortissant doit
communiquer :

 - ses coordonnées complètes ;
 - la description détaillée de son problème ;
 - l’ensemble des éléments de preuve du dossier (par exemple, la correspondance  et les décisions reçues de l’autorité administrative concernée).

**Délai** : SOLVIT s’engage à trouver une solution dans un délai de dix semaines
à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays
dans lequel est survenu le problème.

**Coût** : Gratuit.

**Issue de la procédure** : A l’issue du délai de dix semaines, le SOLVIT
présente une solution :

 - si cette solution règle le différend portant sur l’application du droit  européen, la solution est acceptée et le dossier est clos ;
 - s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé  vers la Commission européenne.

**Informations supplémentaires** : SOLVIT en France : Secrétariat général des
affaires européennes, 68 rue de Bellechasse, 75700 Paris, site officiel).

