﻿DQP058 - CHIRURGIEN-DENTISTE (PRATICIEN DE L'ART DENTAIRE)
==========================================================


1°. Définition de l'activité
----------------------------

Le chirurgien-dentiste est un professionnel de santé pratiquant l'odontologie,
c'est à dire l'étude de l'organe dentaire, des maxillaires et des tissus
attenants. Ainsi, il assure la prévention, le diagnostic et le traitement des
anomalies liées à ces parties du corps.

Il peut également être amené à pratiquer des opérations nécessitant une
anesthésie, comme pour l'extraction de dents de sagesse par exemple.

Le plus souvent, il s'occupera du détartrage des dents de ses patients, soignera
les caries, préparera la pose de prothèses en faisant des moulages, et fera part
de ses conseils en matière d'hygiène bucco-dentaire.

*Pour aller plus loin* : article L. 4141-1 du Code de la santé publique (CSP).

2°. Qualifications professionnelles
-----------------------------------

### a. Exigences nationales

#### Législation nationale

En application de l’article L. 4111-1 du Code de la santé publique, pour exercer
légalement la profession de chirurgien-dentiste en France, les intéressés
doivent remplir cumulativement les trois conditions suivantes :

 - être titulaire du diplôme français d’État de chirurgien-dentiste, du diplômefrançais d’État de docteur en chirurgie dentaire ou d’un diplôme, certificat ou
autre titre mentionné à l’article L. 4141-3 du Code de la santé publique (cf.
infra « Bon à savoir : la reconnaissance automatique de diplôme ») ;
 - être de nationalité française, de citoyenneté andorrane ou ressortissant d’unÉtat membre de l’Union européenne (UE) ou partie à l’accord sur l’Espace
économique européen (EEE) ou du Maroc, sous réserve de l’application des règles
issues du Code de la santé publique ou d’engagements internationaux ;
 - sauf exception, être inscrit au tableau du conseil de l’Ordre deschirurgiens-dentistes (cf. infra « 5°. b. Demander son inscription au tableau de
l’Ordre des chirurgiens-dentistes »).

*Pour aller plus loin* : articles L. 4111-1 et L. 4141-3 du CSP.

**Bon à savoir** : reconnaissance automatique de diplôme

En application de l’article L. 4141-3 du Code de la santé publique, les
ressortissants de l’UE ou de l’EEE peuvent exercer la profession de
chirurgien-dentiste s’ils sont titulaires d’un des titres suivants :

 - les titres de formation de praticien de l'art dentaire délivrés par l'un de  ces États conformément aux obligations communautaires et figurant sur une
  liste établie par l'arrêté du 13 juillet 2009 fixant la liste et les
  conditions de reconnaissance des titres de formation de praticien de l'art
  dentaire spécialiste délivrés par les Etats membres de la Communauté
  européenne ou parties à l'accord sur l'Espace économique européen visées au
  3° de l'article L. 4141-3 du Code de la santé publique ;
 - les titres de formation de praticien de l'art dentaire délivrés par un État  de l’UE ou de l’EEE conformément aux obligations communautaires, ne figurant
  pas sur la liste de l'arrêté du 13 juillet 2009, s'ils sont accompagnés
  d'une attestation de cet État certifiant qu'ils sanctionnent une formation
  conforme à ces obligations et qu'ils sont assimilés, par lui, aux diplômes,
  certificats et titres figurant sur cette liste ;
 - Les titres de formation de praticien de l'art dentaire délivrés par un État  de l’UE ou de l’EEE sanctionnant une formation de praticien de l'art
  dentaire commencée dans cet État antérieurement aux dates figurant dans
  l'arrêté mentionné à l'arrêté du 13 juillet 2009 et non conforme aux
  obligations communautaires, s'ils sont accompagnés d'une attestation de l'un
  de ces États certifiant que le titulaire des titres de formation s'est
  consacré, dans cet État, de façon effective et licite aux activités de
  praticien de l'art dentaire ou, le cas échéant, de praticien de l'art
  dentaire spécialiste, pendant au moins trois années consécutives au cours
  des cinq années précédant la délivrance de l'attestation ;
 - les titres de formation de praticien de l'art dentaire délivrés par  l'ancienne Union soviétique ou l'ancienne Yougoslavie ou qui sanctionnent
  une formation commencée avant la date d'indépendance de l'Estonie, de la
  Lettonie, de la Lituanie ou de la Slovénie, s'ils sont accompagnés d'une
  attestation des autorités compétentes de l'Estonie, de la Lettonie ou de la
  Lituanie pour les titres de formation délivrés par l'ancienne Union
  soviétique, de la Slovénie pour les titres de formation délivrés par
  l'ancienne Yougoslavie, certifiant qu'ils ont la même validité sur le plan
  juridique que les titres de formation délivrés par cet État. Cette
  attestation est accompagnée d'un certificat délivré par ces mêmes autorités
  indiquant que son titulaire a exercé dans cet État, de façon effective et
  licite, la profession de praticien de l'art dentaire ou de praticien de
  l'art dentaire spécialiste pendant au moins trois années consécutives au
  cours des cinq années précédant la délivrance du certificat ;
 - les titres de formation de praticien de l'art dentaire délivrés par un État,  membre ou partie, sanctionnant une formation de praticien de l'art dentaire
  commencée dans cet État antérieurement aux dates figurant dans l'arrêté
  mentionné à l'arrêté du 13 juillet 2009 et non conforme aux obligations
  communautaires mais permettant d'exercer légalement la profession de
  praticien de l'art dentaire dans l'État qui les a délivrés, si le praticien
  de l'art dentaire justifie avoir effectué en France au cours des cinq années
  précédentes trois années consécutives à temps plein de fonctions
  hospitalières, le cas échéant dans la spécialité correspondant aux titres de
  formation, en qualité d'attaché associé, de praticien attaché associé,
  d'assistant associé ou de fonctions universitaires en qualité de chef de
  clinique associé des universités ou d'assistant associé des universités, à
  condition d'avoir été chargé de fonctions hospitalières dans le même temps ;
 - un titre de formation de médecin délivré en Italie, en Espagne, en Autriche,  en République tchèque, en Slovaquie et en Roumanie sanctionnant une
  formation commencée au plus tard aux dates fixées par arrêté des ministres
  chargés de l'enseignement supérieur et de la santé, s'il est accompagné
  d'une attestation des autorités compétentes de cet État certifiant qu'il
  ouvre droit dans cet État à l'exercice de la profession de praticien de
  l'art dentaire et que son titulaire s'est consacré, dans cet État, de façon
  effective et licite, aux activités de praticien de l'art dentaire pendant au
  moins trois années consécutives au cours des cinq années précédant la
  délivrance de l'attestation.
 - les titres de formation de praticien de l'art dentaire délivrés par un État,  membre ou partie, sanctionnant une formation débutée avant le 18 janvier
  2016 ;
 - les titres de formation de médecin délivrés par l'Espagne sanctionnant une  formation de médecin commencée dans cet État entre le 1er janvier 1986 et le
  31 décembre 1997, s'ils sont accompagnés d'une attestation délivrée par les
  autorités compétentes de cet État indiquant que son titulaire a suivi avec
  succès au moins trois années d'études conformes aux obligations
  communautaires de formation de base à la profession de praticien de l'art
  dentaire, qu'il a exercé, de façon effective, licite et à titre principal,
  la profession de praticien de l'art dentaire pendant au moins trois années
  consécutives au cours des cinq années précédant la délivrance de
  l'attestation et qu'il est autorisé à exercer ou exerce, de façon effective,
  licite et à titre principal, cette profession dans les mêmes conditions que
  les titulaires de titres de formation figurant sur la liste mentionnée à
  l'arrêté du 13 juillet 2009.

#### Formation

Les études de pharmacie sont composées de trois cycles d'une durée totale
comprise entre six et neuf ans selon la filière choisie.

**Diplôme de formation générale en sciences odontologiques

Le premier cycle est sanctionné par le diplôme de formation générale en sciences
odontologiques. Il comprend six semestres et correspond au niveau licence. Les
deux premiers semestres correspondent à la [première année commune aux études de
santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021276755&dateTexte=20180119).

La formation a pour objectif :

 - l'acquisition d'un socle de connaissances scientifiques indispensables à la  maîtrise ultérieure des savoirs et des savoir-faire nécessaires à l'exercice
  de la profession de chirurgien-dentiste. Cette base scientifique englobe la
  biologie, certains aspects des sciences exactes et plusieurs disciplines des
  sciences humaines et sociales ;
 - l'acquisition de connaissances dans les domaines de la séméiologie médicale,  de la pharmacologie et des disciplines odontologiques ;
 - l'apprentissage du travail en équipe et des techniques de communication,  nécessaires à l'exercice professionnel ;

Elle permet également à l'étudiant d'apprendre à communiquer, à établir un
diagnostic, à concevoir une proposition thérapeutique, à comprendre une démarche
de soins coordonnés et à assurer les gestes de première urgence.

La formation comprend des enseignements théoriques, méthodologiques, appliqués
et pratiques ainsi que l’accomplissement d'un stage d'initiation aux soins de
quatre semaines à temps complet.

*Pour aller plus loin* :
[arrêté](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023850748&dateTexte=20180119)
du 22 mars 2011 relatif au régime des études en vue du diplôme de formation
générale en sciences odontologiques.

**Diplôme de formation approfondie en sciences odontologiques

Le deuxième cycle d’études de chirurgien-dentiste est sanctionné par le diplôme
de formation approfondie en sciences odontologiques et comprend quatre semestres
correspondant au niveau master.

Il a pour objectif :

 - l'acquisition des connaissances scientifiques, médicales et odontologiques  complétant et approfondissant celles acquises au cours du cycle précédent et
  nécessaires à l'acquisition des compétences pour l'ensemble des activités de
  prévention, de diagnostic et de traitement des maladies congénitales ou
  acquises, réelles ou supposées, de la bouche, des dents, des maxillaires et
  des tissus attenants ;
 - l'acquisition des connaissances pratiques et des compétences cliniques dans  le cadre des stages et de la formation pratique et clinique ;
 - une formation à la démarche scientifique ;
 - l'apprentissage du raisonnement clinique ;
 - l'apprentissage du travail en équipe pluriprofessionnelle, en particulier  avec les autres odontologistes ;
 - l'acquisition des techniques de communication indispensables à l'exercice  professionnel ;
 - la sensibilisation au développement professionnel continu comprenant  l'évaluation des pratiques professionnelles et l'approfondissement continu
  des connaissances.

Outre les enseignements théoriques et pratiques, la formation comprend
l'accomplissement de stages hospitaliers.

Le deuxième cycle est validé par la réussite au contrôle de connaissances des
enseignements dispensés au cours de la formation, ainsi que la délivrance d'un
certificat de synthèse clinique et thérapeutique.

*Pour aller plus loin* : articles 4 à 15 de
l'[arrêté](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027343802&dateTexte=20180119)
du 8 avril 2013 relatif au régime des études en vue du diplôme d'Etat de docteur
en chirurgie dentaire.

**Diplôme d’État de docteur en chirurgie dentaire

Le troisième cycle est sanctionné par la délivrance du diplôme d’État de docteur
en chirurgie dentaire. Il comporte :

 - un cycle court de deux semestres de formation ;
 - d'un cycle long de six à huit semestres de formation pour les étudiants reçusau concours de l'internat en odontologie ;
 - la soutenance d'une thèse.

D'une durée de deux semestres, le troisième cycle court est consacré à
l'approche globale du patient et à la préparation à l'exercice autonome de la
profession.

Il est accompagné d'un stage d'initiation à la vie professionnelle d'une durée
de 250 heures auprès d'un chirurgien-dentiste.

L'étudiant devra soutenir une thèse devant un jury à partir du deuxième semestre
du troisième cycle court et jusqu'à sa validation. Le DE de docteur en chirurgie
dentaire sera ainsi remis à l'étudiant qui a validé les enseignements du
troisième cycle et validé sa thèse.

*Pour aller plus loin* : article 16 et suivants de l'arrêté du 8 avril 2013
relatif au régime des études en vue du diplôme d’État de docteur en chirurgie
dentaire.

#### Coûts associés à la qualification

La formation menant à l’obtention du DE de docteur en chirurgie-dentaire est
payante. Son coût varie selon les universités qui dispensent les enseignements.
Pour plus d'informations, il est conseillé de se rapprocher de l’université
considérée.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Le professionnel ressortissant d’un État de l'UE ou de l'EEE qui est établi et
exerce légalement son activité dans l’un de ces États, peut, exercer en France,
de manière temporaire et occasionnelle, la même activité et ce, sans être
inscrit au tableau de l'ordre des chirurgiens-dentistes.

Pour cela, le professionnel doit effectuer une déclaration préalable, ainsi
qu'une déclaration justifiant qu'il possède les connaissances linguistiques
nécessaires pour exercer en France (cf. infra « 5°. a. Effectuer une déclaration
préalable d'activité pour les ressortissants de l'UE ou de l'EEE exerçant une
activité temporaire et occasionnelle (LPS) »).

**À savoir** : l’inscription au tableau de l’Ordre des chirurgiens-dentistes n’est
pas requise pour le professionnel en situation de libre prestation de services
(LPS). Il n'est donc pas tenu de s’acquitter des cotisations ordinales. Le
chirurgien-dentiste est simplement enregistré sur une liste spécifique tenue par
le Conseil national de l’Ordre.

La déclaration préalable doit être accompagnée d’une déclaration concernant les
connaissances linguistiques nécessaires à la réalisation de la prestation. Dans
cette hypothèse, le contrôle de la maîtrise de la langue doit être proportionné
à l’activité à exercer et réalisé une fois la qualification professionnelle
reconnue.

Lorsque les titres de formation ne bénéficient pas d’une reconnaissance
automatique (cf. supra « 2°. a. Législation nationale »), les qualifications
professionnelles du prestataire sont vérifiées avant la première prestation de
services. En cas de différences substantielles entre les qualifications de
l’intéressé et la formation exigée en France, qui seraient de nature à nuire à
la santé publique, le prestataire est soumis à une épreuve d’aptitude.

Le chirurgien-dentiste en situation de LPS est tenu de respecter les règles
professionnelles applicables en France, notamment l’ensemble des règles
déontologiques (cf. infra « 3°. Conditions d’honorabilité, règles
déontologiques, éthique »). Il est soumis à la juridiction disciplinaire de
l’Ordre des chirurgiens-dentistes.

**À noter** : la prestation est réalisée sous le titre professionnel français de
chirurgien-dentiste. Toutefois, lorsque les titres de formation ne bénéficient
pas d’une reconnaissance et dans le cas où les qualifications n’ont pas été
vérifiées, la prestation est réalisée sous le titre professionnel de l’État
d’établissement, de manière à éviter toute confusion avec le titre professionnel
français.

*Pour aller plus loin* : article L. 4112-7 du CSP.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

**Le régime de reconnaissance automatique du diplôme

L’article L. 4141-3 du Code de la santé publique crée un régime de
reconnaissance automatique en France de certains diplômes ou titres, le cas
échéant, accompagnés de certificats, obtenus dans un État de l’UE ou de l’EEE
(cf. supra « 2°. a. Législation nationale »).

Il appartient au conseil national de l’Ordre des chirurgiens-dentistes de
vérifier la régularité des diplômes, titres, certificats et attestations, d’en
accorder la reconnaissance automatique puis de statuer sur la demande
d’inscription au tableau de l’Ordre.

*Pour aller plus loin* : article L. 4151-5 du CSP.

**Le régime de l'autorisation individuelle d'exercer

Si le ressortissant de l’UE ou de l’EEE ne remplit pas les conditions pour
bénéficier du régime de reconnaissance automatique de ses titres ou diplômes, il
relève d’un régime d’autorisation d’exercice (cf. infra « 5°. b. Le cas échéant,
demander une autorisation individuelle d’exercice).

Les personnes qui ne bénéficient pas de la reconnaissance automatique mais qui
sont titulaires d’un titre de formation permettant d’exercer légalement la
profession de chirurgien-dentiste, peuvent être individuellement autorisées à
exercer en France, par le ministre de la santé, après avis d’une commission
composée notamment de professionnels.

Si l’examen des qualifications professionnelles attestées par les titres de
formation et l’expérience professionnelle fait apparaître des différences
substantielles avec les qualifications requises pour l’accès à la profession et
son exercice en France, l’intéressé doit se soumettre à une mesure de
compensation.

Selon le niveau de qualification exigé en France et celui détenu par
l’intéressé, l’autorité compétente peut soit :

 - proposer au demandeur de choisir entre un stage d’adaptation ou une épreuved’aptitude ;
 - imposer un stage d’adaptation et / ou une épreuve d’aptitude.

*Pour aller plus loin* : articles L. 4141-3-1 et R. 4111-14 et suivants du CSP.

3°. Conditions d'honorabilité, règles déontologiques, éthique
-------------------------------------------------------------

### a. Respect du Code de déontologie des chirurgiens-dentistes

Les dispositions du Code de déontologie s’imposent à tous les
chirurgiens-dentistes exerçant en France, qu'ils soient inscrits au tableau de
l’Ordre ou qu’ils soient dispensés de cette obligation (cf. supra : « 5°. b.
Demander son inscription au tableau de l’Ordre des chirurgiens-dentistes »).

**À savoir** : l’ensemble des dispositions du Code de déontologie est codifié aux
articles R. 4127-201 à R. 4127-284 du Code de la santé publique.

À ce titre, les chirurgiens-dentistes doivent notamment respecter les principes
de dignité, de non-discrimination, de secret professionnel ou encore
d'indépendance.

### b. Obligation de développement professionnel continu

Les chirurgiens-dentistes doivent participer annuellement à un programme de
développement professionnel continu. Ce programme vise à maintenir et actualiser
leurs connaissances et leurs compétences ainsi qu'à améliorer leurs pratiques
professionnelles.

À ce titre, le professionnel de santé (salarié ou libéral) doit justifier de son
engagement dans une démarche de développement professionnel. Le programme se
présente sous la forme de formations (présentielles, mixtes ou
non-présentielles) d’analyse, d’évaluation, et d’amélioration des pratiques et
de gestion des risques. L’ensemble des formations suivies est consigné dans un
document personnel contenant les attestations de formation.

*Pour aller plus loin* : article R. 4127-214 du CSP.

4°. Assurance
-------------

### a. Obligation de souscrire à une assurance de responsabilité civile professionnelle

En qualité de professionnel de santé, le chirurgien-dentiste exerçant à titre
libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que
facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire
pour ses salariés une telle assurance pour les actes effectués à l’occasion de
leur activité professionnelle.

*Pour aller plus loin* : article L. 1142-2 du Code de la santé publique.

### b. Obligation d’affiliation à caisse autonome de retraite des chirurgiens-dentistes et des sages-femmes (CARCDSF)

Tout chirurgien-dentiste inscrit au tableau de l'Ordre des chirurgiens-dentistes
et exerçant sous la forme libérale (même à temps partiel et même s’il exerce par
ailleurs une activité salariée) a l’obligation d’adhérer à la CARCDSF.

L’intéressé doit se déclarer à la CARCDSF dans le mois suivant le début de son
activité libérale.

*Pour aller plus loin* : article R. 643-1 du Code de la sécurité sociale ; site de
la [CARCDSF](http://www.carcdsf.fr/).

### c. Obligation de déclaration auprès de l’Assurance maladie

Une fois inscrit au tableau de l’Ordre, le chirurgien-dentiste exerçant sous
forme libérale doit déclarer son activité auprès de la Caisse primaire
d’assurance maladie (CPAM).

**Modalités** : l’inscription auprès de la CPAM peut être réalisée en ligne sur
le site officiel de l’Assurance maladie.

**Pièces justificatives** : le déclarant doit communiquer un dossier complet
comprenant :

 - la copie d’une pièce d’identité en cours de validité ;
 - l'attestation d'inscription au tableau de l'Ordre ;
 - un relevé d’identité bancaire (RIB) professionnel ;
 - le cas échéant, la notification de déclaration de l'installation radiologique.

Pour plus d’informations, il est conseillé de se reporter à la rubrique
consacrée à l’installation en libéral des chirurgiens-dentistes du site de
l’Assurance maladie.

5°. Démarche et formalités de reconnaissance de qualification
-------------------------------------------------------------

### a. Effectuer une déclaration préalable d'activité pour les ressortissants de l'UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

Tout ressortissant de l’UE ou de l’EEE qui est établi et exerce légalement les
activités de chirurgien-dentiste dans l’un de ces États peut exercer en France
de manière temporaire ou occasionnelle s’il en fait la déclaration préalable
(cf. supra 2° b. « Ressortissants UE et EEE : en vue d’un exercice temporaire et
occasionnel (Libre Prestation de Service) »).

La déclaration préalable doit être renouvelée tous les ans.

**À noter** : tout changement de situation du demandeur doit être notifié dans les
mêmes conditions.

**Autorité compétente** : la déclaration doit être adressée, avant la première
prestation de services, au Conseil national de l’Ordre des
chirurgiens-dentistes.

**Modalités de la déclaration et récépissé** : la déclaration peut être envoyée
par courrier ou directement effectuée en ligne sur le site officiel de l’Ordre
des chirurgiens-dentistes.

Lorsque le Conseil national de l’Ordre reçoit la déclaration et l’ensemble des
pièces justificatives nécessaires, il adresse au prestataire un récépissé
précisant son numéro d’enregistrement ainsi que la discipline exercée.

**À noter** : le prestataire de services informe préalablement l’organisme national
d’assurance maladie compétent de sa prestation de services par l’envoi d’une
copie de ce récépissé ou par tout autre moyen.

**Délai** : dans un délai d’un mois à compter de la réception de la déclaration,
le Conseil national de l’Ordre informe le demandeur :

 - qu’il peut ou non débuter la prestation de services ;
 - lorsque la vérification des qualifications professionnelles met en évidenceune différence substantielle avec la formation exigée en France, qu’il doit
prouver avoir acquis les connaissances et les compétences manquantes en se
soumettant à une épreuve d’aptitude. S’il satisfait à ce contrôle, il est
informé dans un délai d’un mois qu’il peut débuter la prestation de services ;
 - lorsque l’examen du dossier met en évidence une difficulté nécessitant uncomplément d’informations, des raisons du retard pris dans l’examen de son
dossier. Il dispose alors d’un délai d’un mois pour obtenir les compléments
d’informations demandés. Dans ce cas, avant la fin du 2ème mois à compter de la
réception de ces informations, le Conseil national informe le prestataire, après
réexamen de son dossier :

 - qu’il peut ou non débuter la prestation de services,
 - lorsque la vérification des qualifications professionnelles du prestataire  met en évidence une différence substantielle avec la formation exigée en
  France, qu’il doit démontrer qu’il a acquis les connaissances et compétences
  manquantes, notamment en se soumettant à une épreuve d’aptitude.

Dans cette dernière hypothèse, s’il satisfait à ce contrôle, il est informé dans
le délai d’un mois qu’il peut débuter la prestation de services. Dans le cas
contraire, il est informé qu’il ne peut pas débuter la prestation de services.
En l’absence de réponse du Conseil national de l’Ordre dans ces délais, la
prestation de services peut débuter.

**Pièces justificatives** : la déclaration préalable doit être accompagnée d’une
déclaration concernant les connaissances linguistiques nécessaires à la
réalisation de la prestation et des pièces justificatives suivantes :

 - le formulaire de déclaration préalable de prestation de services ;
 - la copie d’une pièce d’identité en cours de validité ou d’un documentattestant la nationalité du demandeur ;
 - la copie du ou des titres de formation, accompagnée, le cas échéant, d’unetraduction par un traducteur agréé ;
 - une attestation de l’autorité compétente de l’État d’établissement de l’UE oude l’EEE certifiant que l’intéressé est légalement établi dans cet État et qu’il
n’encourt aucune interdiction d’exercer, accompagnée, le cas échéant, d’une
traduction en français établie par un traducteur agréé.

**À noter** : le contrôle de la maîtrise de la langue doit être proportionné à
l’activité à exercer et réalisé une fois la qualification professionnelle
reconnue.

Coût : gratuit.

*Pour aller plus loin* : articles L. 4112-7, R. 4112-9 et suivants du CSP ; arrêté
du 20 janvier 2010 relatif à la déclaration préalable de prestation de services
pour l’exercice des professions de médecin, chirurgien-dentiste et sage-femme.

### b. Formalités pour les ressortissants de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Le cas échéant, demander une autorisation individuelle d’exercice

Si le ressortissant ne relève pas du régime de reconnaissance automatique de son
diplôme, il doit solliciter une autorisation d'exercer.

**Autorité compétente** : la demande est adressée en deux exemplaires, par
lettre recommandée avec demande d’avis de réception à la cellule chargée des
commissions d’autorisation d’exercice (CAE) du Centre national de gestion (CNG).

**Pièces justificatives** : le dossier de demande doit contenir l'ensemble des
pièces justificatives suivantes :

 - le formulaire de demande d’autorisation d’exercice de la profession ;
 - une photocopie d’une pièce d’identité en cours de validité ;
 - une copie du titre de formation permettant l’exercice de la profession dansl’État d’obtention ainsi que, le cas échéant, une copie du titre de formation de
spécialiste ;
 - le cas échéant, une copie des diplômes complémentaires ;
 - toutes pièces utiles justifiant des formations continues, de l’expérience etdes compétences acquises au cours de l’exercice professionnel dans un État de
l’UE ou de l’EEE, ou dans un État tiers (attestations de fonctions, bilan
d’activité, bilan opératoire, etc.) ;
 - dans le cadre de fonctions exercées dans un État autre que la France, unedéclaration de l’autorité compétente de cet État, datant de moins d’un an,
attestant de l’absence de sanctions à l’égard du demandeur.

Selon la situation du demandeur, d’autres pièces justificatives sont exigées.
Pour plus d’informations, il est conseillé de se reporter au site officiel du
CNG.

**À savoir** : les pièces justificatives doivent être rédigées en langue française
ou traduites par un traducteur agréé.

**Délai** : le CNG accuse réception de la demande dans le délai d’un mois à
compter de sa réception.

Le silence gardé pendant un certain délai à compter de la réception du dossier
complet vaut décision de rejet de la demande. Ce délai est porté à :

 - quatre mois pour les demandes présentées par les ressortissants de l’UE ou del’EEE titulaires d’un diplôme délivré dans l’un de ces États ;
 - six mois pour les demandes présentées par les ressortissants d’États tierstitulaires d’un diplôme délivré par un État de l’UE ou de l’EEE ;
 - un an pour les autres demandes.

Ce délai peut être prolongé de deux mois, par décision de l’autorité
ministérielle notifiée au plus tard un mois avant l’expiration de celui-ci, en
cas de difficulté sérieuse portant sur l’appréciation de l’expérience
professionnelle du candidat.

*Pour aller plus loin* : arrêté du 25 février 2010 fixant la composition du
dossier à fournir aux commissions d'autorisation d'exercice compétentes pour
l'examen des demandes présentées en vue de l'exercice en France des professions
de médecin, chirurgien-dentiste, sage-femme et pharmacien.

**Bon à savoir** : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation et
l'expérience professionnelle du ressortissant et celles requises pour exercer en
France, la CNG peut décider soit :

 - de proposer au demandeur de choisir entre un stage d’adaptation ou une épreuved’aptitude ;
 - d'imposer un stage d’adaptation et / ou une épreuve d’aptitude.

L’épreuve d’aptitude a pour objet de vérifier, par des épreuves écrites ou
orales ou par des exercices pratiques, l’aptitude du demandeur à exercer la
profession de chirurgien-dentiste. Elle porte sur les matières qui ne sont pas
couvertes par le ou les titres de formation du demandeur ou son expérience
professionnelle.

Le stage d’adaptation a pour objet de permettre aux intéressés d’acquérir les
compétences nécessaires à l’exercice de la profession de chirurgien-dentiste. Il
est accompli sous la responsabilité d’un chirurgien-dentiste et peut être
accompagné d’une formation théorique complémentaire facultative. La durée du
stage n’excède pas trois ans. Il peut être effectué à temps partiel.

*Pour aller plus loin* : articles R. 4111-14 et R. 4111-17 à R. 4111-20 du CSP.

**Demander son inscription au tableau de l'Ordre des chirurgiens-dentistes

L'inscription au tableau de l'Ordre est obligatoire pour exercer légalement
l'activité de chirurgien-dentiste en France.

L'inscription ne s'applique pas :

 - aux ressortissants de l’UE ou de l’EEE qui sont établis et qui exercentlégalement l’activité de chirurgien-dentiste dans un État membre ou partie,
lorsqu’ils exécutent en France, de manière temporaire et occasionnelle, des
actes de leur profession (cf. supra « 2°. b. Ressortissants UE et EEE : en vue
d’un exercice temporaire et occasionnel ») ;
 - aux chirurgiens-dentistes appartenant aux cadres actifs du service de santédes armées ;
 - aux chirurgiens-dentistes qui, ayant la qualité de fonctionnaire de l’État oud’agent titulaire d’une collectivité locale, ne sont pas appelés, dans
l’exercice de leurs fonctions, à exercer la chirurgie-dentaire.

**À noter** : l’inscription au tableau de l’Ordre permet la délivrance automatique
et gratuite de la carte de professionnel de santé (CPS). La CPS est une carte
d’identité professionnelle électronique. Elle est protégée par un Code
confidentiel et contient notamment les données d’identification de
chirurgien-dentiste (identité, profession, spécialité). Pour plus
d’informations, il est recommandé de se reporter au site gouvernemental de
l’Agence française de la santé numérique.

**Autorité compétente** : la demande d’inscription est adressée au président du
conseil de l’Ordre des chirurgiens-dentistes du département dans lequel
l’intéressé souhaite établir sa résidence professionnelle.

La demande peut être directement déposée au conseil départemental de l’Ordre
concerné ou lui être adressée par courrier recommandé avec demande d’avis de
réception.

**À savoir** : en cas de transfert de sa résidence professionnelle hors du
département, le praticien est tenu de demander sa radiation du tableau de
l’Ordre du département où il exerçait et son inscription au tableau de l’Ordre
de sa nouvelle résidence professionnelle.

**Procédure** : à la réception de la demande, le conseil départemental désigne
un rapporteur qui procède à l’instruction de la demande et fait un rapport
écrit. Le conseil vérifie les titres du candidat et demande communication du
bulletin n° 2 du casier judiciaire de l’intéressé. Il vérifie notamment que le
candidat :

 - remplit les conditions nécessaires de moralité et d’indépendance ;
 - remplit les conditions nécessaires de compétence ;
 - ne présente pas une infirmité ou un état pathologique incompatible avecl’exercice de la profession (cf. supra « 3°. e. Aptitude physique »).

En cas de doute sérieux sur la compétence professionnelle du demandeur ou sur
l’existence d’une infirmité ou d’un état pathologique incompatible avec
l’exercice de la profession, le conseil départemental saisit le conseil régional
ou interrégional qui diligente une expertise. S’il est constaté, au vu du
rapport d’expertise, une insuffisance professionnelle rendant dangereux
l’exercice de la profession, le conseil départemental refuse l’inscription et
précise les obligations de formation du praticien.

Aucune décision de refus d’inscription ne peut être prise sans que l’intéressé
ait été invité quinze jours au moins à l’avance par lettre recommandée avec
demande d’avis de réception à comparaître devant le conseil pour y présenter ses
explications.

La décision du conseil de l’Ordre est notifiée, dans la semaine qui suit, à
l’intéressé au Conseil national de l’Ordre des chirurgiens-dentistes et au
directeur général de l’agence régionale de santé (ARS). La notification se fait
par lettre recommandée avec demande d’avis de réception.

La notification mentionne les voies de recours contre la décision. La décision
de refus doit être motivée.

**Délai** : le président accuse réception du dossier complet dans un délai d’un
mois à compter de son enregistrement.

Le conseil départemental de l’Ordre doit statuer sur la demande d’inscription
dans un délai maximum de trois mois à compter de la réception du dossier complet
de demande. À défaut de réponse dans ce délai, la demande d’inscription est
réputée rejetée.

Ce délai est porté à six mois pour les ressortissants des États tiers lorsqu’il
y a lieu de procéder à une enquête hors de la France métropolitaine. L’intéressé
en est alors avisé.

Il peut également être prorogé d’une durée qui ne peut excéder deux mois par le
conseil départemental lorsqu’une expertise a été ordonnée.

**Pièces justificatives** : l’intéressé doit adresser un dossier complet de
demande d’inscription comprenant :

 - deux exemplaires du questionnaire normalisé avec une photo d’identité, rempli,daté et signé, disponible dans les conseils départementaux de l’Ordre ;
 - une photocopie d’une pièce d’identité en cours de validité ou, le cas échéant,une attestation de nationalité délivrée par une autorité compétente ;
 - le cas échéant, une photocopie de la carte de séjour de membre de la familled’un citoyen de l’UE en cours de validité, de la carte de résident de longue
durée-CE en cours de validité ou de la carte de résident portant mention du
statut de réfugié en cours de validité ;
 - le cas échéant, une photocopie d'une attestation de nationalité en cours devalidité ;
 - une copie, accompagnée le cas échéant d’une traduction, faite par untraducteur agréé, des titres de formation à laquelle sont joints :

 - lorsque le demandeur est un ressortissant de l’UE ou de l’EEE, la ou les  attestations prévues (cf. supra « 2°. a. Exigences nationales »),
 - lorsque le demandeur bénéficie d’une autorisation d’exercice individuelle  (cf. supra « 2°. c. Ressortissants UE et EEE : en vue d’un exercice
  permanent »), la copie de cette autorisation,
 - lorsque le demandeur présente un diplôme délivré dans un État étranger dont  la validité est reconnue sur le territoire français, la copie des titres à
  la possession desquels cette reconnaissance peut être subordonnée ;
 - pour les ressortissants d’un État tiers, un extrait de casier judiciaire ou undocument équivalent datant de moins de trois mois, délivré par une autorité
compétente de l’État d’origine. Cette pièce peut être remplacée, pour les
ressortissants des États de l’UE ou de l’EEE qui exigent une preuve de moralité
ou d’honorabilité pour l’accès à l’activité de médecin, par une attestation,
datant de moins de trois mois, de l’autorité compétente de l’État d’origine
certifiant que ces conditions de moralité ou d’honorabilité sont remplies ;
 - une déclaration sur l’honneur du demandeur certifiant qu’aucune instancepouvant donner lieu à condamnation ou sanction susceptible d’avoir des
conséquences sur l’inscription au tableau n’est en cours à son encontre ;
 - un certificat de radiation d’inscription ou d’enregistrement délivré parl’autorité auprès de laquelle le demandeur était antérieurement inscrit ou
enregistré ou, à défaut, une déclaration sur l’honneur du demandeur certifiant
qu’il n’a jamais été inscrit ou enregistré ou, à défaut, un certificat
d’inscription ou d’enregistrement dans un État de l’UE ou de l’EEE ;
 - tous les éléments de nature à établir que le demandeur possède lesconnaissances linguistiques nécessaires à l’exercice de la profession ;
 - un curriculum vitae.

**Voies de recours** : le demandeur ou le Conseil national de l’Ordre des
chirurgiens-dentistes peuvent contester la décision d’inscription ou de refus
d’inscription dans un délai de 30 jours à compter de la notification de la
décision ou de la décision implicite de rejet. L’appel est porté devant le
conseil régional territorialement compétent.

Le conseil régional doit statuer dans un délai de deux mois à compter de la
réception de la demande. À défaut de décision dans ce délai, le recours est
réputé rejeté.

La décision du conseil régional est également susceptible d’appel, dans les 30
jours, auprès du Conseil national de l’Ordre des chirurgiens-dentistes. La
décision ainsi rendue peut elle-même faire l’objet d’un recours devant le
Conseil d’État.

Coût : l’inscription au tableau de l’Ordre est gratuite mais elle engendre
l’obligation de s’acquitter de la cotisation ordinale obligatoire dont le
montant est fixé annuellement et qui doit être versée au cours du premier
trimestre de l’année civile en cours. Le paiement peut s’effectuer en ligne sur
le site officiel du Conseil national de l’Ordre des chirurgiens-dentistes. À
titre indicatif, le montant de cette cotisation s’élevait à 422 euros en 2017.

*Pour aller plus loin* : articles L. 4112-1 à L. 4112-6, et R. 4112-1 à R. 4112-20
du CSP.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’informations sur la reconnaissance
académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État
membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de
trouver une solution à un différend opposant un ressortissant de l’UE à
l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière
de reconnaissance des qualifications professionnelles.

**Conditions** : l’intéressé ne peut recourir à SOLVIT que s’il établit :

 - que l’Administration publique d’un État de l’UE n’a pas respecté les droitsque la législation européenne lui confère en tant que citoyen ou entreprise d’un
autre État de l’UE ;
 - et qu’il n’a pas déjà initié d’action judiciaire (le recours administratifn’est pas considéré comme tel).

**Procédure** : le ressortissant doit remplir un [formulaire de plainte en
ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine
pour demander, si nécessaire, des informations supplémentaires et pour vérifier
que le problème relève bien de sa compétence.

**Pièces justificatives** : pour saisir SOLVIT, le ressortissant doit
communiquer :

 - ses coordonnées complètes ;
 - la description détaillée de son problème ;
 - l’ensemble des éléments de preuve du dossier (par exemple, la correspondanceet les décisions reçues de l’autorité administrative concernée).

**Délai** : SOLVIT s’engage à trouver une solution dans un délai de dix semaines
à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays
dans lequel est survenu le problème.

**Coût** : gratuit.

**Issue de la procédure** : à l’issue du délai de dix semaines, le SOLVIT
présente une solution :

 - si cette solution règle le différend portant sur l’application du droiteuropéen, la solution est acceptée et le dossier est clos ;
 - s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyévers la Commission européenne.

Informations supplémentaires : SOLVIT en France : Secrétariat général des
affaires européennes, 68 rue de Bellechasse, 75700 Paris, [site
officiel](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

