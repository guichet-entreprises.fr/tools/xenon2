﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->

<!-- var(site:home)="Accueil" -->

Premier exemple de site
===================================================

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget egestas mi. Praesent facilisis nunc id accumsan tempor. Nunc pharetra tempus mi, vel placerat nunc congue non. Vestibulum tristique risus diam. Proin ultricies a metus vel finibus. Suspendisse ut sodales lectus. Proin tincidunt quam nibh, id pretium magna faucibus nec. Nulla ac blandit ipsum. Nam sed varius felis, rhoncus cursus nulla. Donec placerat lacinia ultrices. Proin quis ex et eros molestie tincidunt ut ut leo. Sed porttitor vehicula lectus. Quisque convallis fringilla tellus nec pellentesque. Etiam cursus justo eros, ac consequat magna semper eget. Etiam maximus fringilla laoreet. Praesent congue, elit nec pellentesque convallis, arcu sapien mollis lacus, eget volutpat lectus tortor eu mauris.

Ut dapibus dapibus laoreet. Duis tincidunt, ipsum eget dictum scelerisque, odio lorem volutpat risus, non semper velit ex at massa. Pellentesque nisi neque, ornare ut tellus consequat, rutrum placerat nulla. Ut sit amet elit tellus. Etiam commodo nibh pretium nunc ornare bibendum. Nullam id erat lacinia, accumsan odio vel, ullamcorper justo. Nulla rutrum ultrices orci sed lacinia. Vestibulum ac molestie nisi. Etiam sollicitudin id neque at tempor. Pellentesque suscipit dictum eros, id iaculis mi imperdiet non. Nulla condimentum, lorem sit amet dictum rhoncus, nisl ipsum finibus augue, a accumsan lorem mauris at nisi. Duis dignissim ultricies elit ac rhoncus. Suspendisse eu velit nec quam commodo faucibus. Fusce venenatis vehicula placerat. Phasellus gravida at leo mollis pretium.

Aliquam fermentum felis sed erat pellentesque aliquam. Phasellus consequat elit nisl, ac sodales diam sagittis nec. In a consectetur nisl. In iaculis egestas aliquam. Cras eget sapien sed enim ultricies venenatis. Curabitur nulla tortor, varius tincidunt elementum quis, hendrerit in lacus. Sed dictum tempor quam vel tristique. Proin consequat non velit efficitur dictum. Integer placerat, odio a tempus ultrices, ligula sapien ultrices mauris, in pellentesque enim neque vitae velit. Quisque justo justo, consectetur porta tortor vitae, consequat pretium libero. Maecenas gravida justo arcu, facilisis ultrices libero facilisis eget. Aenean est risus, luctus ut luctus et, cursus vel leo. Fusce quis justo malesuada, facilisis nulla a, lacinia enim. Vestibulum malesuada hendrerit laoreet. Phasellus condimentum ornare arcu, in lobortis metus varius a.

Proin laoreet, erat egestas luctus consequat, augue lectus venenatis purus, nec pharetra nulla sapien non felis. Suspendisse venenatis orci et ligula gravida malesuada. Aenean tempus enim sed consequat tempus. Vivamus sapien nisi, vehicula vel volutpat vel, aliquam luctus ante. Pellentesque erat massa, vestibulum et molestie vel, aliquet et massa. Nunc imperdiet justo ligula, eu porttitor ipsum placerat vitae. Morbi tellus odio, elementum vitae libero vel, pretium pellentesque neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam placerat pellentesque nulla nec vehicula. Suspendisse vestibulum felis ante, auctor condimentum libero porttitor a. In hac habitasse platea dictumst. Proin libero tellus, pellentesque in mollis ac, condimentum at ligula. Donec a eleifend nisi.

Nulla facilisis fermentum nunc vel pharetra. Donec blandit lacus nisl, a fringilla justo rhoncus eget. Integer et libero turpis. Proin finibus libero ac neque mollis facilisis. Proin feugiat fermentum justo eget dignissim. Mauris condimentum libero odio, maximus luctus elit scelerisque in. Suspendisse ultricies sed lorem quis accumsan. Mauris non pellentesque nibh. 
