<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Services" -->
<!-- var(keywords)="Services" -->

Services
========

## Les différents services du site Guichet Partenaires

Les différents services du site guichet-partenaires.fr auxquels vous avez accès dépendent de votre profil.
Vous trouverez ci-dessous la liste des fonctionnalités disponibles :

### Utilisateur 

Vous avez été identifié comme utilisateur par votre référent réseau.
Sur www.guichet-partenaires.fr, vous pouvez dès à présent :

* accéder aux dossiers des déclarants et les télécharger ;
* les traiter sous la forme souhaitée (en version dématérialisée ou papier) ;
* les trier ;
* les archiver ;
* nous contacter.

Vous aurez ainsi une visibilité complète sur l’ensemble des dossiers que vous avez traités et qu’il vous reste à traiter.
Vous pouvez ainsi administrer et organiser selon vos besoins le traitement des dossiers qui vous sont destinés. 

D'autres fonctionnalités vous seront proposées prochainement, et notamment :

* nous faire remonter votre expérience de navigation et de traitement des dossiers sur guichet-partenaires.fr au moyen d'un formulaire de satisfaction ;
* nous suggérer des modifications de notre site.

### Référent système de paiement

Vous avez été identifié comme « référent système de paiement » par votre référent réseau.
Sur www.guichet-partenaires.fr, vous pouvez désormais mettre à jour les informations de paiement de votre organisme en ligne, de façon simple et rapide, et consulter les dernières écritures pour votre organisme.

### Référent

Vous avez été identifié comme « référent » de votre réseau par le service Guichet Entreprises ou par votre tête de réseau.
Sur Guichet Partenaires, vous pouvez dès à présent : 

* accéder aux dossiers des déclarants  adressés à votre entité et les télécharger ;
* les traiter sous la forme souhaitée (en version dématérialisée ou papier) ;
* les archiver ;
* créer un nouvel utilisateur ;
* nous contacter ;
* modifier vos canaux et vos coordonnées de transmission (dossiers dématérialisés 
transmis par courriel, le canal FTP ou la fonction *back office*, ou en version papier) ;
* modifier les informations de paiement de votre entité.

Vous aurez ainsi une visibilité complète sur l’ensemble des dossiers que votre organisme a traités et ceux qu'il lui reste à traiter.
Vous pouvez ainsi administrer et organiser le traitement des dossiers à destination de votre organisme selon ses besoins.

D'autres fonctionnalités vous seront proposées prochainement, et notamment :

* nous faire remonter votre expérience de navigation et de traitement des dossiers sur guichet-partenaires.fr au moyen d'un formulaire de satisfaction ;
* nous suggérer des modifications de notre site.

### Tête de réseau

Vous avez été identifié comme tête de réseau par le service Guichet Entreprises.
Sur www.guichet-partenaires.fr, vous pouvez dès à présent :

* accéder aux dossiers des déclarants de votre réseau et les télécharger ;
* les traiter sous la forme souhaitée (en version dématérialisée ou papier) ;
* les archiver ;
* créer un nouveau référent ou un nouvel utilisateur ;
* nous contacter ;
* modifier vos canaux de transmission (dossiers dématérialisés transmis par courriel, le canal FTP ou la fonction *back office*, ou en version papier) ;
* modifier les informations de paiement de votre réseau.

D'autres fonctionnalités vous seront proposées prochainement, et notamment :

* modifier vos information de contact ;
* nous faire remonter votre expérience de navigation et de traitement des dossiers sur www.guichet-partenaires.fr au moyen d'un formulaire de satisfaction ;
* nous suggérer des modifications de notre site.

### Bureaux réglementaires

Si vous faites partie d'un bureau réglementaire, vous pouvez contacter le service Guichet Entreprises afin de nous informer des évolutions réglementaires relatives aux professions et activités réglementées en France.
