<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="La réception des dossiers" -->
<!-- var(keywords)="La réception des dossiers" -->

La réception des dossiers
========================

Le site www.guichet-partenaires.fr vous permet de recevoir et traiter les dossiers déposés par les déclarants sur les sites [www.guichet-entreprises.fr](https://www.guichet-entreprises.fr) et [www.guichet-qualifications.fr](https://www.guichet-qualifications.fr).

## Choisir son canal de réception

Votre référent réseau a la possibilité de choisir le canal de réception qui convient le mieux à votre organisme :

* les canaux de réception en ligne : ces canaux vous permettent de recevoir les dossiers des déclarants de façon dématérialisée, de les consulter en ligne et de les archiver.
Ils vous offriront une vision complète des dossiers traités ou à traiter. Trois possibilités de réception en ligne s'offrent à vous :
  + le canal FTP (*File Transfer Protocol* ou protocole de transfert de fichier) dénommé Eddie,
  + le canal *back office* du site www.guichet-partenaires.fr,
  + le canal courriel ;
* le canal de réception par courrier avec accusé de réception.

## Traitement des dossiers

Une fois votre compte Guichet Partenaires créé et validé, vous serez redirigé vers votre espace dédié.
Celui-ci se présente sous la forme d'un tableau de bord affichant les dossiers dont vous avez la charge.
Chaque dossier comporte une référence, la date de transmission, la dénomination (exemple : déclaration de début d'activité agricole), ainsi que son état (« À traiter », « Traité » et « Archivé »).

Selon l'état des dossiers dont vous êtes responsable, jusqu'à quatre actions vous sont proposées : « Consulter », « Télécharger », « Traiter » et « Archiver ».

## Délais légaux

Conformément aux articles [R. 123-25](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256049&dateTexte=&categorieLien=cid) et [R. 123-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255899&dateTexte=&categorieLien=cid) du Code de commerce, les organismes destinataires sont tenus de traiter les dossiers transmis par le service Guichet Entreprises.
Conformément à l'[article 123-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255899&dateTexte=&categorieLien=cid) du Code de commerce, « le centre de formalités des entreprises compétent, saisi du dossier complet conformément aux dispositions de l'[article R. 123-7](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255891&dateTexte=&categorieLien=cid), transmet le jour même aux organismes destinataires, et le cas échéant aux autorités habilitées à délivrer les autorisations, les informations et pièces les concernant. »
