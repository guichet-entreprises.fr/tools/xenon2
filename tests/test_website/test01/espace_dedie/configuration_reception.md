<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Configuration du canal de réception des dossiers" -->
<!-- var(keywords)="Configuration" -->

Configuration du canal de réception des dossiers
=========================================

Votre référent réseau a la possibilité de choisir le canal de réception qui convient le mieux à votre autorité :

* le canal de réception par courrier avec accusé de réception ;
* les canaux de réception en ligne : ces canaux vous permettent de recevoir les dossiers des déclarants de façon dématérialisée, de les consulter en ligne et de les archiver.
Ils vous offriront une vision complète des dossiers traités ou à traiter. Trois possibilités de réception en ligne s'offrent à vous :
  + le canal courriel,
  + le canal *back office* du site www.guichet-partenaires.fr,
  + le canal FTP (*File Transfer Protocol* ou protocole de transfert de fichier).

Pour configurer le canal de réception de votre autorité, veuillez vous rendre sur le tableau de bord Guichet Partenaires et **cliquer sur « Configurer les canaux de transmission » dans le bouton « Services »** en haut à droite du tableau de bord.

![dashboard](file://Dashboard_GP.png)

Veuillez ensuite **sélectionner l'autorité compétente que vous souhaitez configurer dans le menu déroulant** :

![Sélection de l'AC](file://Selection_de_l_AC.png)

Vous pouvez désormais **choisir votre mode de réception** parmi les quatre modes proposés dans le menu « Choix du canal à configurer » :

![Sélection du canal de réception](file://Selection_du_canal_de_reception.png)

## Recevoir les dossiers par courrier postal

Pour recevoir les dossiers par courrier postal, après avoir cliqué sur le bouton « Vous souhaitez recevoir le dossier par courrier postal » dans le menu « Choix du canal à configurer », **veuillez cocher la case « Activer ce canal » dans l'écran qui s'affiche et renseigner les informations de votre autorité** :

![Ecran courrier postal](file://Ecran_courrier_postal.png)

## Recevoir les dossiers par courrier électronique

Pour recevoir les dossiers par courrier électronique, après avoir cliqué sur le bouton « Vous souhaitez recevoir le dossier par courrier électronique » dans le menu « Choix du canal à configurer »**veuillez cocher la case « Activer ce canal » dans l'écran qui s'affiche et renseigner l'adresse courriel de votre autorité** :

![Ecran email](file://Ecran_email.png)

## Recevoir les dossiers dans le *backoffice* partenaires

Pour recevoir les dossiers dans votre espace *backoffice* partenaires, après avoir cliqué sur le bouton « Vous souhaitez recevoir le dossier dans le backoffice partenaires »

## Recevoir les dossiers via le canal FTP

Pour recevoir les dossiers via le canal FTP, après avoir cliqué sur le bouton « Vous souhaitez recevoir le dossier via FTP », trois options s'offrent à vous :

* recevoir les dossier sur le serveur FTP de votre autorité ;
* récupérer les dossiers sur le serveur FTP du service Guichet Entreprises ;
* utiliser le canal FTP d'une autre autorité.

![Ecran FTP](file://Ecran_FTP.png)

### Recevoir les dossier sur le serveur FTP de votre autorité (mode « Push »)

Pour recevoir les dossier sur le serveur FTP de votre autorité, deux modes d'authentification s'offrent à vous :

* l'authentification par nom d'utilisateur et mot de passe ;
* l'authentification par clé RSA.

![FTP mode push](file://FTP_mode_push.png)

#### Nom d'utilisateur et mot de passe

Si vous avez choisi l'option nom d'utilisateur/mot de passe, veuillez cocher la case « Activer ce canal » et saisir les informations suivantes :

* un commentaire (optionnel) ;
* le chemin d'un sous-dossier dans le serveur FTP ;
* le mot de passe de votre compte FTP.

![FTP User mot de passe](file://FTP_User_mot_de_passe.png)

#### Clé RSA

Si vous avez choisi l'option clé RSA, veuillez cocher la case « Activer ce canal » et saisir les informations suivantes :

* l'identifiant du canal FTP ;
* un commentaire (optionnel) ;
* le chemin d'un sous-dossier dans le serveur FTP ;
* votre clé publique RSA.

![FTP clé RSA](file://FTP_cle_RSA.png)

### Récupérer les dossiers sur le serveur FTP du service Guichet Entreprises

Pour recevoir les dossier sur le serveur FTP de votre autorité, deux modes d'authentification s'offrent à vous :

* l'authentification par nom d'utilisateur et mot de passe ;
* l'authentification par clé RSA.

#### Nom d'utilisateur et mot de passe

Si vous avez choisi l'option nom d'utilisateur/mot de passe, veuillez cocher la case « Activer ce canal » et saisir les informations suivantes :

* l'identifiant du canal FTP ;
* un commentaire (optionnel) ;
* le chemin d'un sous-dossier dans le serveur FTP ;
* le mot de passe de votre compte FTP.

#### Clé RSA

Si vous avez choisi l'option clé RSA, veuillez cocher la case « Activer ce canal » et saisir les informations suivantes :

* l'identifiant du canal FTP ;
* un commentaire (optionnel) ;
* le chemin d'un sous-dossier dans le serveur FTP ;
* votre clé publique RSA.

### Utiliser le canal FTP d'une autre autorité

Pour recevoir les dossier sur le serveur FTP d'une autre autorité, veuillez cliquer sur « Vous souhaitez utiliser la canal FTP d'une autre autorité », cocher la case « Activer le canal » et renseigner les informations suivantes :

* le chemin d'un sous-dossier dans le serveur FTP ;
* l'autorité propriétaire du canal ;
* votre autorité.

![FTP autre autorité](file://FTP_autre_autorite.png)
