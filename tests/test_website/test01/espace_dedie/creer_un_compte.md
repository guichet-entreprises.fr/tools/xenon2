<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Créer un compte" -->
<!-- var(keywords)="Créer un compte" -->

Créer un compte
==============
*Pour profiter de l'ensemble des fonctionnalités du site Guichet Partenaires, nous vous recommandons d'utiliser une version récente des navigateurs Mozilla Firefox et Google Chrome.*

## Utilisateur

Si vous êtes agent d'un organisme partenaire du service Guichet Entreprises et que vous désirez instruire des dossiers depuis le site Guichet Partenaires, vous devez faire une demande de création de compte auprès de votre référent réseau, seule personne habilitée à effectuer cette action.

## Référent

Si le service Guichet Entreprises vous a identifié comme « référent » de votre réseau, vous devriez avoir reçu un courriel contenant un lien vous permettant de créer puis de valider un compte sur www.guichet-partenaires.fr.
Il vous suffit alors de cliquer sur le lien et de suivre les étapes de création de compte.
Si vous n'avez pas reçu le courriel ou que le lien est inactif, veuillez [contacter l'assistance utilisateur](Contacter_l_assistance_utilisateur.md).

Une fois votre compte créé, vous pourrez créer des comptes « utilisateur » et « référent système de paiement » pour les agents de votre organisme.
Pour cela, il vous suffit de cliquer sur le lien suivant : [formalité de création de compte](LIEN).

Pour plus d'informations sur les profils du site Guichet Partenaires, veuillez consulter la page « [Je suis partenaire](Je_suis_partenaire.md) ».



Navigation de l'article
----------------------