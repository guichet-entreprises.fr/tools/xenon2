<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Transmission des dossiers aux partenaires du service Guichet Entreprises" -->
<!-- var(keywords)="Transmission partenaires" -->

Transmission des dossiers aux partenaires du service Guichet Entreprises
=======================================================================

## Mode de transmission

La transmission des dossiers déposés par les déclarants peut s'effectuer par différents moyens.
Chaque organisme peut choisir le canal de transmission qui convient le mieux à son fonctionnement, à savoir :

- par courrier ;
- par le FTP (*File Tansfer Protocol* ou protocole de transfert de fichier) Eddie ;
- par la fonction *back office* des sites Guichet Entreprises et Guichet Qualifications ;
- par courriel.

Si aucun canal n'a été choisi, les dossiers sont par défaut envoyés à votre organisme par courrier.

## Configuration d'un canal de transmission

Pour modifier le canal de transmission ou vos coordonnées, connectez-vous sur votre espace dédié.

## Support

Un problème technique ?
Le service Guichet Entreprises est offre un service de support joignable  par les déclarants comme par les partenaires du service, de 9 h à 18 h, jours ouvrés.
Veuillez vous rendre sur la page [support](support_GP.md) pour le contacter.

**[RAJOUTER INFOGRAPHIE]**

Navigation de l'article
-----------------------