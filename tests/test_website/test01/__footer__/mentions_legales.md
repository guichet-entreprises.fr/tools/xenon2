﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Mentions légales" -->
<!-- var(keywords)="Mentions légales" -->

<!-- var(page:breadcrumb)="Mentions légales" -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(collapsable)="close" -->

Mentions légales <!-- collapsable:close -->
================

Identification de l’éditeur  <!-- collapsable:open --> 
---------------------------

Service à compétence nationale Guichet Entreprises

120 rue de Bercy, 75572 Paris Cedex 12

Le service Guichet Entreprises, créé par l’arrêté du 22 avril 2015 ([JORF du 22 avril 2015](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A1B17F7C2CD76682B8F5E3E5CE690458.tpdila13v_1?cidTexte=JORFTEXT000030517635&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000030517419)), est placé sous l’autorité de la direction générale des entreprises du ministère de l’Économie et des Finances.

La conception éditoriale, le suivi, la maintenance technique et les mises à jour du site internet www.guichet-partenaires.fr sont assurés par le service Guichet Entreprises.

Directeur de la publication 
---------------------------

Monsieur Florent Tournois, chef du service Guichet Entreprises

Prestataire d’hébergement 
-------------------------

Cloud Temple, 215 avenue Georges Clémenceau 92024 Nanterre (01 41 91 77 77)

Informatique et libertés
------------------------

En application de la loi Informatique et Libertés du 6 janvier 1978, vous disposez d’un droit d’accès, de rectification, de modification et de suppression des données qui vous concernent. Vous pouvez exercer ce droit de plusieurs façons :

- en envoyant un courriel au support : support.guichet-entreprises[at]helpline.fr ;
- en envoyant un courrier à l'adresse suivante :


> Service Guichet Entreprises

> Bâtiment Necker

> 120 rue de Bercy – Télédoc 766  

> 75572 Paris cedex 12

Droits de reproduction
----------------------

Le contenu de ce site relève de la législation française et internationale sur le droit d’auteur et la propriété intellectuelle.

L’ensemble des éléments graphiques du site est la propriété du service Guichet Entreprises. Toute reproduction ou adaptation des pages du site qui en reprendrait les éléments graphiques est strictement interdite.

Toute utilisation des contenus à des fins commerciales est également interdite.

Toute citation ou reprise de contenus du site doit avoir obtenu l’autorisation du directeur de la publication. La source (www.guichet-partenaires.fr) et la date de la copie devront être indiquées ainsi que la mention du service « Guichet Entreprises ».

Liens vers les pages du site
----------------------------

Tout site public ou privé est autorisé à établir des liens vers les pages du site www.guichet-partenaires.fr. Il n’y a pas à demander d’autorisation préalable. Cependant, l’origine des informations devra être précisée, par exemple sous la forme : « Création d’entreprise (source : www.guichet-partenaires.fr, un site du service Guichet Entreprises) ». Les pages du site www.guichet-partenaires.fr ne devront pas être imbriquées à l’intérieur des pages d’un autre site. Elles devront être affichées dans une nouvelle fenêtre ou un nouvel onglet.

Liens vers les pages de sites extérieurs
----------------------------------------

Les liens présents sur le site www.guichet-partenaires.fr peuvent orienter l’utilisateur sur des sites extérieurs dont le contenu ne peut en aucune manière engager la responsabilité du SCN Guichet Entreprises.

Environnement technique
-----------------------

Nous vous recommandons l'utilisation d'une version récente des navigateurs Mozilla Firefox et Google Chrome pour bénéficier de toutes les fonctionnalités du site Guichet Partenaires.

