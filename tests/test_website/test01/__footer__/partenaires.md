<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Partenaires" -->
<!-- var(keywords)="Partenaires" -->

Nos partenaires
===============

Le réseau
---------

Naturellement proches des enjeux des entrepreneurs dont ils sont les premiers interlocuteurs, les centres de formalités des entreprises (CFE) sont les partenaires privilégiés du service Guichet Entreprises. Leur travail commun permet aujourd’hui aux entrepreneurs français et européens d'effectuer l'ensemble de leurs démarches facilement et rapidement, en ligne.

De nombreux acteurs sont aussi impliqués dans la reconnaissance des qualifications professionnelles des ressortissants de l'Union européenne et de l'Espace économique européen  désireux de travailler de manière temporaire ([libre prestation de services](https://www.guichet-entreprises.fr/fr/eugo/libre-prestation-de-services-lps/)) ou permanente ([libre établissement](https://www.guichet-entreprises.fr/fr/eugo/libre-etablissement-le/)) en France.

Nos partenaires (liste non exhaustive) :

* l'[Agence France Entrepreneur (AFE)](https://www.guichet-entreprises.fr/fr/partenaires/agence-france-entrepreneur/) ;
* les [chambres d’agriculture (CA)](https://www.guichet-entreprises.fr/fr/partenaires/chambres-d-agriculture-ca/) ;
* les [chambres de commerce et d’industrie (CCI)](https://www.guichet-entreprises.fr/fr/partenaires/chambres-de-commerce-et-d-industrie-cci/) ;
* les [chambres de métiers et de l’artisanat (CMA)](https://www.guichet-entreprises.fr/fr/partenaires/chambres-de-metiers-et-de-l-artisanat-cma/) ;
* la [Chambre nationale de la batellerie artisanale (CNBA)](https://www.guichet-entreprises.fr/fr/partenaires/chambre-nationale-de-la-batellerie-artisanale-cnba/) ;
* les [greffiers des tribunaux de commerce](https://www.guichet-entreprises.fr/fr/partenaires/greffiers-des-tribunaux-de-commerce/) ;
* l’[Institut national de la propriété industrielle (INPI)](https://www.guichet-entreprises.fr/fr/partenaires/institut-national-de-la-propriete-industrielle-inpi/) ;
* les [Urssaf](https://www.guichet-entreprises.fr/fr/partenaires/urssaf/) ;
* les collectivités locales et territoriales ;
* les directions compétentes pour la reconnaissance des activités et professions réglementées.

Les bureaux réglementaires
--------------------------

En France, certaines activités et professions sont réglementées. Le service Guichet Entreprises publie des fiches d'information sur chacune d'entre elles : les fiches « [Activités réglementées](https://www.guichet-entreprises.fr/fr/activites-reglementees/) » sont disponibles sur le site [www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/fr/), et les fiches « [Professions réglementées](https://www.guichet-qualifications.fr/fr/professions-reglementees/) » le sont sur [www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/).

Depuis la création du service Guichet Entreprises, les bureaux réglementaires sont des partenaires précieux dans la mise à disposition du public d'une information à jour, fiable et structurée sur ces activités et professions réglementées. Prochainement, le site Guichet Partenaires leur permettra de nous signaler toute évolution réglementaire ou législative les concernant en ligne, de façon simple et rapide.


