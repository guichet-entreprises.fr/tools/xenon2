<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Qui sommes nous ?" -->
<!-- var(keywords)="Qui sommes nous" -->

Qui sommes-nous ?
================

Le site Guichet Partenaires, un site du service à compétence nationale Guichet Entreprises
------------------------------------------------------------------------------------------

Le service Guichet Entreprises a pour mission d’assurer la mise à disposition d’un service électronique accessible par l’Internet, sécurisé et gratuit, permettant :
* d’accomplir, à distance et par voie électronique, les formalités nécessaires à la création, aux modifications de situation et à la cessation d’activité d’une entreprise ainsi qu’à l’accès à une activité réglementée et à son exercice, au sens de la [directive 2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur (directive Services), mentionnées aux articles [R. 123-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000021926763) et [R. 123-30-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000032941383&dateTexte=&categorieLien=cid) du Code de commerce ;
* d’accomplir, à distance et par voie électronique, les formalités, procédures et exigences en matière de reconnaissance, pour l’exercice d’une profession réglementée en France, des qualifications professionnelles acquises dans un autre Etat membre de l’Union européenne ou de l’Espace économique européen, au sens de la [directive 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) du Parlement européen et du Conseil relative à la reconnaissance des qualifications professionnelles (« directive qualifications professionnelles »), mentionnées aux articles [R. 123-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000021926763), [R. 123-30-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000032941383&dateTexte=&categorieLien=cid) et [R. 123-30-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000034568806&dateTexte=&categorieLien=cid) du Code de commerce ; 
* d’accéder, à distance et par voie électronique, à l’information sur ces formalités, procédures et exigences mentionnées aux articles [R. 123-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255836&dateTexte=&categorieLien=cid), [R. 123-21](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256030&dateTexte=&categorieLien=cid), [R. 123-30-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000032941383&dateTexte=&categorieLien=cid) et [R. 123-30-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000034568806&dateTexte=&categorieLien=cid) du Code du commerce.


Le site www.guichet-partenaires.fr est conçu et développé par le service à compétence nationale « Guichet Entreprises », créé par l’arrêté du 22 avril 2015 (JORF du 25 avril 2015).
Il est placé sous l’autorité de la [Direction générale des entreprises](https://www.entreprises.gouv.fr/) au sein du [ministère de l’Économie et des Finances](https://www.economie.gouv.fr/).

Le service à compétence nationale « Guichet Entreprises » gère les sites Internet [www.guichet-entreprises.fr](https://www.guichet-entreprises.fr) et [www.guichet-qualifications.fr](https://www.guichet-qualifications.fr) qui, à eux deux, constituent le guichet unique électronique défini par les directives européennes [2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) et [2005/36/CE](https://eur-lex.europa.eu/legal-content/fr/TXT/?uri=CELEX:32005L0036).
Des guichets de ce type existent dans toute l’Europe et sont fédérés au sein du projet « [Eugo](http://ec.europa.eu/internal_market/eu-go/index_fr.htm)» de la Commission européenne.

Au service Guichet Entreprises, nous avons pour ambition :
* de donner la possibilité aux futurs créateurs et aux dirigeants d’entreprise de réaliser, en ligne, les formalités administratives liées à la vie d’une entreprise ;
* d'encourager la mobilité professionnelle des résidents de l’Union européenne (UE) et de l’Espace économique européen (EEE) en offrant une information complète sur l’accès et l’exercice des professions réglementées en France, en vue d’une reconnaissance de qualification professionnelle.

Ainsi, sur www.guichet-partenaires.fr tout agent d'un organisme partenaire du service Guichet Entreprises peut consulter, télécharger et archiver les dossiers dématérialisés de création, de modifications de la situation et de cessation d’activité d’une entreprise et de reconnaissance de qualification professionnelle dont il est destinataire.

Le site www.guichet-partenaires.fr s’adresse à toutes les organismes partenaires du service Guichet Entreprises intervenant dans le parcours de création d'entreprise des citoyen français, des ressortissants de l'Union européenne (UE) ou de l'Espace économique européen (EEE) et dans la reconnaissance des qualifications professionnelles des ressortissants de l'UE et de l'EEE.

TODO:LIEN VIDÉO PRÉSENTATION DU SCN

La Direction générale des entreprises (DGE)
-------------------------------------------

Le service à compétence nationale Guichet Entreprises est placé sous l'autorité de la Direction générale des entreprises (DGE) au sein du ministère de l'Économie et des Finances.
La DGE a pour mission de développer la compétitivité et la croissance des entreprises de l'industrie et des services.
Force de propositions des ministres dans tous les domaines de la compétitivité des entreprises, elle comprend :
* un secrétariat général chargé d'assurer son fonctionnement ;
* le service de l'industrie ;
* le service de l'économie numérique ;
* le service du tourisme, du commerce, de l'artisanat et des services ;
* le service de la compétitivité, de l'innovation et du développement des entreprises ;
* le service de l'action territoriale, européenne et internationale.









