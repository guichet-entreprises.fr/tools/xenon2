<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Partenaire" -->
<!-- var(keywords)="Partenaire" -->

Je suis partenaire
=================

Vous êtes teneur de registre, agent d'un centre de formalités des entreprises ou agent d'un organisme chargé de la reconnaissance des qualifications professionnelles ?
Le site Guichet Partenaires est fait pour vous !
Sur Guichet Partenaires, vous aurez accès à plusieurs fonctionnalités qui faciliteront votre travail.

## Quel est mon profil ?

Selon vos fonctions au sein de votre organisme, nous avons créé trois profils auxquels différents [services](Services.md) sont associés.

### Utilisateur

Sont utilisateurs du site Guichet Partenaires les agents en charge du traitement des dossiers constitués par les déclarants sur les sites du service Guichet Entreprises possédant [un compte créé par le référent de leur organisme](Créer_un_compte.md).

### Référent « système de paiement »

Le référent « système de paiement » est responsable du système et des informations de paiement d'un organisme destinataire des dossiers déposés par les déclarants sur les sites du service Guichet Entreprises.

### Référent

Le référent d'un organisme a la possibilité, en plus des fonctionnalités liées à l'instruction des dossiers déposés par les déclarants sur les sites du service Guichet Entreprises, de créer des comptes pour les agents qui y travaillent et de choisir les canaux de transmission par lesquels son organisme recevra les dossiers.

> Pour plus d'informations sur les fonctionnalités que vous confère votre rôle sur guichet-partenaires.fr, nous vous invitons à consulter la page [Services](Services.md).

## Quel délai ai-je pour traiter un dossier transmis par le service Guichet Entreprises ?

Conformément aux articles [R. 123-25](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256049&dateTexte=&categorieLien=cid) et [R. 123-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255899&dateTexte=&categorieLien=cid) du Code de commerce, les organismes destinataires sont tenus de traiter les dossiers transmis par le service Guichet Entreprises.
Conformément à l'[article 123-9 du Code de commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255899&dateTexte=&categorieLien=cid), « le centre de formalités des entreprises compétent, saisi du dossier complet conformément aux dispositions de l'article R. 123-7, transmet le jour même aux organismes destinataires, et le cas échéant aux autorités habilitées à délivrer les autorisations, les informations et pièces les concernant. »
