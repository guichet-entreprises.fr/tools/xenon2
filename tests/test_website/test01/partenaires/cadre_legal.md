<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Cadre légal" -->
<!-- var(keywords)="Cadre légal" -->

Cadre légal
==================

Les missions du service Guichet Entreprises s'inscrivent dans un cadre fixé par les directives européennes [2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur (« directive services ») et [2005/36/CE ](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32005L0036)du Parlement européen et du Conseil du 20 novembre 2013 modifiant la directive 2005/36/CE relative à la reconnaissance des qualifications professionnelles (« directive qualifications professionnelles »).

Le site www.guichet-partenaires.fr est conçu et développé par le service à compétence nationale « Guichet Entreprises », créé par l’arrêté du 22 avril 2015 (JORF du 25 avril 2015).
Il est placé sous l’autorité de la [Direction générale des entreprises](https://www.entreprises.gouv.fr/) au sein du [ministère de l’Économie et des Finances](https://www.economie.gouv.fr/).


Les organismes destinataires des dossiers conformes aux articles [R. 123-7](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255891&dateTexte=&categorieLien=cid), [R. 123-23](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000032944474&dateTexte=&categorieLien=id) et [R. 123-24](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=291827F72B9E2E1DCEA84F4A9A285CDD.tplgfr42s_2?idArticle=LEGIARTI000035680237&cidTexte=LEGITEXT000005634379&dateTexte=20180927&categorieLien=id&oldAction=&nbResultRech=) du Code de commerce déposés par les déclarants sur les sites [www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/) et [www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) sont responsables de la transmission aux organismes et administrations destinataires des éléments du dossier de déclaration d'entreprise qu'ils ont reçus par voie électronique conformément à l'article [R. 123-25](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256049&dateTexte=&categorieLien=cid) du Code de commerce.

