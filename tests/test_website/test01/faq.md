<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="FAQ" -->
<!-- var(keywords)="FAQ" -->


Foire Aux Questions
===================

* [J'ai oublié mon mot de passe](##J-ai-oublié-mon-mot-de-passe)
* [Puis-je modifier mes informations de contact ?](##Puis-je-modifier-mes-informations-de-contact)
* [Mon référent m'a créé un compte mais je ne peux pas me connecter](##Mon-référent-m-a-créé-un-compte-mais-je-ne-peux-pas-me-connecter)
* [Je dispose d'un compte sur guichet-partenaires.fr mais je ne vois pas mes dossiers](##Je-dispose-d-un-compte-sur-guichet-partenaires-fr-mais-je-ne-vois-pas-mes-dossiers)
* [Je suis référent mais je n'ai pas reçu le courriel d'activation](##Je-suis-référent-mais-je-n-ai-pas-reçu-le-courriel-d-activation)
* [Je suis tête de réseau mais je ne peux pas visualiser la liste des agents de mon réseau](##Je-suis-tête-de-réseau-mais-je-ne-peux-pas-visualiser-la-liste-des-agents-de-mon-réseau)
* [Je ne parviens pas à visualiser les fichiers PDF de mes dossiers](##Je-ne-parviens-pas-à-visualiser-les-fichiers-PDF-de-mes-dossiers)

## J'ai oublié mon mot de passe<a id="J-ai-oublié-mon-mot-de-passe"></a>

Si vous avez oublié votre mot de passe, cliquez sur « [Mot de passe oublié](LIEN) ».
Saisissez le courriel que vous utilisez sur www.guichet-partenaires.fr et vous recevrez par courriel un lien à usage unique qui vous permettra de réinitialiser votre mot de passe.
Le lien restera valable pendant 24 heures.

## Puis-je modifier mes informations de contact ?<a id="Puis-je-modifier-mes-informations-de-contact"></a>

Depuis votre tableau de bord, vous accéderez à votre compte (lien « Mon compte »), vous aurez alors la possibilité de changer vos informations personnelles ou votre mot de passe.

Pour modifier votre mot de passe, veuillez suivre la procédure d’oubli de mot de passe.

## Mon référent m'a créé un compte mais je ne peux pas me connecter<a id="Mon-référent-m-a-créé-un-compte-mais-je-ne-peux-pas-me-connecter"></a>

Si vous ne pouvez pas accéder au compte créé par votre référent, veuillez [réinitialiser votre mot de passe](##J-ai-oublié-mon-mot-de-passe).
Si malgré cela vous ne parvenez toujours pas à vous connecter, [veuillez consulter l'assistance utilisateur](support.md).

## Je dispose d'un compte sur guichet-partenaires.fr mais je ne vois pas mes dossiers<a id="Je-dispose-d-un-compte-sur-guichet-partenaires-fr-mais-je-ne-vois-pas-mes-dossiers"></a>

Si vous ne pouvez pas accéder aux dossiers de votre compte, [veuillez consulter l'assistance utilisateur](support.md).

## Je suis référent mais je n'ai pas reçu le courriel d'activation<a id="Je-suis-référent-mais-je-n-ai-pas-reçu-le-courriel-d-activation"></a>

Si vous n'avez pas reçu la courriel d'activation, [veuillez consulter l'assistance utilisateur](support.md).

## Je suis tête de réseau mais je ne peux pas visualiser la liste des agents de mon réseau<a id="Je-suis-tête-de-réseau-mais-je-ne-peux-pas-visualiser-la-liste-des-agents-de-mon-réseau"></a>

Si vous ne pouvez pas visualiser la liste des agents de votre réseau, [veuillez consulter l'assistance utilisateur](support.md).

## Je ne parviens pas à visualiser les fichiers PDF de mes dossiers<a id="Je-ne-parviens-pas-à-visualiser-les-fichiers-PDF-de-mes-dossiers"></a>

Si vous ne parvenez pas à visualiser les fichiers PDF de vos dossiers depuis un navigateur autre que Mozilla Firefox ou Google Chrome, nous vous recommandons d'utiliser une version récente de l'un de ces deux navigateurs.
Si malgré cela vous ne parvenez toujours pas à les visualiser, [veuillez consulter l'assistance utilisateur](support.md).