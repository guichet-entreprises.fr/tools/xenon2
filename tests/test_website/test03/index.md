﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(footer_off)="Off" -->
<!-- var(nav_default_off)="Off" -->
<!-- var(nav_menu_off)="Off" -->
<!-- var(breadcrumb_off)="Off" -->
<!-- var(collapsable)="close" -->
<!-- var(site:home)="Accueil" -->


DQP005 - AGENT DE PROTECTION PHYSIQUE DES PERSONNES <!-- collapsable: -->
===================================================

1°. Définition de l'activité <!-- collapsable:open -->
----------------------------

L'agent de protection physique des personnes (« agent 3P »), ou plus couramment appelé « garde du corps », est un professionnel dont la mission principale est d'assurer la sécurité et l'intégrité physique de ses clients dans leurs déplacements professionnels ou privés.

À ce titre, il sera capable de suivre les procédures d'urgence et les techniques d'intervention physique pour sortir ces personnes d'une zone de danger. Il doit ainsi faire preuve d'une bonne condition physique pour assurer l'exercice de la profession et, le cas échéant, être capable de porter les premiers secours.

*Pour aller plus loin* : article L. 611-1 paragraphe 3° du code de la sécurité intérieure (CSI).

2°. Qualifications professionnelles <!-- collapsable:open -->
-----------------------------------

### a. Exigences nationales <!-- collapsable:open -->

#### Législation nationale

Toute personne souhaitant exercer l'activité d'agent 3P doit justifier d'une aptitude professionnelle et être titulaire d'une carte professionnelle délivrée par la commission régionale d'agrément et de contrôle (CNAC) établie au sein du conseil national des activités privées de sécurité (CNAPS).

Cette carte professionnelle est délivrée à condition que l'intéressé satisfasse aux conditions suivantes :

 - être de nationalité française ou ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) ;
 - exercer cette activité dans un État de l'UE ou de l'EEE lorsqu'il n'est pas inscrit au registre du commerce et des sociétés ;
 - ne pas avoir été condamné à une peine correctionnelle ou criminelle inscrite au bulletin n°2 du casier judiciaire ou d'un document équivalent pour le ressortissant ;
 - ne pas être sous le coup d'un arrêté d'expulsion ou d'interdiction de territoire français en cours ;
 - ne pas être en faillite personnelle ou en redressement judiciaire ;
 - ne pas exercer l'une des activités visées au paragraphe « 3°. Incompatibilité d'exercice » ;
 - avoir les qualifications requises pour exercer la profession d'agent 3P.

*Pour aller plus loin* : article L. 612-20 du code de la sécurité intérieure.

#### Formation

La formation professionnelle permettant de justifier l'aptitude professionnelle de l'agent 3P est soumise à une autorisation délivrée par le CNAPS. Cette décision peut intervenir :

 - avant l'embauche dans une entreprise assurant la protection des personne : dans ce cas, l'intéressé recevra une **autorisation préalable** pour intégrer un centre de formation, valable pendant six mois, qu'il remettra au centre de formation. Sera notamment reconnu comme justifiant d'une aptitude professionnelle, le titulaire du [certificat de qualification professionnelle](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=3B1663A73B7FCAC7D55A33FCF8F85ACB.tpdila22v_2?cidTexte=JORFTEXT000035210951&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000035210946) « agent de protection physique des personnes ».
 - pendant l'embauche : l'intéressé qui a conclu un contrat de travail avec une entreprise de protection privée recevra une **autorisation provisoire** d'exercer valable pendant six mois, qui sera remise à son employeur.

###### À noter

La demande d'autorisation se fait directement [en ligne](https://depot-teleservices-cnaps.interieur.gouv.fr/autorisation-prealable/identification) sur le site du CNAPS.

*Pour aller plus loin* : articles L. 612-22 et L. 612-23 du CSI.

#### Coûts associés à la qualification

La formation menant à la profession d'agent 3P peut être payante. Pour plus d'information, il est conseillé de se rapprocher des organismes de formation la dispensant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Service)

Le ressortissant d’un État de l'UE ou de l’EEE, exerçant l’activité d’agent 3P dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration adressée à la commission locale d'agrément et de contrôle (CLAC) territorialement compétente pour Paris (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

Si l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude dans un délai d'un mois à compter de la réception de la demande de déclaration par la CLAC.

*Pour aller plus loin* : article R. 612-25 du CSI.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité d'agent de protection physique des personnes dans cet État, peut exercer la même activité en France de manière permanente.

Il devra demander la carte professionnelle auprès de la CLAC territorialement compétente (cf. infra « 5°. a. Obtenir une carte professionnelle pour le ressortissant de l'UE ou de l'EEE »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

Si l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard de celles requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une mesure de compensation (cf. infra « 5°. b. Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : article L. 612-20 et R. 612-24-1 du CSI.

3°. Incompatibilité d'exercice
------------------------------

Dès lors que l'intéressé exerce la fonction d'agent 3P, il ne peut pas :

 - surveiller ou garder des personnes ou des biens meubles ou immeubles ;
 - surveiller et transporter des bijoux, des fonds ou des métaux précieux ;
 - protéger des navires français contre des menaces d'actes terroristes ou de prise de contrôle ;
 - de rechercher des informations ou des renseignements destinés à des tiers dans le cadre de l'activité d'agent de recherches privées.

*Pour aller plus loin* : article L. 611-1 et L. 612-2 du CSI.

4°. Formation continue et assurance <!-- collapsable: -->
-----------------------------------

### a. Obligation de suivre une formation professionnelle continue

Le renouvellement de la carte professionnelle est soumis au suivi d'une formation continue visant au maintien et à l'actualisation des compétences de l'intéressé. Cette formation dispensée sous la forme d'un stage d'une durée de [38 heures](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=FFBC66EEE39A5DDB89FD23A75901C22A.tplgfr22s_2?idArticle=JORFARTI000034104603&cidTexte=JORFTEXT000034104578&dateTexte=29990101&categorieLien=id) doit intervenir dans un délai de vingt-quatre mois avant la date d'expiration de la carte.

*Pour aller plus loin* : article L. 622-19-1 du CSI et articles 1 et 8 de l'arrêté du 27 février 2017 relatif à la formation continue des agents privés de sécurité.

### b. Obligation de souscrire une assurance de responsabilité civile professionnelle

En sa qualité de professionnel indépendant, l'agent 3P doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié dans une agence, cette assurance n'est que facultative. Dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

*Pour aller plus loin* : article L. 612-5 du CSI.

5°. Démarche et formalités de reconnaissance de qualification
-------------------------------------------------------------

### a. Effectuer une déclaration pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

###### Autorité compétente

La délégation territoriale de la CLAC dans laquelle se trouve Paris est compétente pour se prononcer sur la demande de déclaration.

###### Pièces justificatives

La demande de déclaration est un dossier transmis par tout moyen comprenant l'ensemble des documents suivants :

 - une déclaration comportant les informations relatives à l'état civil du ressortissant ;
 - une photocopie de sa pièce d'identité ;
 - une attestation certifiant que l'intéressé exerce et est établi légalement dans un État de l'UE ou de l'EEE ;
 - l'absence de condamnation pénale inscrite sur son casier judiciaire ;
 - une preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années, lorsque ni l'activité professionnelle, ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE.

###### Délai

La CLAC dispose d'un délai d'un mois pour rendre sa décision :

 - d’autoriser le ressortissant à faire sa première prestation de service ;
 - de soumettre l’intéressé à une mesure de compensation sous la forme d’une épreuve d’aptitude, s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France ;
 - de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, il aura deux mois pour se décider, à compter de la résolution de la ou des difficultés.

En l'absence de réponse de l'autorité compétente dans ces délais, la prestation de service peut débuter.

*Pour aller plus loin* : article R. 612-25 du CSI.

### b. Obtenir une carte professionnelle pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent

###### Autorité compétente

La CLAC territorialement compétente se prononce sur la délivrance de la carte professionnelle dès lors que le ressortissant remplit les conditions d'attribution.

###### Pièces justificatives

Pour obtenir la carte professionnelle, le ressortissant transmet par voie postale, un dossier complet à la CLAC territorialement compétente. Ce dossier doit comporter les documents justificatifs suivants :

 - un [formulaire](https://www.cnaps-securite.fr/sites/default/files/inline-files/FormCP-MG2_0.pdf) dûment complété et signé ;
 - une photocopie d'une pièce d'identité du ressortissant ;
 - une attestation d'emploi délivrée par l'employeur ou le futur employeur du ressortissant ;
 - un justificatif attestant de son aptitude professionnelle qui peut être :

  + un certificat professionnel enregistré au répertoire national des certifications professionnelles,
  + un certificat de qualification professionnelle élaboré par la branche professionnelle relative à la protection physique des personnes,
  + un titre de formation ou une attestation de compétences, délivrés par un État de l'UE ou de l'EEE qui réglemente l'activité d'agent 3P sur son territoire et comprenant le détail et la durée des modules de la formation suivie,
  + une preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années, lorsque ni l'activité professionnelle, ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE.

###### À savoir

Les pièces justificatives doivent être rédigées en langue française ou traduites par un traducteur agréé, le cas échéant.

###### Durée et renouvellement

La carte professionnelle est délivrée sous la forme dématérialisée d'un numéro d'enregistrement, et valable pendant cinq ans. Tout changement de situation professionnelle devra être notifié à la CLAC mais n'entraîne pas le renouvellement obligatoire de la carte. À l'issue de ces cinq ans, le professionnel pourra demander le renouvellement trois mois avant la date d'expiration à condition de présenter une attestation de formation continue (cf. supra « 4°. a. Obligation de suivre une formation professionnelle continue »).

###### Issue de la procédure

Une fois que le ressortissant a obtenu le numéro d'enregistrement de la CLAC territorialement compétente, il devra le transmettre à son employeur qui lui délivrera la carte professionnelle définitive.

###### Bon à savoir

Mesures de compensation

Pour exercer son activité en France ou accéder à la profession, le ressortissant peut être amené à se soumettre à une mesure de compensation, qui peut être :

 - un stage d'adaptation d'une durée maximale de trois ans ;
 - une épreuve d'aptitude réalisée dans les six mois suivant sa notification à l'intéressé.

*Pour aller plus loin* : articles L. 612-20, L. 612-24, R. 612-12 et suivants du CSI.

### b. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’informations sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

###### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

 - que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
 - et qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

###### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

###### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

 - ses coordonnées complètes ;
 - la description détaillée de son problème ;
 - l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

###### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

###### Coût

Gratuit.

###### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

 - si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
 - s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

Informations supplémentaires : SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris, [site officiel](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).
