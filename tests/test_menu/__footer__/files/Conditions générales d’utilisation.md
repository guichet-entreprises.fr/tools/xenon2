﻿<!-- var(author)        ="SCN Guichet Entreprises"  -->
<!-- var(page:title)        ="Page title:Qui"               -->
<!-- var(page:breadcrumb)   ="Breadcrumb:Qui"                 -->
<!-- var(title)             ="Title:Qui"                 -->


Conditions générales d’utilisation du téléservice guichet-entreprises.fr
========================================================================

Préambule
---------

Ce document présente les modalités d’engagement à l’utilisation du téléservice
guichet-entreprises.fr (ci-après le « Service ») pour les usagers. Il s’inscrit
dans le cadre juridique :

-   De la directive 2006/123/CE du Parlement européen et du Conseil du 12
    décembre 2006 relative aux services dans le marché intérieur.

-   Du règlement n° 910/2014 du Parlement européen et du Conseil du 23 juillet
    2014 (e-IDAS) sur l'identification électronique et les services de confiance
    pour les transactions électroniques au sein du marché intérieur.

-   L’ordonnance du 8 décembre 2005 relative aux échanges électroniques entre
    les usagers et les autorités administratives et entre les autorités
    administratives et le décret n° 2010-112 du 2 février 2010 pris pour
    l'application des articles 9, 10 et 12 de cette ordonnance.

-   Du dispositif de la loi n° 78-17 du 6 janvier 1978 modifiée relative à
    l'informatique, aux fichiers et aux libertés.

-   L’article 441-1 du Code Pénal.

-   De l’article R. 123-2 du code de commerce.

-   De l’article R. 123-21 du code de commerce.

-   De l’article R. 123-24 du code de commerce.

-   De l’arrêté du 22 avril 2015 portant création d'un service à compétence
    nationale dénommé « guichet entreprises ».

Objet du document
-----------------

Le présent document a pour objet de définir les conditions générales
d’utilisation du téléservice guichet-entreprises.fr, appelé ci-après le «
Service » entre le service à compétence nationale Guichet Entreprises et les
usagers.

Le service à compétence nationale Guichet Entreprises est placé sous l’autorité
de la direction générale des entreprises du ministère de l’Économie et des
Finances.

Définition et objet de guichet-entreprises.fr
---------------------------------------------

Guichet-entreprises.fr (ci-après dénommé le Service) est un téléservice mis en
œuvre par le service à compétence nationale Guichet Entreprises (ci-après
dénommé « le service Guichet Entreprises ») contribuant à simplifier les
démarches liées à la création, aux modifications de la situation et à la
cessation d'activité d'une entreprise des usagers français et européens.

L’utilisation du Service est facultative et gratuite. Toutefois, dans le cadre
des formalités effectuées sur ce portail, comme celles de la création
d’entreprise création d’entreprise, des paiements en ligne peuvent être
demandés. Ceux-ci sont sécurisés.

L’ensemble des destinataires au téléservice sont dénommées ci-après les
organismes partenaires.

En vertu de l’article R123-3 du code de commerce, les données recueillies sont
transmises aux fins de traitement aux organismes partenaires suivants :

« *1° Sous réserve des dispositions des 2° et 3°, les chambres de commerce et
d’industrie territoriales créent et gèrent les centres de formalités des
entreprises compétents pour :*

*a) Les commerçants ;*

*b) Les sociétés commerciales.*

*2° Les chambres de métiers et de l’artisanat de région créent et gèrent les
centres compétents pour les personnes physiques et les sociétés assujetties à
l’immatriculation au répertoire des métiers et pour les personnes physiques
bénéficiant de la dispense d’immatriculation prévue au V de l’article 19 de la
loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du
commerce et de l’artisanat, à l’exclusion des personnes mentionnées au 3° du
présent article.*

*3° La chambre nationale de la batellerie artisanale crée et gère le centre
compétent pour les personnes physiques et les sociétés assujetties à
l’immatriculation au registre des entreprises de la batellerie artisanale.*

*4° Les greffes des tribunaux de commerce ou des tribunaux de grande instance
statuant commercialement créent et gèrent les centres compétents pour :*

*a) Les sociétés civiles et autres que commerciales ;*

*b) Les sociétés d’exercice libéral ;*

*c) Les personnes morales assujetties à l’immatriculation au registre du
commerce et des sociétés autres que celles mentionnées aux 1°,2° et 3° ;*

*d) Les établissements publics industriels et commerciaux ;*

*e) Les agents commerciaux ;*

*f) Les groupements d’intérêt économique et les groupements européens d’intérêt
économique.*

*5° Les unions de recouvrement des cotisations de sécurité sociale et
d’allocations familiales (URSSAF) ou les caisses générales de sécurité sociale
créent et gèrent les centres compétents pour :*

*a) Les personnes exerçant, à titre de profession habituelle, une activité
indépendante réglementée ou non autre que commerciale, artisanale ou agricole ;*

*b) Les employeurs dont les entreprises ne sont pas immatriculées au registre du
commerce et des sociétés, au répertoire des métiers ou au registre des
entreprises de la batellerie artisanale, et qui ne relèvent pas des centres
mentionnés au 6°.*

*6° Les chambres d’agriculture créent et gèrent les centres compétents pour les
personnes physiques et morales exerçant à titre principal des activités
agricoles […].* »

Fonctionnalités
---------------

Le Service permet à l’usager :

-   d’accéder à la documentation précisant les obligations des centres de
    formalités des entreprises (CFE) ainsi que les éléments constitutifs du
    dossier de déclaration et du dossier de demandes d’autorisation ;

-   créer un compte utilisateur donnant accès à un espace de stockage personnel.
    Cet espace permet à l’usager :

-   de gérer et utiliser ses données à caractère personnel, de conserver les
    informations le concernant et les documents et pièces justificatives qui lui
    sont nécessaires pour l’accomplissement des démarches administratives.

Depuis son espace, l’usager peut :

-   constituer son dossier unique visé à l’article R 123-1 du code de commerce
    comprenant le dossier de déclaration et, le cas échéant, de demandes
    d’autorisation,

-   transmettre son dossier de déclaration et, le cas échéant, de demandes
    d’autorisation au centre de formalités des entreprises compétent ;

-   accéder aux informations de suivi du traitement du dossier de déclaration
    et, le cas échéant, du dossier de demandes d’autorisation.

-   permettre aux CFE de mettre en œuvre les traitements nécessaires à
    l’exploitation des informations reçues de la personne morale visées au
    dernier alinéa de l’ article R.123-21 du code de commerce : réception du
    dossier unique prévu à l’article 2 de la loi n°94-126 du 11 février 1994
    relative à l’initiative et à l’entreprises individuelle et transmis par la
    personne morale visées à l’article R 123-21 du code de commerce ; réception
    des informations de suivi du traitement de ces dossiers telles que
    transmises par les organismes et autorités partenaires ; transmission des
    informations de suivi du traitement des dossiers uniques à la personne
    morale visée à l’article R121-21 du code de commerce.

Modalités d’inscription et d’utilisation du Service 
----------------------------------------------------

L’accès au Service est ouvert à toute personne et gratuit. Il est facultatif et
n’est pas exclusif d’autres canaux d’accès pour permettre à l’usager d’accomplir
ses formalités.

Pour la gestion de l’accès à l’espace personnel du Service, l’usager partage les
informations suivantes :

-   l’identifiant de connexion choisi par l’usager ;

-   le mot de passe choisi par l’usager ;

-   l’adresse électronique de l’usager.

Pour l’utilisation de l’espace de stockage personnel, l’usager partage les
informations suivantes :

-   Pour une personne physique :

-   nom, nom de naissance,

-   prénom, prénom usuel,

-   date et lieu de naissance,

-   nationalité,

-   adresse, adresse de destination des courriers,

-   situation maritale, régime matrimonial,

-   nom du conjoint, nom de naissance du conjoint,

-   prénom du conjoint, prénom usuel du conjoint,

-   adresse du conjoint,

-   date et lieu de naissance du conjoint,

-   nationalité du conjoint,

-   situation de forain, situation d’exercice ambulant,

-   situation de commerçant au sein d'un état membre de l'Union européenne autre
    que la France,

-   présence d’une déclaration d'insaisissabilité sur la résidence principale,
    lieu de la publication au bureau de la conservation des hypothèques,

-   noms des précédents exploitants, noms de naissance des précédents
    exploitants,

-   prénoms des précédents exploitants, prénoms usuels des précédents
    exploitants,

-   nom du loueur de fonds, nom de naissance du loueur de fonds,

-   prénom du loueur de fonds, prénom usuel du loueur de fonds,

-   adresse du loueur de fonds,

-   numéro de sécurité sociale,

-   régime d'assurance maladie,

-   exercice d’une activité non salariée antérieure,

-   numéro de sécurité sociale du conjoint ou pacsé collaborateur,

-   nom du fondé de pouvoir,

-   nom de naissance du fondé de pouvoir,

-   prénom du fondé de pouvoir,

-   prénom usuel du fondé de pouvoir,

-   adresse du fondé de pouvoir,

-   lieu de naissance du fondé de pouvoir,

-   date de naissance du fondé de pouvoir,

-   nationalité du fondé de pouvoir,

-   nom de l’ayant droit,

-   nom de naissance de l’ayant droit,

-   prénom de l’ayant droit,

-   prénom usuel de l’ayant droit,

-   numéro de sécurité sociale de l’ayant droit,

-   lieu de naissance de l’ayant droit,

-   date de naissance de l’ayant droit,

-   nationalité de l’ayant droit,

-   lien de parenté,

-   nom du signataire,

-   nom de naissance du signataire,

-   prénom du signataire,

-   prénom usuel du signataire,

-   adresse du signataire ;

-   Pour une personne morale :

-   nom du loueur de fonds,

-   nom de naissance du loueur de fonds,

-   prénom du loueur de fonds,

-   prénom usuel du loueur de fonds,

-   adresse du loueur de fonds,

-   nationalité du dirigeant d’une société associée,

-   nom du dirigeant d’une société associée,

-   nom de naissance du dirigeant d’une société associée,

-   prénom du dirigeant d’une société associée,

-   prénom usuel du dirigeant d’une société associée,

-   adresse du dirigeant d’une société associée,

-   lieu de naissance du dirigeant d’une société associée,

-   date de naissance du dirigeant d’une société associée,

-   nom du représentant de la personne morale,

-   nom de naissance du représentant de la personne morale,

-   prénom et prénom usuel du représentant de la personne morale,

-   adresse du représentant de la personne morale,

-   date et lieu de naissance du représentant de la personne morale,

-   nationalité du représentant de la personne morale,

-   nom du conjoint,

-   nom de naissance du conjoint,

-   prénom du conjoint,

-   prénom usuel du conjoint,

-   adresse du conjoint,

-   lieu de naissance du conjoint,

-   date de naissance du conjoint,

-   nationalité du conjoint,

-   nom d’une autre personne pouvant engager l’établissement,

-   nom de naissance d’une autre personne pouvant engager l’établissement,

-   prénom d’une autre personne pouvant engager l’établissement,

-   prénom usuel d’une autre personne pouvant engager l’établissement,

-   adresse d’une autre personne pouvant engager l’établissement,

-   lieu de naissance d’une autre personne pouvant engager l’établissement,

-   date de naissance d’une autre personne pouvant engager l’établissement,

-   nationalité d’une autre personne pouvant engager l’établissement,

-   nom du signataire,

-   nom de naissance du signataire,

-   prénom du signataire,

-   prénom usuel du signataire,

-   adresse du signataire,

-   numéro de sécurité sociale du dirigeant,

-   régime d'assurance maladie du dirigeant,

-   numéro de sécurité sociale du conjoint ou pacsé collaborateur,

-   nom de l’ayant droit,

-   nom de naissance de l’ayant droit,

-   prénom de l’ayant droit,

-   prénom usuel de l’ayant droit,

-   numéro de sécurité sociale de l’ayant droit,

-   lieu de naissance de l’ayant droit,

-   date de naissance de l’ayant droit,

-   nationalité de l’ayant droit,

-   lien de parenté.

L’utilisation du Service requiert une connexion et un navigateur internet. Le
navigateur doit être configuré pour autoriser les *cookies* de session.

Afin de garantir un bon fonctionnement du Service, il est conseillé d’utiliser
les versions de navigateurs suivantes :

-   Firefox version 45 et plus ;

-   Opera version 11 et plus ;

-   Safari version 5.1 pour Windows et version 6.0 et plus pour MacOs ;

-   Internet Explorer version 9 et plus ;

-   Microsoft Edge version 25 et plus ;

-   Google Chrome version 48 et plus.

Le Service est optimisé pour un affichage en 1024x768 pixels. Il est recommandé
d’utiliser la dernière version du navigateur et de le mettre à jour
régulièrement pour bénéficier des correctifs de sécurité et des meilleures
performances.

Conditions spécifiques d’utilisation du service de signature
------------------------------------------------------------

Le service de signature électronique est accessible directement via le Service.

L’article R123-24 du code de commerce en application du règlement n° 910/2014 du
Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sont les références
applicables au service de signature électronique du Service.

Rôles et engagement
-------------------

### Engagement du service Guichet Entreprises

1.  Le service Guichet Entreprises met en œuvre et opère le Service conformément
    au cadre juridique en vigueur défini en préambule.

2.  Le service Guichet Entreprises s’engage à prendre toutes les mesures
    nécessaires permettant de garantir la sécurité et la confidentialité des
    informations fournies par l’usager.

3.  Le service Guichet Entreprises s’engage à assurer la protection des données
    collectées dans le cadre du Service, et notamment empêcher qu’elles soient
    déformées, endommagées ou que des tiers non autorisés y aient accès,
    conformément aux mesures prévues par l’ordonnance du 8 décembre 2005
    relative aux échanges électroniques entre les usagers et les autorités
    administratives et entre les autorités administratives et le décret n°
    2010-112 du 2 février 2010 pris pour l'application des articles 9, 10 et 12
    de cette ordonnance.

4.  Le service Guichet Entreprises et les organismes partenaires garantissent
    aux usagers du Service les droits d’accès, de rectification et d’opposition
    prévus par la loi n° 78-17 du 6 janvier 1978 relative à l’informatique aux
    fichiers et aux libertés. Ce droit peut s’exercer de plusieurs façons :

    1.  en s’adressant au Centre de Formalités des Entreprises (CFE)
        destinataire du dossier de déclaration ;

    2.  en envoyant un courriel au support :
        support.guichet-entreprises\@helpline.fr

    3.  en envoyant un courrier à :

>   Service à compétence nationale Guichet Entreprises

>   120 rue de Bercy – Télédoc 766

>   75572 Paris cedex 12

1.  Le service Guichet Entreprises et les organismes partenaires s’engagent à
    n’opérer aucune commercialisation des informations et documents transmis par
    l’usager au moyen du Service, et à ne pas les communiquer à des tiers, en
    dehors des cas prévus par la loi.

2.  Le service Guichet Entreprises s’engage à assurer la traçabilité de toutes
    les actions réalisées par l’ensemble des utilisateurs du Service, y compris
    celles des organismes partenaires et de l’usager.

3.  Le service Guichet Entreprises offre aux usagers un support en cas
    d’incident ou d’alerte sécurité défini.

Engagement de l’usager
----------------------

1.  L’usager remplit en ligne son dossier et valide celui-ci en y joignant
    éventuellement les pièces nécessaires au traitement de ce dernier.

2.  A l’issue de la constitution de son dossier, s’affiche à l’écran un
    récapitulatif des éléments renseignés par l’usager afin que celui-ci puisse
    les vérifier et les confirmer. Après confirmation, le dossier est transmis
    au CFE compétent. La confirmation et la transmission du formulaire par
    l’usager vaut signature de celui-ci.

3.  Les présentes conditions générales s’imposent à tout utilisateur usager du
    Service.

Disponibilité et évolution du Service
-------------------------------------

Le Service est disponible 7 jours sur 7, 24 heures sur 24. Le service Guichet
Entreprises se réserve toutefois la faculté de faire évoluer, de modifier ou de
suspendre, sans préavis, le Service pour des raisons de maintenance ou pour tout
autre motif jugé nécessaire. L'indisponibilité du service ne donne droit à
aucune indemnité. En cas d'indisponibilité du Service, une page d’information
est alors affichée à l’usager lui mentionnant cette indisponibilité ; il est
alors invité à effectuer sa démarche ultérieurement.

Les termes des présentes conditions d'utilisation peuvent être amendés à tout
moment, sans préavis, en fonction des modifications apportées au service, de
l'évolution de la législation ou de la réglementation, ou pour tout autre motif
jugé nécessaire.

Responsabilités
---------------

1.  La responsabilité du service Guichet Entreprises ne peut être engagée en cas
    d’usurpation d’identité ou de toute utilisation frauduleuse du Service.

2.  Les données transmises aux services en ligne des organismes partenaires
    restent de la responsabilité de l’usager, même si celles-ci sont transmises
    par les moyens techniques mis à disposition dans le Service. L’usager peut à
    tout moment les modifier ou les supprimer auprès des organismes partenaires.
    Il peut choisir de supprimer toutes les informations de son compte en
    supprimant ses données auprès du Service.

3.  Il est rappelé à l’usager que toute personne procédant à une fausse
    déclaration pour elle-même ou pour autrui s’expose, notamment, aux sanctions
    prévues à l’article 441-1 du Code Pénal, prévoyant des peines pouvant aller
    jusqu’à trois ans d’emprisonnement et 45 000 euros d’amende.
