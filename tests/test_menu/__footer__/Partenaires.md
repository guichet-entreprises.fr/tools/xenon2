﻿Partenaires
===========

Associer les meilleures expertises de la création !

Le site guichet-entreprises.fr est le fruit d’une démarche mise en œuvre à
l’initiative de l’État français dans le cadre de l’application de la directive
européenne sur les services.

Naturellement proches des enjeux des entrepreneurs dont ils sont les premiers
interlocuteurs, les Centres de formalités des entreprises (CFE) sont les
partenaires privilégiés de guichet-entreprises.fr. Leur travail commun permet
aujourd’hui aux entrepreneurs français et européens de créer leur entreprise sur
un site simple et complet.

**Nos partenaires :**

-   [Les Chambres de Commerce et d’Industrie (CCI)](https://www.qual.guichet-entreprises.fr/fr/partenaires/chambres-de-commerce-et-d-industrie-cci/)

-   [Les Greffiers des Tribunaux de
    Commerce](https://www.qual.guichet-entreprises.fr/fr/partenaires/greffiers-des-tribunaux-de-commerce/)

-   [Les Urssaf](https://www.qual.guichet-entreprises.fr/fr/partenaires/urssaf/)

-   [Les Chambres de Métiers et de l’Artisanat
    (CMA)](https://www.qual.guichet-entreprises.fr/fr/partenaires/chambres-de-metiers-et-de-l-artisanat-cma/)

-   [Les Chambres d’Agriculture
    (CA)](https://www.qual.guichet-entreprises.fr/fr/partenaires/chambres-d-agriculture-ca/)

-   [La Chambre Nationale de la Batellerie Artisanale
    (CNBA)](https://www.qual.guichet-entreprises.fr/fr/partenaires/chambre-nationale-de-la-batellerie-artisanale-cnba/)

-   [L’Institut national de la propriété industrielle
    (INPI)](https://www.qual.guichet-entreprises.fr/fr/partenaires/institut-national-de-la-propriete-industrielle-inpi/)
