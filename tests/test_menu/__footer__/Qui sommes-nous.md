﻿Qui sommes-nous ?
=================

Le site guichet-entreprises.fr est conçu et développé par le service à
compétence nationale « Guichet Entreprises », créé par l’arrêté du 22 avril 2015
(JORF du 25 avril 2015). Il est placé sous l’autorité de la direction générale
des entreprises au sein du ministère de l’économie et des finances.

Le service à compétence nationale « Guichet Entreprises » gère les sites
Internet guichet-entreprises.fr et guichet-qualifications.fr [1] qui, à eux
deux, constituent le guichet unique électronique défini par les directives
européennes 2006/123/CE et 2005/36/CE. Des guichets de ce type existent dans
toute l’Europe et sont fédérés au sein du projet
« [EUGO ](http://ec.europa.eu/internal_market/eu-go/index_fr.htm)» de la
Commission européenne.

Au service Guichet Entreprises, nous avons pour ambition de donner la
possibilité aux futurs créateurs et aux dirigeants d’entreprise de réaliser, en
ligne, les formalités liées à la vie d’une entreprise.

Ainsi, sur guichet-entreprises.fr tout entrepreneur peut accomplir à distance
les formalités et procédures nécessaires à la création, aux modifications de la
situation et à la cessation d’activité d’une entreprise.

Pour la création d’entreprise, le site guichet-entreprises.fr enregistre les
formalités de création et les transmet au centre de formalités des entreprises
(CFE) concerné de l’un des six réseaux :

-   les Chambres de commerce et d’industrie (CCI) –
    [www.cci.fr](http://www.cci.fr)

-   les Chambres d’agriculture –
    [www.chambres-agriculture.fr](http://www.chambres-agriculture.fr)

-   les Chambres de métiers et de l’artisanat (CMA) –
    [www.artisanat.fr](http://www.artisanat.fr)

-   les Urssaf – [www.urssaf.fr](http://www.urssaf.fr)

-   les Greffiers des tribunaux de commerce –
    [www.cngtc.fr](http://www.cngtc.fr)

-   la Chambre nationale de la batellerie artisanale (CNBA) –
    [www.cnba-transportfluvial.fr](http://www.cnba-transportfluvial.fr)

Guichet-entreprises.fr offre une information complète sur les formalités,
procédures et exigences relatives à la vie d’une entreprise ou à l’exercice
d’une activité réglementée par le biais des fiches « activités règlementées ».

Guichet-entreprises.fr s’adresse à tous les Français mais aussi à tous les
résidents de l’Union européenne ou de l’Espace économique européen. Il les
informe sur les possibilités de s’implanter durablement (libre établissement) ou
d’exercer temporairement (libre prestation de services) en France, et leur
permet d’effectuer toutes les formalités en ligne.

[1] Le site Internet
[guichet-qualifications.fr](http://www.guichet-qualifications.fr) encourage la
mobilité professionnelle en donnant au citoyen l’information la plus complète
possible sur l’accès et l’exercice des professions réglementées en France.
