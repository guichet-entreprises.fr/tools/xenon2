﻿Mentions légales Guichet-entreprises.fr
=======================================

Identification de l’éditeur <!-- collapsable:open -->
----------------------------

Service à Compétence Nationale (SCN) Guichet Entreprises

120 rue de Bercy, 75572 Paris Cedex 12

Le service Guichet Entreprises, créé par l’arrêté du 22 avril 2015 ([JORF du 22
avril
2015](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A1B17F7C2CD76682B8F5E3E5CE690458.tpdila13v_1?cidTexte=JORFTEXT000030517635&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000030517419)),
est placé sous l’autorité de la direction générale des Entreprises du ministère
de l’Economie et des Finances.

La conception éditoriale, le suivi, la maintenance technique et les mises à jour
du site internet www.guichet-entreprises.fr sont assurés par le service Guichet
Entreprises.

Directeur de la publication 
----------------------------

Monsieur Florent Tournois, chef du service Guichet Entreprises.

Prestataire d’hébergement 
--------------------------

Cloud Temple, 215 avenue Georges Clémenceau 92024 Nanterre – 01 41 91 77 77

Informatiques et libertés
-------------------------

En application de la loi Informatique et Libertés du 6 janvier 1978, vous
disposez d’un droit d’accès, de rectification, de modification et de suppression
des données qui vous concernent. Vous pouvez exercer ce droit de plusieurs
façons :

-   en vous adressant au Centre de Formalités des Entreprises (CFE) destinataire
    du dossier de déclaration ;

-   en envoyant un courriel au support :
    support.guichet-entreprises\@helpline.fr

-   en envoyant un courrier à :

SCN Guichet Entreprises

120 rue de Bercy – Télédoc 766

75572 Paris cedex 12

Traitement des données personnelles
-----------------------------------

Le site www.guichet-entreprises.fr a pour objet principal la dématérialisation
des formalités de création d’entreprise et modification ou cessation d’activité.
Il offre un service informatique sécurisé vous permettant, selon votre choix,
de :

-   transmettre un dossier unique tel que défini à l’article R123-23 du code de
    commerce dès lors qu’il respecte les dispositions de l’article R123-24 du
    code de commerce ;

-   préparer un tel dossier de manière interactive et le transmettre.

Dans le cadre des formalités effectuées sur ce portail, comme celles de la
création d’entreprise, des paiements en ligne peuvent être demandés. Ceux-ci
sont sécurisés.

Les données recueillies permettent de constituer le « dossier unique » tel
qu’attendu au titre de l’article R123-7 du code de commerce.

En vertu de l’article R123-3 du code de commerce, les données recueillies sont
transmises aux fins de traitement aux organismes suivants :

*« 1° Sous réserve des dispositions des 2° et 3°, les chambres de commerce et
d’industrie territoriales créent et gèrent les centres de formalités des
entreprises compétents pour :*

*a) Les commerçants ;*

*b) Les sociétés commerciales.*

*2° Les chambres de métiers et de l’artisanat de région créent et gèrent les
centres compétents pour les personnes physiques et les sociétés assujetties à
l’immatriculation au répertoire des métiers et pour les personnes physiques
bénéficiant de la dispense d’immatriculation prévue au V de l’article 19 de la
loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du
commerce et de l’artisanat, à l’exclusion des personnes mentionnées au 3° du
présent article.*

*3° La chambre nationale de la batellerie artisanale crée et gère le centre
compétent pour les personnes physiques et les sociétés assujetties à
l’immatriculation au registre des entreprises de la batellerie artisanale.*

*4° Les greffes des tribunaux de commerce ou des tribunaux de grande instance
statuant commercialement créent et gèrent les centres compétents pour :*

*a) Les sociétés civiles et autres que commerciales ;*

*b) Les sociétés d’exercice libéral ;*

*c) Les personnes morales assujetties à l’immatriculation au registre du
commerce et des sociétés autres que celles mentionnées aux 1°,2° et 3° ;*

*d) Les établissements publics industriels et commerciaux ;*

*e) Les agents commerciaux ;*

*f) Les groupements d’intérêt économique et les groupements européens d’intérêt
économique.*

*5° Les unions de recouvrement des cotisations de sécurité sociale et
d’allocations familiales (URSSAF) ou les caisses générales de sécurité sociale
créent et gèrent les centres compétents pour :*

*a) Les personnes exerçant, à titre de profession habituelle, une activité
indépendante réglementée ou non autre que commerciale, artisanale ou agricole ;*

*b) Les employeurs dont les entreprises ne sont pas immatriculées au registre du
commerce et des sociétés, au répertoire des métiers ou au registre des
entreprises de la batellerie artisanale, et qui ne relèvent pas des centres
mentionnés au 6°.*

*6° Les chambres d’agriculture créent et gèrent les centres compétents pour les
personnes physiques et morales exerçant à titre principal des activités
agricoles […]. »*

Stockage des données personnelles
---------------------------------

Au titre de l’article R123-27 du code de commerce, *« si le déclarant utilise un
service de conservation provisoire des données proposé par le service de
déclaration dans des conditions conformes à la loi n° 78-17 du 6 janvier 1978
relative à l’informatique, aux fichiers et aux libertés, il est procédé, à
l’issue de la période de conservation provisoire d’une durée maximale de douze
mois, à l’effacement de toutes les données et de tous les fichiers concernant le
déclarant sur les supports informatiques où ils figurent. Le déclarant en est
avisé préalablement par voie électronique ou, à défaut, par lettre simple. »*

Au titre de l’article R123-19 du code de commerce, les renseignements destinés à
être portés sur un registre de publicité légale peuvent être conservés par le
centre de formalité des entreprises (CFE) pendant un délai de trois ans.

Au titre de l’ordonnance du 24 août 2011 (dite ordonnance « Paquet Télécom »)
qui modifie l’article 32 II de la loi du 6 janvier 1978, les *cookies*
automatiques mis en œuvre par ce site ont pour finalité exclusive de permettre
ou faciliter la communication par voie électronique et/ou sont strictement
nécessaires à la fourniture d’un service de communication en ligne à la demande
expresse de l’utilisateur.

Droits de reproduction
----------------------

Le contenu de ce site relève de la législation française et internationale sur
le droit d’auteur et la propriété intellectuelle.

L’ensemble des éléments graphiques du site est la propriété du service Guichet
Entreprises. Toute reproduction ou adaptation des pages du site qui en
reprendrait les éléments graphiques est strictement interdite.

Toute utilisation des contenus à des fins commerciales est également interdite.

Toute citation ou reprise de contenus du site doit avoir obtenu l’autorisation
du directeur de la publication. La source (www.guichet-entreprises.fr) et la
date de la copie devront être indiquées ainsi que le copyright du service
Guichet Entreprises.

Liens vers les pages du site
----------------------------

Tout site public ou privé est autorisé à établir des liens vers les pages du
site www.guichet-entreprises.fr. Il n’y a pas à demander d’autorisation
préalable. Cependant, l’origine des informations devra être précisée, par
exemple sous la forme : « Création d’entreprise (source :
www.guichet-entreprises.fr, un site du service Guichet Entreprises). » Les pages
du site www.guichet-entreprises.fr ne devront pas être imbriquées à l’intérieur
des pages d’un autre site. Elles devront être affichées dans une nouvelle
fenêtre ou un nouvel onglet.

Liens vers les pages de sites extérieurs
----------------------------------------

Les liens présents sur le site www.guichet-entreprises.fr peuvent orienter
l’utilisateur sur des sites extérieurs dont le contenu ne peut en aucune manière
engager la responsabilité du SCN Guichet Entreprises.

Environnement technique
-----------------------

Certains navigateurs peuvent bloquer par défaut l’ouverture de fenêtres sur ce
site. Afin de vous permettre d’afficher certaines pages, vous devez autoriser
l’ouverture des fenêtres lorsque le navigateur vous le propose en cliquant sur
le bandeau d’avertissement alors affiché en haut de la page.

En cas d’absence de message d’avertissement de la part de votre navigateur, vous
devez configurer celui-ci afin qu’il autorise l’ouverture des fenêtres pour le
site www.guichet-entreprises.fr.
