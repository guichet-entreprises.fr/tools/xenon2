﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
|                                                                             |
|         Code generated. Automatically generated file; DO NOT EDIT           |
|                                                                             |
+-------------------------------------------------------------------------- -->
<!-- begin-ref(list-reference-fr-directive-qualification-professionnelle-artisanat) -->
  - [Boucher](./boucher.md "Boucher")
  - [Boulanger-pâtissier](./boulanger-patissier.md "Boulanger-pâtissier")
  - [Carreleur](./carreleur.md "Carreleur")
  - [Carrossier](./carrossier.md "Carrossier")
  - [Charcutier](./charcutier.md "Charcutier")
  - [Chauffagiste](./chauffagiste.md "Chauffagiste")
  - [Climaticien](./climaticien.md "Climaticien")
  - [Coiffeur à domicile](./coiffeur-a-domicile.md "Coiffeur à domicile")
  - [Coiffeur en salon](./coiffeur-en-salon.md "Coiffeur en salon")
  - [Couvreur](./couvreur.md "Couvreur")
  - [Electricien](./electricien.md "Electricien")
  - [Esthéticien](./estheticien.md "Esthéticien")
  - [Glacier](./glacier.md "Glacier")
  - [Installateur de réseaux d'eau](./installateur-de-reseaux-deau.md "Installateur de réseaux d'eau")
  - [Installateur de réseaux de gaz](./installateur-de-reseaux-de-gaz.md "Installateur de réseaux de gaz")
  - [Maçon](./macon.md "Maçon")
  - [Maréchal-ferrant](./marechal-ferrant.md "Maréchal-ferrant")
  - [Menuisier-charpentier](./menuisier-charpentier.md "Menuisier-charpentier")
  - [Peintre](./peintre.md "Peintre")
  - [Personnel des installations de combustions supérieures à 400kW](./personnel-des-installations-combustions.md "Personnel des installations de combustions supérieures à 400kW")
  - [Photographe navigant](./photographe-navigant.md "Photographe navigant")
  - [Plaquiste-platrier](./plaquiste-platrier.md "Plaquiste-platrier")
  - [# Plombier](./plombier.md "# Plombier")
  - [Poissonnier](./poissonnier.md "Poissonnier")
  - [Pyrotechnicien en chef](./pyrotechnicien-en-chef.md "Pyrotechnicien en chef")
  - [Ramoneur](./ramoneur.md "Ramoneur")
  - [Réparateur d'automobiles](./reparateur-dautomobiles.md "Réparateur d'automobiles")
  - [Réparateur de cycles](./reparateur-de-cycles.md "Réparateur de cycles")
  - [Réparateur de matériels agricoles et forestiers](./reparateur-de-materiels-agricoles.md "Réparateur de matériels agricoles et forestiers")
  - [Réparateur de matériels de travaux publics](./reparateur-de-materiels-de-travaux.md "Réparateur de matériels de travaux publics")
  - [Réparateur de motocycles](./reparateur-de-motocycles.md "Réparateur de motocycles")
<!-- end-ref -->