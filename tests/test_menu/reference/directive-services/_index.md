﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(type)    ="description" -->
<!-- var(lang)    ="fr"     -->
<!-- var(category)="Directive Services" -->


# Directive Services

Liste :
<!-- begin-include(list-reference-fr-directive-services) -->
  - [Alimentation](./alimentation/_index.md "Alimentation")
  - [Autres services](./autres-services/_index.md "Autres services")
  - [Bâtiment – Immobilier](./batiment-immobilier/_index.md "Bâtiment – Immobilier")
  - [Enseignement](./enseignement/_index.md "Enseignement")
  - [Expertise](./expertise/_index.md "Expertise")
  - [Négoce et Commerce de biens](./negoce-et-commerce-de-biens/_index.md "Négoce et Commerce de biens")
  - [Secteur animalier](./secteur-animalier/_index.md "Secteur animalier")
  - [Secteur financier et juridique](./secteur-financier-et-juridique/_index.md "Secteur financier et juridique")
  - [Tourisme, Loisirs, Culture](./tourisme-loisirs-culture/_index.md "Tourisme, Loisirs, Culture")
<!-- end-include -->