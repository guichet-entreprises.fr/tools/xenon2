﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(type)    ="description" -->
<!-- var(lang)    ="fr"     -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)  ="Alimentation"   -->

# Alimentation

Liste :
<!-- begin-include(list-reference-fr-directive-services-alimentation) -->
  - [Boucher](./boucher.md "Boucher")
  - [Boulanger](./boulanger.md "Boulanger")
  - [Charcutier](./charcutier.md "Charcutier")
  - [Restauration traditionnelle](./restauration-traditionnelle.md "Restauration traditionnelle")
  - [Débitant de boissons](./debitant-de-boissons.md "Débitant de boissons")
  - [Pâtissier](./patissier.md "Pâtissier")
  - [Restauration rapide - Vente à emporter](./restauration-rapide-vente-a-emporter.md "Restauration rapide - Vente à emporter")
  - [Commerce de détail alimentaire](./commerce-de-detail-alimentaire.md "Commerce de détail alimentaire")
  - [Confiseur-glacier](./confiseur-glacier.md "Confiseur-glacier")
  - [Salon de thé](./salon-de-the.md "Salon de thé")
  - [Poissonnier](./poissonnier.md "Poissonnier")
<!-- end-include -->