﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(type)    ="description" -->
<!-- var(lang)    ="fr"     -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)  ="Autres services"   -->

# Autres services

Liste :
<!-- begin-include(list-reference-fr-directive-services-autres-services) -->
  - [Coiffeur en salon](./coiffeur-en-salon.md "Coiffeur en salon")
  - [Teinturerie non industrielle](./teinturerie-non-industrielle.md "Teinturerie non industrielle")
  - [Crèche – Accueil d’enfants de moins de 6 ans](./creche-accueil-denfants-de-moins.md "Crèche – Accueil d’enfants de moins de 6 ans")
  - [Entretien et réparation de véhicules](./entretien-et-reparation-de-vehicules.md "Entretien et réparation de véhicules")
  - [Établissement recevant du public](./etablissement-recevant-du-public.md "Établissement recevant du public")
  - [Exploitant de station de bronzage](./exploitant-de-station-de-bronzage.md "Exploitant de station de bronzage")
  - [Institut de beauté-esthéticienne](./institut-de-beaute-estheticienne.md "Institut de beauté-esthéticienne")
  - [Journaliste](./journaliste.md "Journaliste")
  - [Maisons de retraite – Accueil collectif de personnes âgées](./maisons-de-retraite-accueil-collectif.md "Maisons de retraite – Accueil collectif de personnes âgées")
  - [Organisme privé de placement de personnel](./organisme-prive-de-placement-personnel.md "Organisme privé de placement de personnel")
  - [Services à la personne](./services-a-la-personne.md "Services à la personne")
  - [Agence de mannequins](./agence-de-mannequins.md "Agence de mannequins")
  - [Coiffeur à domicile](./coiffeur-a-domicile.md "Coiffeur à domicile")
  - [Gestion des déchets et recyclage](./gestion-des-dechets-et-recyclage.md "Gestion des déchets et recyclage")
  - [Laverie libre-service](./laverie-libre-service.md "Laverie libre-service")
  - [Maréchal-ferrant](./marechal-ferrant.md "Maréchal-ferrant")
  - [Services funéraires](./services-funeraires.md "Services funéraires")
  - [Tatouage-perçage](./tatouage-percage.md "Tatouage-perçage")
  - [Télésurveillance et sécurité électronique](./telesurveillance-et-securite-electronique.md "Télésurveillance et sécurité électronique")
  - [Société de domiciliation](./societe-de-domiciliation.md "Société de domiciliation")
  - [Collecte des déchets : huiles](./collecte-des-dechets-huiles.md "Collecte des déchets : huiles")
  - [Collecte des déchets : pneus](./collecte-des-dechets-pneus.md "Collecte des déchets : pneus")
  - [Traitement des déchets : huiles](./traitement-des-dechets-huiles.md "Traitement des déchets : huiles")
  - [Traitement des déchets : pneus](./traitement-des-dechets-pneus.md "Traitement des déchets : pneus")
<!-- end-include -->