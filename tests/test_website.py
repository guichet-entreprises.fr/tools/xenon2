#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import logging
import sys
import os
import os.path

import xe2.website
import xe2layout.config


def test_conf_gen():
    filename = os.path.join(__get_this_folder(), "test_website",
                            "test01", "index.md")
    conf_filename = os.path.join(__get_this_folder(), "test_website",
                                 "test01", "index.yml")
    xe2.website.create_conf(filename)

    conf = xe2layout.config.read_yaml(conf_filename)
    # pprint.pprint(content)
    # pprint.pprint(conf)
    # print(os.path.isfile(conf['paths']['template_conf']))
    # print(os.path.isfile(conf['paths']['entries']['main']['index']))

    # assert os.path.isfile(conf['paths']['template_conf'])
    assert os.path.isfile(conf['paths']['entries']['main']['index'])

def test_website():
    test_num = 6
    filename = os.path.join(__get_this_folder(), "test_website",
                            "test%02d" % test_num, "index.md")
    conf_filename = xe2.website.create_conf(filename)
    site = xe2.website.WebSite(conf_filename, 'main')
    site.generate()
    # print(site)


def test_multi():
    test_num = "multilink"
    filename = os.path.join(__get_this_folder(), "test_website",
                            "test-%s" % test_num, "index.md")
    conf_filename = xe2.website.create_conf(filename)
    site = xe2.website.WebSite(conf_filename, 'main')
    assert len(site.res.edges) == 1
    for (src, dest) in site.res.edges:
        assert len(site.res.edges[src, dest]['links']) > 1

def try_gp():
    # filename = os.path.join(__get_this_folder(), "..", "..",
    #                         "www.guichet-partenaires.fr", "xenon2.yml")
    filename = os.path.join(__get_this_folder(), "..", "..",
                            "www.guichet-qualifications.fr", "xenon2.yml")
    # conf_filename = xe2.website.create_conf(filename)
    site = xe2.website.generate_site(filename)
    # site.generate()
    # print(site.res.graph['menu'])
    # result = xe2.website.prepare_menu_links('index.md', site.res)
    # print(result)
    # assert len(site.res.edges) == 1
    # for (src, dest) in site.res.edges:
    #     assert len(site.res.edges[src, dest]['links']) > 1


def test_compute_url():
    source = "/a/b/c/"
    destin = "/a/b/c/d/e/f.md"
    # print(xe2.website.compute_url(source, destin))
    assert xe2.website.compute_url(source, destin) == "./d/e/f.md"

    source = r"c:\dev\projet-ge.fr\win-tools\xenon2\examples" \
        r"\example05\input-md"
    destin = r"c:\dev\projet-ge.fr\win-tools\xenon2\examples" \
        r"\example05\input-md\footer\./xx/page.md"
    # print(compute_url(source, destin))
    assert xe2.website.compute_url(source, destin) == "./footer/xx/page.md"

    source = r"c:\dev\projet-ge.fr\win-tools\xenon2\examples" \
        r"\example05\output\index.html"
    destin = r"c:\dev\projet-ge.fr\win-tools\xenon2\examples" \
        r"\example05\output"
    # print(xe2.website.compute_url(source, destin))
    assert xe2.website.compute_url(source, destin) == "./.."


###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    # test_conf_gen()
    # test_website()
    # test_multi()
    try_gp()
    # test_compute_url()

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
