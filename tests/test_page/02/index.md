﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(footer_off)="Off" -->
<!-- var(nav_default_off)="Off" -->
<!-- var(nav_menu_off)="Off" -->
<!-- var(breadcrumb_off)="Off" -->
<!-- var(site:home)="Accueil" -->

Exemple de tableau
===================================================


> contenu 
> contenu 
> contenu 



| Tables   |      Are      |  Cool |
| -------- | :-----------: | ----: |
| col 1 is | left-aligned  | $1600 |
| col 2 is |   centered    |   $12 |
| col 3 is | right-aligned |    $1 |



**Avertissement relatif à la qualité de la traduction automatique**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Cette page a été traduite au moyen d'un outil de traduction automatique et peut contenir des erreurs. Nous invitons les utilisateurs à vérifier l'exactitude des informations fournies sur cette page avant d'entreprendre toute démarche.

Le service Guichet Entreprises ne saurait être tenu responsable de l'exploitation d'informations qui se révéleraient inexactes en raison d'une traduction automatique non fidèle à l'original.<!-- alert-end:warning -->


