﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(footer_off)="Off" -->
<!-- var(nav_default_off)="Off" -->
<!-- var(nav_menu_off)="Off" -->
<!-- var(breadcrumb_off)="Off" -->
<!-- var(site:home)="Accueil" -->


Example 4  <!-- section-banner:actu.png -->
===================================================

Ce qui a changé au 1er janvier

[Exemple de lien du context](https://www.google.com "title Ici")
[Exemple de lien box <!-- link-model:box -->](https://www.google.com "title Ici")
[Exemple de lien box-trans <!-- link-model:box-trans -->](https://www.google.com "title Ici")
[Exemple de lien normal <!-- link-model:normal -->](https://www.google.com "title Ici")


Ce Titre n'apparait pas  <!-- section:information -->
===================================================

Bienvenue 
----------
Simple, rapide et sécurisé : le service guichet-entreprises.fr vous permet de réaliser vos démarches en ligne.

Futur créateur
--------------
Formes juridiques, protection du nom de l’entreprise, aides publiques… Informez-vous avant de vous lancer !

Le régime du micro-entrepreneur
-------------------------------
Retrouvez toutes les informations pratiques pour démarrer l’activité de votre micro-entreprise (ex-autoentreprise).

Démarches en ligne
------------------
Réalisez vos formalités liées à l’immatriculation (toute forme juridique), à la modification et à la cessation d’activité d’une entreprise.


Une seule adresse pour créer son entreprise  <!-- section-welcome: -->
===================================================

Le service en ligne guichet-entreprises.fr encourage la création d’entreprise en France en permettant au citoyen de réaliser ses démarches administratives autour de la création d’une activité (immatriculation, demandes d’autorisation…). Il est le site des pouvoirs publics de la création d’entreprise, de la modification et de la cessation d’activité d’une entreprise. Ce service est l’initiative du ministère de l’Économie et des Finances.

Faciliter les démarches administratives, c’est encourager l’esprit d’entreprendre !


591000
------
Entreprises ont été créées en 2017 en France, soit 7 % de plus qu’en 2016 (Insee)

54975 
------
Dossiers transmis au Centre de formalités des entreprises (CFE) en 2017 via guichet-entreprises.fr

105
------
Fiches d'information dédiées aux activités règlementées en France disponibles en ligne 

2000
------
Plus de 2000 dispositifs d'aides publiques pour créer ou développer votre entreprise


Activités réglementées  <!-- section:courses -->
===================================================

Artisan du bâtiment, restaurateur, infirmier, boulanger… avant de démarrer l’activité de votre entreprise, informez-vous sur les démarches à accomplir et sur les conditions d’accès à une activité réglementée en France.

Vérifiez si votre activité est réglementée en consultant nos fiches d’information :

[Exemple de lien](https://www.google.com "title Ici")
[Exemple de lien box <!-- link-model:box -->](https://www.google.com "title Ici")
[Exemple de lien box-trans <!-- link-model:box-trans -->](https://www.google.com "title Ici")
[Exemple de lien normal <!-- link-model:normal -->](https://www.google.com "title Ici")


voici une litse : 
 - test1
 - test3
   + second niveau
   + second niveau
   + second niveau
 - test1
 - test3
 - test1
 - test3




[Exemple de lien](https://www.google.com "title Ici")
[Exemple de lien box <!-- link-model:box -->](https://www.google.com "title Ici")
[Exemple de lien box-trans <!-- link-model:box-trans -->](https://www.google.com "title Ici")
[Exemple de lien normal <!-- link-model:normal -->](https://www.google.com "title Ici")



Prêt à vous lancer ?  <!-- section:stories -->
===================================================

Le service guichet-entreprises.fr simplifie vos démarches en vous offrant une information claire et un parcours personnalisé.
Le service enregistre et transmet vos formalités au Centre de formalités des entreprises compétent (CCI, CMA, Urssaf, Greffiers des Tribunaux de commerce, Chambres d’agriculture ou Chambre nationale de la batellerie artisanale. Tous les organismes de la création d’entreprise – service des impôts des entreprises, Urssaf, Insee… – sont informés par les CFE. Vous recevrez par la suite n° Siren, code APE, n° de TVA…

Déjà inscrit ? [Se connecter <!-- link-model:normal -->](https://www.google.com "title Ici")

[Mode d'emploi](https://www.google.com "title Ici") [Créer un compte](https://www.google.com "title Ici")


Pourquoi choisir guichet-entreprises.fr ?  <!-- section-welcome: -->
===================================================

Le service en ligne est conçu pour vous faciliter le parcours de la création d'entreprise. Il vous suffit de répondre aux questions posées sur votre projet professionnel, d'y joindre les pièces justificatives et de régler les éventuels frais liés à la formalité. Votre dossier est directement envoyé à l'administration compétente où il sera traité.

[Espace personnel](http://google.fr/) <!-- image:espace-personnel.png -->
------------------------------------------------------------------------------
Gérer votre(vos) dossier(s) créé(s) sur le site, suspendre la complétion de votre dossier et le sauvegarder...

[Service sécurisé](http://google.fr/) <!-- image:espace-securise.png -->
------------------------------------------------------------------------------
Vous réalisez vos démarches en ligne, sans avoir à vous déplacer, sur une plateforme sécurisée.

[Paiement en ligne](http://google.fr/) <!-- image:paiement-securise.png -->
------------------------------------------------------------------------------
Dans le cadre d’une inscription au RCS, RM, RSAC ou RSEIRL, vous pouvez régler vos frais directement en ligne.

[Partenaires mobilisés](http://google.fr/) <!-- image:partenaires.png -->
------------------------------------------------------------------------------
Centre de formalités des entreprises (CFE), Insee, service des impôts des entreprises, Urssaf...


Entreprendre en France et en Europe  <!-- section-stories:actu.png -->
===================================================

Vous êtes ressortissant de l’Union européenne ou de l’Espace économique européen ?

Nous encourageons la création d’entreprise et la mobilité professionnelle en France et en Europe.
Guichet-entreprises.fr est le guichet unique français de la création d’entreprise, 
membre du réseau Eugo créé par la Commission européenne. 
Il vous permet de comprendre et de connaître les conditions auxquelles vous êtes soumis pour créer une activité de services.
Si vous devez faire reconnaître une qualification professionnelle, rendez-vous sur [guichet-qualifications.fr <!-- link-model:normal -->](https://www.google.com "title Ici") !

[Comment exercer en France de façon temporaire ou permanente ?<!-- link-model:normal -->](https://www.google.com "title Ici")

