﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import logging
import sys
import os
import os.path
import jinja2

import xe2.menu as menu


def test_read_menu_by_folder():
    folder_input = os.path.join(__get_this_folder(), "test_menu")
    folder = os.path.join(folder_input, "Santé")

    the_menu = menu.create_menu_from_folder(folder, folder_input)

    # print(the_menu)
    assert the_menu

def test_read_menu():
    the_folder = os.path.join(__get_this_folder(), "test_menu")
    folder_input = os.path.join(the_folder, "__menu__")
    menu_filename = os.path.join(folder_input, "menu.md")

    the_menu = menu.read_menu(menu_filename, the_folder)

    # print(the_menu)
    assert the_menu

def test_read_menu_gq():
    the_folder = os.path.join(__get_this_folder(), "test_menu")
    folder_input = os.path.join(the_folder, "gq")
    menu_filename = os.path.join(folder_input, "__menu__.md")

    the_menu = menu.read_menu(menu_filename, the_folder)

    print(the_menu)
    # assert the_menu

def test_read_menu_reference():
    the_folder = os.path.join(__get_this_folder(), "test_menu")
    folder_input = os.path.join(the_folder, "reference")
    menu_filename = os.path.join(folder_input, "_list_menu.md")

    the_menu = menu.read_menu(menu_filename, the_folder)

    assert the_menu[1].children[1].children[1].url == \
        "reference\\directive-services\\autres-services\\teinturerie-non-industrielle.md"
    # assert the_menu


###############################################################################
# test menu
###############################################################################
def try_menu(j2_filename):
    local_folder = os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]

    folder_input = os.path.join(local_folder,
                                "../../layout/jinja")

    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(folder_input),
        autoescape=jinja2.select_autoescape(['html', 'xml']))

    template = env.get_template(j2_filename)

    menu_general = [
        {'label': "Home",
         'url': 'index.html'},
        {'label': "Home2",
         'url': 'index.html'},
        {'label': "Home3",
         'url': 'index.html',
         'children': [
             {'label': "Home4",
              'url': 'index.html'},
             {'label': "Home5",
              'url': 'index.html'},
         ]},
        {'label': "Home6",
         'url': 'index.html',
         'children': [
             {'label': "Home7",
              'url': 'index.html',
              'children': [
                  {'label': "Home8",
                   'url': 'index.html'},
                  {'label': "Home9",
                   'url': 'index.html'},
              ]},
             {'label': "Home10",
              'url': 'index.html'},
         ]},
        {'label': "Home10",
         'url': 'index.html'},
        {'label': "Home11",
         'url': 'index.html'},
    ]

    context = {}
    context['menu_general'] = menu_general

    html_content = template.render(context)
    print(html_content)


###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    # test_read_menu_by_folder()
    # test_read_menu()
    # test_read_menu_reference()
    test_read_menu_gq()

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
