#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

###############################################################################
# standard object to wrap file and access easily to the filename
#
###############################################################################

import logging
import sys
import os
import os.path
import re
from urllib.parse import urljoin, urlparse, urlunparse
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import urltools
import pydot

import pymdtools.common as common
import pymdtools.mdcommon as mdcommon
import pymdtools.markdownify as mkfy
import pymdtools.normalize as normalize


###############################################################################
# try to manage the exception handling
#
# @param kwargs list of arguments to print
# @return the wrap function
###############################################################################
def handle_exception(action_desc, **kwargs_print_name):
    def decorator(the_decorated_function):
        # ---------------------------------------------------------------------
        def wrapper(*args, **kwargs):
            try:
                return the_decorated_function(*args, **kwargs)
            except Exception as ex:
                message = "%s (%s)\n" % (action_desc,
                                         the_decorated_function.__name__)
                for key in kwargs_print_name:
                    if key in kwargs:
                        message += "%s : %s\n" % (kwargs_print_name[key],
                                                  kwargs[key])
                raise type(ex)("%s\n%s" % (str(ex), message))
        # ---------------------------------------------------------------------

        wrapper.__name__ = the_decorated_function.__name__
        wrapper.__doc__ = the_decorated_function.__doc__
        wrapper.__dict__.update(the_decorated_function.__dict__)
        return wrapper

    return decorator


###############################################################################
# return the domain name
#
# @param url
# @return the domain name or the url if its not external
###############################################################################
def get_domain_name(url):
    return str(urlparse(url).netloc)

###############################################################################
# Get page filename
#
# @param url the url to grab
# @param root_url the root url to compute the destination folder
# @param root_dest_folder the root destination folder
# @return the content of the web url
###############################################################################
def get_page_folder(url, root_url, root_dest_folder):
    root_url = urltools.normalize(root_url)
    url = urltools.normalize(url)
    page_dest_folder = os.path.join(root_dest_folder, url[len(root_url):])
    page_dest_folder = common.check_create_folder(page_dest_folder)
    return page_dest_folder


###############################################################################
# Get page filename
#
# @param url the url to grab
# @param root_url the root url to compute the destination folder
# @param root_dest_folder the root destination folder
# @return the content of the web url
###############################################################################
def get_page_filename(url, root_url, root_dest_folder):
    return os.path.join(get_page_folder(url, root_url, root_dest_folder),
                        "page.html")


###############################################################################
# Get page content (only content) filename
#
# @param url the url to grab
# @param root_url the root url to compute the destination folder
# @param root_dest_folder the root destination folder
# @return the content of the web url
###############################################################################
def get_content_page_filename(url, root_url, root_dest_folder):
    return os.path.join(get_page_folder(url, root_url, root_dest_folder),
                        "content.html")


###############################################################################
# Get image filename
#
# @param url the url to grab
# @param root_url the root url to compute the destination folder
# @param root_dest_folder the root destination folder
# @return the content of the web url
###############################################################################
def get_image_filename(url, root_url, root_dest_folder):
    return os.path.join(get_page_folder(url, root_url, root_dest_folder),
                        "image.png")


###############################################################################
# Get content from web and save it (for cache)
# save the content in page.html file
#
# @param url the url to grab
# @param root_url the root url to compute the destination folder
# @param dest_folder the root destination folder
# @return the content of the web url
###############################################################################
def get_from_web(url, root_url, root_dest_folder):
    url = urltools.normalize(url)
    page_file = get_page_filename(url, root_url, root_dest_folder)

    result = ""
    if not os.path.isfile(page_file):
        result = requests.get(url).text
        common.set_file_content(page_file, result)
    else:
        result = common.get_file_content(page_file)

    return result


###############################################################################
# Construct an id from an url
#
# @param url the url to test
# @return the result id.
###############################################################################
def id_from_url(url):
    url = urlunparse(urlparse(url)._replace(fragment=''))
    url = urltools.normalize(url)
    result = common.get_valid_filename(url, char_to_replace=r'[\\/*?:"<>|()-]')
    return result

###############################################################################
# Get the canonical url
#
# @param url the url to test
# @return the result url.
###############################################################################
def norm_url(url):
    url = urlunparse(urlparse(url)._replace(fragment=''))
    url = urltools.normalize(url)
    return str(url)

###############################################################################
# test if the url is absolute url
#
# @param url the url to test
# @return the test result.
###############################################################################
def is_absolute(url):
    return bool(urlparse(url).netloc)

###############################################################################
# Get links
#
# @param url the url to scan
# @return the screenshot path.
###############################################################################
def get_links(url, html_content):
    soup = BeautifulSoup(html_content, "html5lib")
    links = []

    for link in soup.findAll('a', attrs={'href': True}):
        link_url = norm_url(urljoin(url, link.get('href')))
        if not link_url.startswith("javascript") and \
                not link_url.startswith("mailto"):
            if link_url not in links:
                links.append(link_url)

    return links

###############################################################################
# Get links
#
# @param url the url to scan
# @return the screenshot path.
###############################################################################
def get_site_links(url, root_url, root_dest_folder, max_links=10000):
    root_url = urltools.normalize(root_url)
    url = urltools.normalize(url)
    result = []
    count = 0

    # ------------------------------------------------------------------------
    def scan(url, root_url, root_dest_folder, count):
        if count > max_links:
            return count
        url = norm_url(url)
        if url in result:
            return count

        count += 1
        logging.info("%04d/%04d : scanning %s", count, max_links, url)
        result.append(url)
        links = get_links(url, get_from_web(url, root_url, root_dest_folder))
        for link in links:
            if (link not in result) and (count < max_links):
                if link.startswith(root_url):
                    if "%20" in link:
                        print("######")
                        print("url=%s link=%s" % (url, link))
                        print("######")
                    else:
                        count = scan(link, root_url, root_dest_folder, count)
                else:
                    # add only the domain for external links
                    domain = get_domain_name(link)
                    if domain not in result:
                        result.append(domain)
        return count
    # ------------------------------------------------------------------------

    scan(root_url, root_url, root_dest_folder, count)
    result.sort()
    return result

###############################################################################
# create a page node
#
# @param url the url to scan
# @return the screenshot path.
###############################################################################
def create_page_node(url, page_dest_folder, image=False):
    name_id = id_from_url(url)
    table_img = '< <TABLE  FIXEDSIZE="TRUE" WIDTH="1" TITLE="%s" ' \
        '  BORDER="0" CELLBORDER="0" CELLPADDING="4" ' \
        '  CELLSPACING="0">' \
                '<TR><TD VALIGN="MIDDLE" WIDTH="1"><B>%s</B></TD></TR>' \
                '<TR><TD FIXEDSIZE="TRUE" WIDTH="1">' \
                '<IMG SCALE="WIDTH" SRC="%s" />' \
                '</TD></TR></TABLE> >' % (url, url,
                                          os.path.join(page_dest_folder,
                                                       "image.png"))
    labelloc = "t"
    if not image:
        table_img = url
        labelloc = "c"
    result = pydot.Node(name=name_id,
                        URL=url, shape="box",
                        fontname="Arial", fontsize=8,
                        labelloc=labelloc,
                        style="filled,setlinewidth(0)",
                        fillcolor="#0092bc",
                        fontcolor="#ffffff",
                        margin=0, width=5,
                        label=table_img)
    return result

###############################################################################
# create a page node
#
# @param url the url to scan
# @return the screenshot path.
###############################################################################
def create_ext_domain_node(url):
    name_id = id_from_url(url)

    result = pydot.Node(name=name_id, label=url, URL=url,
                        shape="house",
                        #  fixedsize=True, width=2,
                        fontname="Arial", fontsize=8,
                        style="filled,setlinewidth(0)",
                        fillcolor="#e53138",
                        fontcolor="#ffffff",
                        margin=0, width=3)
    return result

###############################################################################
# create a page node
#
# @param url the url to scan
# @return the screenshot path.
###############################################################################
def create_edge(url_src, url_dest):
    src_id = id_from_url(url_src)
    dest_id = id_from_url(url_dest)

    result = pydot.Edge(src=src_id, dst=dest_id,
                        arrowsize=2,
                        style="setlinewidth(2)",
                        color="#848687")

    return result

###############################################################################
# Get links
#
# @param url the url to scan
# @return the screenshot path.
###############################################################################
def get_graph_links(root_url, urls, root_dest_folder):
    root_url = norm_url(root_url)
    logging.info("Create graph %s", root_url)
    site = pydot.Dot(graph_type='digraph',
                     fontname="Arial",
                     compound='true', size="2000",
                     #  splines="curved",
                     overlap="prism")

    external = pydot.Cluster('ext', label='External resources',
                             fontname="Arial", style="bold",
                             fontcolor="#848687", color="#848687")
    site.add_subgraph(external)

    count = 0
    count_max = len(urls)
    for url in urls:
        count += 1
        logging.info("%04d/%04d : Create node with %s", count, count_max, url)
        name_id = id_from_url(url)
        if len(site.get_node(name_id)) > 0:
            logging.error('"%s" id is already defined', (name_id))
            raise Exception('"%s" id is already defined' % (name_id))

        if url.startswith(root_url):
            page_dest_folder = get_page_folder(url, root_url, root_dest_folder)
            site.add_node(create_page_node(url, page_dest_folder))
        else:
            external.add_node(create_ext_domain_node(url))

    count = 0
    for url in urls:
        count += 1
        logging.info("%04d/%04d : Create edge with %s", count, count_max, url)
        if url.startswith(root_url):
            content_filename = get_content_page_filename(url, root_url,
                                                         root_dest_folder)
            if url == root_url:
                content_filename = get_page_filename(url, root_url,
                                                     root_dest_folder)
            links = get_links(url, common.get_file_content(content_filename))
            filtered_links = []
            for dest in links:
                link = dest
                if not link.startswith(root_url):
                    link = get_domain_name(link)
                if link not in filtered_links:
                    filtered_links.append(link)

            for link in filtered_links:
                logging.info("%04d/%04d : Create edge destination from %s to %s",
                             count, count_max, url, link)
                site.add_edge(create_edge(url, link))

    return site


###############################################################################
# Get content of the website
#
# @param url the url to scan
# @param dest_folder the folder to save filess
###############################################################################
def grab_page(url, root_url, root_dest_folder, function_extract_content=None):
    root_url = norm_url(root_url)
    url = norm_url(url)
    if not url.startswith(root_url):
        return

    html_page = get_from_web(url, root_url, root_dest_folder)
    screenshot(url, root_url, root_dest_folder)

    if function_extract_content is not None:
        content_html = function_extract_content(html_page)
        content_folder = get_page_folder(url, root_url, root_dest_folder)
        content_filename = get_content_page_filename(url, root_url,
                                                     root_dest_folder)
        common.set_file_content(content_filename, content_html)
        # md_content = pypandoc.convert_text(
        #     content_html, 'md', format='html')
        md_content = mkfy.markdownify(content_html,
                                      heading_style=mkfy.UNDERLINED,
                                      bullets="-+*")

        common.set_file_content(os.path.join(content_folder, "content.md"),
                                md_content)


###############################################################################
# Get content of the website
#
# @param url the url to scan
# @param dest_folder the folder to save filess
###############################################################################
def grab_website(root_url, root_dest_folder, function_extract_content=None):
    root_url = norm_url(root_url)
    root_dest_folder = common.check_create_folder(root_dest_folder)
    urls = get_site_links(root_url, root_url, root_dest_folder,
                          max_links=10000)
    count = 0
    count_max = len(urls)
    for url in urls:
        count += 1
        logging.info("%04d/%04d : work with %s", count, count_max, url)
        grab_page(url, root_url, root_dest_folder,
                  function_extract_content=function_extract_content)

    # graph = get_graph_links(root_url, urls, root_dest_folder)
    # logging.info("Create dot file")
    # graph.write_dot(os.path.join(root_dest_folder, 'site.dot'))
    # logging.info("Create svg graph with dot")
    # graph.write_svg(os.path.join(root_dest_folder, 'site.svg'),
    #                 prog="dot", encoding="utf-8")
    # logging.info("Create svg graph with twopi")
    # graph.write_svg(os.path.join(root_dest_folder, 'site_2PI.svg'),
    #                 prog="twopi", encoding="utf-8")
    # logging.info("Create svg graph with circo")
    # graph.write_svg(os.path.join(root_dest_folder, 'site_circo.svg'),
    #                 prog="circo", encoding="utf-8")

###############################################################################
# Get content
#
# @param url the url to scan
# @return the screenshot path.
###############################################################################
def get_url_page_content(html_page):
    soup = BeautifulSoup(html_page, "html5lib")

    contents = soup.findAll("div", attrs={"id": "content",
                                          "class": "site-content"})
    result = ""
    for element in contents:
        result += str(element)

    result = re.sub(r"<!--(?P<comment>[\s\S]*?)-->", "", result)

    return result

###############################################################################
# Get a screenshot
#
# @param url the url to shot
# @return the screenshot path.
###############################################################################
def screenshot(url, root_url, root_dest_folder):
    phantom_js_exe = os.path.join(get_local_folder(), "../",
                                  "third_party_software",
                                  "phantomjs-2.1.1-windows/bin",
                                  "phantomjs.exe")
    phantom_js_exe = common.check_is_file_and_correct_path(phantom_js_exe)

    url = urltools.normalize(url)
    image_filename = get_image_filename(url, root_url, root_dest_folder)

    if os.path.isfile(image_filename):
        return image_filename

    driver = webdriver.PhantomJS(phantom_js_exe)
    driver.set_window_size(1024, 768)
    driver.get(url)
    driver.save_screenshot(image_filename)
    driver.quit()

    return None

###############################################################################
# Get the local folder of this script
#
# @return the local folder.
###############################################################################
def get_local_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)


###############################################################################
# Launch the test
###############################################################################
def __launch_test():
    import pytest
    pytest.main(__get_this_filename())


###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    # screenshot('http://www.guichet-entreprises.fr/')
    # site = get_graph_links('https://www.guichet-entreprises.fr/')
    # site.write_svg('out.svg')

    # os.environ.setdefault('PYPANDOC_PANDOC',
    #                       os.path.join(get_local_folder(), "../../",
    #                                    "third_party_software",
    #                                    "pandoc-2.2.1", "pandoc.exe"))

    # the_folder = os.path.join(
    #     get_local_folder(), "../../examples/extract-gq.fr/output/")
    # the_folder = "C:\\dev\\extract\\gq_en\\"
    # # the_folder = "C:\\dev\\extract\\gq_fr\\"
    # # the_folder = "C:\\dev\\extract\\ge_fr\\"
    # # the_folder = "C:\\dev\\extract\\ge_en\\"

    today = common.get_today()

    sites = [
        {'url': "https://www.guichet-entreprises.fr/fr/",
         'dest': 'C:\\dev\\extract-%s\\ge_fr\\' % today},
        {'url': "https://www.guichet-entreprises.fr/en/",
         'dest': 'C:\\dev\\extract-%s\\ge_en\\' % today},
        {'url': "https://www.guichet-qualifications.fr/fr/",
         'dest': 'C:\\dev\\extract-%s\\gq_fr\\' % today},
        {'url': "https://www.guichet-qualifications.fr/en/",
         'dest': 'C:\\dev\\extract-%s\\gq_en\\' % today},
    ]

    for site in sites:
        grab_website(site['url'], site['dest'],
                     function_extract_content=get_url_page_content)

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
