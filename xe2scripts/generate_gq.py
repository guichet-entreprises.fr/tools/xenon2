#!/usr/bin/env python
# -*- coding: utf-8 -*-


import logging
import sys
import os
import os.path

import xe2.website
import xe2.manage


def check_key(conf_filename):
    # site = xe2.website.WebSite(conf_filename, 'fr')
    # xe2.manage.check_key_in_md(site)
    # result =
    xe2.manage.analyse_dead_links(conf_filename)
    # print(result)


def generate(conf_filename):
    # sites = xe2.website.generate_site(conf_filename)

    # for site in sites:
    #     xe2.website.analyse(site)
    site = xe2.manage.generate_site(conf_filename)
    # xe2.website.analyse(site)


###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    conf_ge = os.path.join(__get_this_folder(), "..", "..",
                           "www.guichet-entreprises.fr",
                           "xenon2.yml")
    conf_gq = os.path.join(__get_this_folder(), "..", "..",
                           "www.guichet-qualifications.fr",
                           "xenon2.yml")
    conf_gp = os.path.join(__get_this_folder(), "..", "..",
                           "www.guichet-partenaires.fr",
                           "xenon2.yml")

    # conf = xe2layout.config.read_yaml(conf_filename)
    # pprint.pprint(conf)

    # generate(conf_ge)
    check_key(conf_gq)

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
