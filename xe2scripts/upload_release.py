#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import os.path
import logging
import re
import codecs
from urllib.parse import urljoin, quote
import requests
import jinja2
import upref
import pymdtools.common as common

import xe2

# -----------------------------------------------------------------------------
def jinja_env():
    templates_folder = os.path.join(__get_this_folder(), "templates")
    template_env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(templates_folder),
        autoescape=jinja2.select_autoescape(['html', 'xml']))

    return template_env

# -----------------------------------------------------------------------------
def release_description(uploads):
    context = {
        "uploads": uploads,
        "version": xe2.__version_info__,
        "today": common.get_today(),
    }

    template = jinja_env().get_template("release_description.md.j2")

    return template.render(context)


# -----------------------------------------------------------------------------
def gitlab_description_upload(description, release_tag):
    data_conf = upref.load_conf(os.path.join(__get_this_folder(),
                                             "templates", "gitlab.conf"))
    data_conf["server"]["value"] = xe2.__gitlab_url__
    data_conf["project_id"]["value"] = xe2.__gitlab_project_id__
    user_data = upref.get_pref(
        data_conf, "gitlab-" + xe2.__module_name__)

    api_url = urljoin(user_data['server'] + "/",
                      "/api/v4/projects/%s/" % user_data['project_id'])
    auth = {'PRIVATE-TOKEN': user_data['private_token']}
    verify = True  # Ignore ssl certificate failures

    # Now we've got the uploaded file info, attach that to the tag
    url = urljoin(
        api_url, 'repository/tags/{t}'.format(t=quote(release_tag, safe='')))
    tag_details = requests.get(url, headers=auth, verify=verify).json()

    if 'name' not in tag_details:
        logging.error("The tag '%s' is not define.", release_tag)
        raise BaseException("The tag '%s' is not define." % release_tag)

    if 'release' in tag_details and tag_details['release'] is not None:
        logging.error("The release '%s' is already done.", release_tag)
        raise BaseException("The release '%s' is already done." % release_tag)

    rsp = requests.post(url + '/release',
                        data={'description': description},
                        headers=auth, verify=verify)

    try:
        rsp.raise_for_status()
        logging.info("Description uploaded to tag %s", release_tag)

    except BaseException as ex:
        logging.info("Setting tag description failed: "
                     "\"%s\" error: %s", description, ex)

    return release_tag


# -----------------------------------------------------------------------------
def gitlab_upload(filenames):
    if not isinstance(filenames, list):
        filenames = [filenames]

    data_conf = upref.load_conf(os.path.join(__get_this_folder(),
                                             "templates", "gitlab.conf"))
    data_conf["server"]["value"] = xe2.__gitlab_url__
    data_conf["project_id"]["value"] = xe2.__gitlab_project_id__
    user_data = upref.get_pref(
        data_conf, "gitlab-" + xe2.__module_name__)

    api_url = urljoin(user_data['server'] + "/",
                      "/api/v4/projects/%s/" % user_data['project_id'])
    auth = {'PRIVATE-TOKEN': user_data['private_token']}
    uploads = []
    verify = True  # Ignore ssl certificate failures

    logging.info("Uploading %s", filenames)

    for filename in filenames:
        with codecs.open(filename, 'rb') as filehandle:
            rsp = requests.post(urljoin(api_url, 'uploads'),
                                files={'file': filehandle},
                                headers=auth, verify=verify)
            try:
                rsp.raise_for_status()
                logging.info("Upload of %s", filename)
            except BaseException as ex:
                logging.info("Upload of %s failed: %s", filename, ex)
            else:
                uploads.append(rsp.json())

    return uploads


# -----------------------------------------------------------------------------
def upload_gitlab_release(filenames, release_tag):
    """Upload a zipfile as release of this project

    Arguments:
        filenames {str} -- list of filename
        tag {str} -- the tag to upload to
    """
    data_conf = {
        "server": {"label": "url of gitlab server"},
        "project_id": {
            "label": "Unique id of project, available "
                     "in Project Settings/General"
        },
        "private_token": {"label": "login token with permissions "
                                   "to commit to repo"},
    }
    user_data = upref.get_pref(data_conf, "xenon2-gitlab")

    server = user_data['server']
    project_id = user_data['project_id']
    private_token = user_data['private_token']

    logging.info("Uploading to %s (id: %s) @ %s",
                 server, project_id, release_tag)

    if not server.endswith('/'):
        server += '/'

    api_url = urljoin(server, "/api/v4/projects/%s/" % project_id)
    auth = {'PRIVATE-TOKEN': private_token}
    uploads = []
    verify = True  # Ignore ssl certificate failures
    logging.info("Uploading %s", filenames)

    for zipfilename in filenames:
        with codecs.open(zipfilename, 'rb') as filehandle:
            rsp = requests.post(urljoin(api_url, 'uploads'),
                                files={'file': filehandle},
                                headers=auth, verify=verify)
            try:
                rsp.raise_for_status()
            except BaseException as ex:
                logging.info("Upload of %s failed: %s", zipfilename, ex)
            else:
                uploads.append(rsp.json()['markdown'])

    def fix_markdown(match):
        return "[%s](%s)" % (match.group(1), quote(match.group(2), safe='/:'))

    uploads = [re.sub(r'^\[(.*)\]\((.*)\)$', fix_markdown, u) for u in uploads]

    description = '  \n'.join(uploads)

    # Now we've got the uploaded file info, attach that to the tag
    url = urljoin(
        api_url, 'repository/tags/{t}'.format(t=quote(release_tag, safe='')))
    tag_details = requests.get(url, headers=auth, verify=verify).json()

    method = requests.post
    if 'release' in tag_details and tag_details['release'] is not None:
        description = '  \n'.join(
            (tag_details['release']['description'], description))
        method = requests.put

    rsp = method(url + '/release',
                 data={'description': description},
                 headers=auth, verify=verify)
    try:
        rsp.raise_for_status()
        tagname = rsp.json()['tag_name']
        logging.info("Uploaded %s to tag %s: %s", filenames,
                     tagname, urljoin(server, "tags/%s" % quote(tagname)))

    except BaseException as ex:
        logging.info("Setting tag description failed: "
                     "\"%s\" error: %s", description, ex)

# -----------------------------------------------------------------------------
def upload_project_release(folder):
    """Upload list of folder to the project

    Arguments:
        folder_list {list} -- List of folder at the root of the proect
    """
    version = xe2.__version__
    release_tag = 'v' + version

    filenames = []

    def add_file(filename):
        if version not in filename:
            return
        filenames.append(filename)

    common.apply_function_in_folder(folder, add_file, filename_ext=".exe")
    filenames.sort()
    filenames = [filenames[-1]]
    uploads = gitlab_upload(filenames)
    description = release_description(uploads)
    gitlab_description_upload(description, release_tag)

# -----------------------------------------------------------------------------
def upload_releases_files():
    upload_project_release(os.path.join(__get_this_folder(), "..", "dist"))

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    upload_releases_files()

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
