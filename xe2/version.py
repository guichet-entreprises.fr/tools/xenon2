#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version_info__ = (2, 0, 36)
__release_date__ = '2020-07-24'
