#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

###############################################################################
# standard object to wrap website
# and generate the website
###############################################################################
import logging
import sys
import os
import os.path
import jinja2

from pymdtools import common
from pymdtools import mdfile
import xe2layout.config as config

from . import website
from . import resource


###############################################################################
# check if a key is a code error
###############################################################################
def is_code_error(key):
    key = str(key)
    error_list = ['400', '401', '403', '404', '405', '408',
                  '410', '411', '412', '413', '414', '415',
                  '500', '501', '502', '503', '506']

    return key.strip() in error_list


###############################################################################
# check internal key
#
# @param site the website
###############################################################################
def list_md_file(site):
    return [node_id for node_id in site.res
            if 'is_md' in site.res.nodes[node_id]]

###############################################################################
# check internal key
#
# @param site the website
###############################################################################
def check_key_in_md(site):
    nodes = list_md_file(site)
    for node_id in nodes:
        ressource = site.res.nodes[node_id]['resource']
        filename = ressource.full_filename
        md_content = mdfile.MarkdownContent(filename)
        if 'key' in md_content:
            logging.info('%s --> %s', filename, md_content['key'])
            print(ressource.relative_filename)
        else:
            logging.info('%s --> No Key', filename)
            key = os.path.splitext(ressource.relative_filename)[0]
            key = key.replace(os.sep, " ")
            key = key.replace("'", " ")
            key = common.slugify(key)
            md_content['key'] = key
            md_content.write()
            logging.info('   |--> Add Key %s', key)

###############################################################################
# check internal key
#
# @param site the website
###############################################################################
def report_dead_links(site):
    result = {}
    result['date'] = common.timestamp_now()
    result['site'] = site

    nodes = list_md_file(site)
    result['nodes_id'] = nodes
    result['all_nodes_id'] = [node_id for node_id in site.res]
    result['nodes'] = {}
    for node_id in result['all_nodes_id']:
        result['nodes'][node_id] = {}
        if 'is_link' in site.res.nodes[node_id]:
            result['nodes'][node_id]['is_link'] = \
                site.res.nodes[node_id]['is_link']

    for node_id in nodes:
        res = site.res.nodes[node_id]['resource']
        filename = res.full_filename
        result['nodes'][node_id]['filename'] = filename
        result['nodes'][node_id]['resource'] = res
        result['nodes'][node_id]['neighbors'] = []
        result['nodes'][node_id]['has_dead_links'] = False
        for neighbor in site.res.neighbors(node_id):
            is_dead = 'is_link' in site.res.nodes[neighbor] \
                and not site.res.nodes[neighbor]['is_link']
            if not result['nodes'][node_id]['has_dead_links']:
                result['nodes'][node_id]['has_dead_links'] = is_dead
            result['nodes'][node_id]['neighbors'].append({
                'id': neighbor,
                'data': site.res.nodes[neighbor],
                'links': site.res.edges[node_id, neighbor]['links'],
                'is_dead': is_dead,
            })

    template_env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(os.path.join(__get_this_folder(),
                                                    "templates")),
        autoescape=jinja2.select_autoescape(['html', 'xml']))

    return template_env.get_template("dead_links.md.j2").render(result)

###############################################################################
# Create a graph for dot
#
# @return the local folder.
###############################################################################
def analyse_dead_links(conf_filename, output="dead_links.md"):
    """
    This function analyse an entire web site.

    @type conf_filename: string
    @param conf_filename: Configuration of the website generation.

    @return nothing
    """

    conf_filename = common.check_is_file_and_correct_path(conf_filename)
    conf = config.read_yaml(conf_filename)

    entries = conf['paths']['entries']
    for site_key in entries:
        if isinstance(entries[site_key], str):
            continue
        site = website.WebSite(conf_filename, site_key)
        report = report_dead_links(site)
        output_filename = os.path.join(conf['paths']['entries']
                                       [site_key]['root'], output)
        common.set_file_content(output_filename, report)


###############################################################################
# Create a graph for dot
#
# @return the local folder.
###############################################################################
def analyse(the_website):
    """
    This function build an analyse of the site.

    @type website: WebSite
    @param website: the web site

    @return nothing
    """

    analyse_function = {
        "Markdown files": lambda node: 'is_md' in node,
        "Not Markdown files":
        lambda node: 'is_md' not in node and 'is_on_filesystem' in node,
        "All Files": lambda node: 'is_on_filesystem' in node,
        "Links":
            lambda node: 'is_link' in node and node['is_link'],
        "Dead links":
            lambda node: 'is_link' in node and not node['is_link'],
    }

    result = {}

    for key in the_website.res:
        for domain in analyse_function:
            if analyse_function[domain](the_website.res.nodes[key]):
                if domain not in result:
                    result[domain] = []
                result[domain].append(key)

    display = ""
    for domain in result:
        display += website.chapter_str(domain)
        for key in result[domain]:
            display += str(key) + '\n'

    print(display)
    return result

###############################################################################
# Create a graph for dot
#
# @return the local folder.
###############################################################################
def find_main_key(conf):
    destinations = conf['paths']['destination']['websites']
    result = 'main'
    min_length = 0

    for key in destinations:
        if key == 'root':
            continue
        if min_length == 0 or len(destinations[key]) < min_length:
            result = key
            min_length = len(destinations[key])

    return result

###############################################################################
# Create a graph for dot
#
# @return the local folder.
###############################################################################
def generate_httpd_conf_context(conf, sites):
    context = {}
    context['this_path'] = conf['paths']['destination']['root']

    main_key = find_main_key(conf)

    context['main_root_path'] = \
        conf['paths']['destination']['websites'][main_key]

    home_key = sites[main_key].res.graph['home_key']
    home_title = sites[main_key].res.nodes[home_key]['title']
    context['main_root_name'] = common.slugify(home_title, separator='_')
    context['path_sep'] = os.sep
    context['entries'] = []

    for site in sites:
        new_entrie = {}
        new_entrie['name'] = site
        sub_folder = os.path.relpath(
            conf['paths']['entries'][site]['root'],
            conf['paths']['root'])
        new_entrie['folder'] = os.path.relpath(
            conf['paths']['destination']['websites'][site] +
            os.sep + sub_folder,
            context['main_root_path'])
        new_entrie['folder'] = new_entrie['folder'].replace(os.sep, "/")
        new_entrie['pages'] = []
        for page_key in conf['paths']['entries'][site]:
            if page_key == "root":
                continue
            the_file = resource.Resource(
                conf['paths']['entries'][site][page_key],
                base_path=sites[site].base_path)
            the_node = sites[site].res.nodes[the_file.relative_filename]
            ressource = the_node['target_resource']
            ressource.base_path = context['main_root_path']
            filename = ressource.relative_filename
            new_entrie['pages'].append({
                'name': page_key,
                'filename': filename.replace(os.sep, "/"),
            })
        context['entries'].append(new_entrie)

    if 'http' in conf and 'redirections' in conf['http']:
        context['redirections'] = conf['http']['redirections']

    return context

###############################################################################
# Create a graph for dot
#
# @return the local folder.
###############################################################################
def generate_httpd_conf(conf, sites):
    template_conf = config.read_yaml(conf['paths']['template_conf'])
    jinja_env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(template_conf['paths']['httpd']),
        autoescape=jinja2.select_autoescape(['html', 'xml']))

    jinja_env.filters['is_code_error'] = is_code_error

    dest_folder = conf['paths']['destination']['root']

    context = generate_httpd_conf_context(conf, sites)

    files = ['localhost.conf', 'entries.conf']

    for filename in files:
        logging.info("Generate httpd conf %s" % filename)
        content = jinja_env.get_template(filename + '.j2').render(context)
        common.set_file_content(os.path.join(dest_folder, filename), content)


###############################################################################
# Create a graph for dot
#
# @return the local folder.
###############################################################################
def generate_site(conf_filename):
    """
    This function build an entire web site.

    @type conf_filename: string
    @param conf_filename: Configuration of the website generation.

    @return nothing
    """
    conf_filename = common.check_is_file_and_correct_path(conf_filename)
    conf = config.read_yaml(conf_filename)

    entries = conf['paths']['entries']
    sites = {}
    site_keys = [key for key in entries if not isinstance(entries[key], str)]

    for site_key in site_keys:
        site = website.WebSite(conf_filename, site_key)
        site.prepare_generation()
        sites[site_key] = site

    website.neighbors(sites)

    for site_key in site_keys:
        sites[site_key].generate()

    generate_httpd_conf(conf, sites)

    return sites

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]
