;##############################################################################
; @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
; 	All Rights Reserved.
; 	Unauthorized copying of this file, via any medium is strictly prohibited
; 	Dissemination of this information or reproduction of this material
; 	is strictly forbidden unless prior written permission is obtained
; 	from Guichet Entreprises.
;##############################################################################
;---------------------------------
;  General
;---------------------------------
!addincludedir "./nsh"
!include "MUI2.nsh"
!include "StrFunc.nsh"
!include "fileassoc.nsh"

;---------------------------------
; The product
;---------------------------------
!define PRODUCT_SHORTNAME "xenon2"
!define PRODUCT_LONGNAME "Xenon 2 Megablast"
!include "version.nsh"

!define BN_PKG "${PRODUCT_SHORTNAME}"
!include "build_number_increment.nsh"
!include "context_menu.nsh"

;---------------------------------
; Explorer context and registry
;---------------------------------
!define DESCRIPTION "Xenon 2 Megablast"

;---------------------------------
; General
;---------------------------------
!define /date TIMESTAMP "%Y-%m-%d"

;---------------------------------
Name "${PRODUCT_LONGNAME}"
OutFile "..\\dist\\setup_${PRODUCT_SHORTNAME}-v${PRODUCT_VERSION}-[${Build_NUMBER}]-${TIMESTAMP}.exe"
ShowInstDetails "nevershow"
ShowUninstDetails "nevershow"
CRCCheck on
XPStyle on
VIProductVersion "${PRODUCT_VERSION}-[${Build_NUMBER}]"
SpaceTexts none

;---------------------------------
!define MUI_ICON "icon/ge.ico"
!define MUI_UNICON "icon/ge.ico"
BrandingText "Guichet Entreprises - ${TIMESTAMP}"

;--------------------------------
;Folder selection page
InstallDir "$PROGRAMFILES\ge.fr\${PRODUCT_SHORTNAME}"
InstallDirRegKey HKCU "Software\${PRODUCT_SHORTNAME}" ""

;--------------------------------
;Modern UI Configuration
 
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages
;--------------------------------
!insertmacro MUI_LANGUAGE "French"


!define MAIN_MENU "Xenon 2"

Var FileClass
;-------------------------------- 
;Installer Sections     
Section "install"  
	;Add files
	SetOutPath "$INSTDIR"

	;Delete previous Files 
	RMDir /r "$INSTDIR\*.*"    

 	File /r "..\\dist\\freeze\\*.*"
 	File /r /x *.log "icon\\*.ico"
 	File  "..\\packaging\\*.bat"
	
	ReadRegStr $FileClass HKCR ".yml" ""
	StrCmp $FileClass "" FileClassNotFound
		Goto FileClassNotFoundDone
	FileClassNotFound:
		StrCpy $FileClass "geyaml"
		WriteRegStr HKCR ".yml" "" "$FileClass"
		WriteRegStr  HKCR "$FileClass" "" `Yaml File`
		WriteRegStr  HKCR "$FileClass\DefaultIcon" "" `$INSTDIR\ge.ico`

	FileClassNotFoundDone:

	!insertmacro ADD_CONTEXT_MENU "yml" `Generate web site` `"$INSTDIR\xenon2_launcher.bat" "%1"` `"$INSTDIR\ge.ico"` `"$INSTDIR\xenon2.ico"`
	!insertmacro ADD_CONTEXT_MENU "yml" `Analyse dead links` `"$INSTDIR\xenon2_dead_links.bat" "%1"` `"$INSTDIR\ge.ico"` `"$INSTDIR\dead.ico"`

	WriteUninstaller "$INSTDIR\Uninstall.exe"
 
SectionEnd
 
 
;--------------------------------    
;Uninstaller Section  
Section "un.Uninstall"
 
	;Delete Files 
	RMDir /r "$INSTDIR\*.*"    

	;Remove the installation directory
	RMDir "$INSTDIR"

	!insertmacro DEL_CONTEXT_MENU "yml" `Generate web site`

SectionEnd
