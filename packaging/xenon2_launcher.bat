@ECHO off
REM ###############################################################################
REM # @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
REM # 	All Rights Reserved.
REM # 	Unauthorized copying of this file, via any medium is strictly prohibited
REM # 	Dissemination of this information or reproduction of this material
REM # 	is strictly forbidden unless prior written permission is obtained
REM # 	from Guichet Entreprises.
REM ###############################################################################
CHCP 65001
MODE 100,40
SETLOCAL EnableDelayedExpansion
GOTO START
:PRINT_LINE <textVar>
(
    SET "LINE_HERE=%~1"
    @ECHO !LINE_HERE!
    exit /b
)
REM ###############################################################################
:START
CLS
CALL :PRINT_LINE "╔══════════════════════════════════════════════════════════════════════════════════════════════════╗"
CALL :PRINT_LINE "║                      ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗┌┐┌┌┬┐┬─┐┌─┐┌─┐┬─┐┬┌─┐┌─┐┌─┐                        ║"
CALL :PRINT_LINE "║                      ║ ╦│ │││  ├─┤├┤  │   ║╣ │││ │ ├┬┘├┤ ├─┘├┬┘│└─┐├┤ └─┐                        ║"
CALL :PRINT_LINE "║                      ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝┘└┘ ┴ ┴└─└─┘┴  ┴└─┴└─┘└─┘└─┘                        ║"
CALL :PRINT_LINE "╚══════════════════════════════════════════════════════════════════════════════════════════════════╝"
CALL :PRINT_LINE "╔══════════════════════════════════════════════════════════════════════════════════════════════════╗"
CALL :PRINT_LINE "║                                                                                                  ║"                         
CALL :PRINT_LINE "║                         ▒██   ██▒▓█████  ███▄    █  ▒█████   ███▄    █                           ║"           
CALL :PRINT_LINE "║                         ▒▒ █ █ ▒░▓█   ▀  ██ ▀█   █ ▒██▒  ██▒ ██ ▀█   █                           ║"            
CALL :PRINT_LINE "║                         ░░  █   ░▒███   ▓██  ▀█ ██▒▒██░  ██▒▓██  ▀█ ██▒                          ║"      
CALL :PRINT_LINE "║                          ░ █ █ ▒ ▒▓█  ▄ ▓██▒  ▐▌██▒▒██   ██░▓██▒  ▐▌██▒                          ║"          
CALL :PRINT_LINE "║                         ▒██▒ ▒██▒░▒████▒▒██░   ▓██░░ ████▓▒░▒██░   ▓██░                          ║"     
CALL :PRINT_LINE "║                         ▒▒ ░ ░▓ ░░░ ▒░ ░░ ▒░   ▒ ▒ ░ ▒░▒░▒░ ░ ▒░   ▒ ▒                           ║"          
CALL :PRINT_LINE "║                         ░░   ░▒ ░ ░ ░  ░░ ░░   ░ ▒░  ░ ▒ ▒░ ░ ░░   ░ ▒░                          ║"        
CALL :PRINT_LINE "║                          ░    ░     ░      ░   ░ ░ ░ ░ ░ ▒     ░   ░ ░                           ║"        
CALL :PRINT_LINE "║                          ░    ░     ░  ░         ░     ░ ░           ░                           ║"             
CALL :PRINT_LINE "║                                                                                                  ║"       
CALL :PRINT_LINE "╚══════════════════════════════════════════════════════════════════════════════════════════════════╝"
@ECHO:
@ECHO:
SET XENON_PATH=%~dp0xenon2.exe
@ECHO: XENON_PATH = %XENON_PATH%
@ECHO:
@ECHO: FILE = %1
@ECHO:
TITLE [Xenon 2] Génération
CD /D %~dp0
"%XENON_PATH%" --verbose --conf "%1" --gen-site=yes
REM -------------------------------------------------------------------------------
CHOICE /C:YN /M "Do it again ? (Y/N)"
IF "%ERRORLEVEL%" EQU "1" GOTO :START
IF "%ERRORLEVEL%" EQU "2" GOTO :EOF
REM -------------------------------------------------------------------------------
:EOF
